
## Installation PHPUnit

- wget https://phar.phpunit.de/phpunit-7.phar
- chmod +x phpunit-7.phar
- mv phpunit-7.phar /usr/local/bin/phpunit

## Config
- Clone a new database from your database
- Create /wp-unit-test/wordpress-tests-lib/wp-tests-config.php from /wp-unit-test/wordpress-tests-lib/wp-tests-config.php.example
- Config database connection within file wp-unit-test/wordpress-tests-lib/wp-tests-config.php

## Run PhpUnitTest
```
- cd wp-unit-test 
- phpunit
- or: phpunit --debug 
```
## Note
- When you run the phpunit command from ```phpunit.xml.dist```, 
it will look for that file, and read the configuration from it.
- This configuration will load the /bootstrap.php file . It will also load all the .php files in the directory that begin with test-

## Writing Your Tests
- Test class will extend the ```WP_UnitTestCase``` class
- Only methods prefixed with “test” will be considered a unit test. All other methods will be skipped.

## Set Up and Tear Down in Class test
- setUp() and tearDown() methods which are automatically called by PHPUnit before and after each test method is run
- In the tearDown() WP_UnitTestCase performs a ROLLBACK query, and in setUp(), the caches are flushed.
- If you don't overnight tearDownAfterClass method in your class, all data of the tables: posts, postmeta, comments,
   commentmeta, term_relationships, termmeta will be reset

# Resources
- WordPress PHPUnit - https://github.com/wp-cli/wp-cli/wiki/Plugin-Unit-Tests
- Documentation for PHPUnit – The PHP Testing Framework - https://phpunit.de/documentation.html 
- How to create Plugin WP Unit Tests · wp-cli/wp-cli Wiki - https://make.wordpress.org/cli/handbook/plugin-unit-tests/
- https://core.trac.wordpress.org/browser/trunk/tests/phpunit/includes/factory.php
- http://develop.svn.wordpress.org/trunk/tests/phpunit/
- https://make.wordpress.org/core/handbook/testing/automated-testing/writing-phpunit-tests/
