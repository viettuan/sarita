<?php

define('WP_ELI_PARENT_DIR', get_template_directory());
define('WP_ELI_PARENT_DIR_URI', get_template_directory_uri());
define('WP_ELI_CORE_DIR', WP_ELI_PARENT_DIR . '/core');
define('WP_ELI_TEMPLATES_DIR', WP_ELI_PARENT_DIR . '/templates');
define('WP_ELI_ASSETS_DIR', WP_ELI_PARENT_DIR . '/assets');
define('WP_ELI_SCRIPTS_DIR', WP_ELI_ASSETS_DIR . '/scripts');
define('WP_ELI_ADMIN_SCRIPTS_DIR', WP_ELI_ASSETS_DIR . '/scripts-admin');
define('WP_ELI_STYLES_DIR', WP_ELI_ASSETS_DIR . '/styles');
define('WP_ELI_MODULES_DIR', WP_ELI_CORE_DIR . '/Modules');