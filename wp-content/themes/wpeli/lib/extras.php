<?php

namespace Roots\Sage\Extras;

use Eli\Helpers\Constant;
use Eli\ThemeOptions\Settings;
use Exception;
use Roots\Sage\Setup;
use WP_Rewrite;
use Eli\Helpers\Common;

/**
 * Add <body> classes
 * @param $classes
 * @return string
 */
function body_class($classes)
{
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    // Add class if sidebar is active
    if (Setup\display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    return $classes;
}

add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more()
{
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}

add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

function arrayGlobal()
{
    return apply_filters('wp_eli_global_variables', [
        'ajax_url' => admin_url('admin-ajax.php'),
        'typenow' => get_post_type(),
    ]);
}


function add_widget($widget_name = 'wp_eli_custom_widget', $add_to_sidebar = '', $settings = [])
{

    if ($add_to_sidebar === '') {
        $add_to_sidebar = 'sidebar-primary';
    }
    // Reset empty sidebar-primary
    $sidebar_options = get_option('sidebars_widgets');

    if (!isset($sidebar_options[$add_to_sidebar])) {
        $sidebar_options[$add_to_sidebar] = ['_multiwidget' => 1];
    }

    $widget = get_option('widget_' . $widget_name);

    if (!is_array($widget)) {
        $widget = [];
    }

    $count = count($widget) + 1;

    // add first widget to sidebar:
    $sidebar_options[$add_to_sidebar][] = $widget_name . '-' . $count;

    $widget[$count] = $settings;

    update_option('sidebars_widgets', $sidebar_options);
    update_option('widget_' . $widget_name, $widget);
}

function getTemplatePartCustom($file, $model, $return = false)
{

    $fileBasename = basename($file);

    if (!file_exists($file) && file_exists(get_stylesheet_directory() . '/' . $fileBasename . '.php')) {
        $file = get_stylesheet_directory() . '/' . $fileBasename . '.php';
    } elseif (!file_exists($file) && file_exists(get_template_directory() . '/' . $fileBasename . '.php')) {
        $file = get_template_directory() . '/' . $fileBasename . '.php';
    }
    ob_start();
    require($file);
    $data = ob_get_clean();

    if ($return) {
        return $data;
    }
    echo $data;
}

add_action('wp_ajax_restore_default_theme_options', __NAMESPACE__ . '\\ajax_restore_default_theme_options');
function ajax_restore_default_theme_options()
{
    try {

        $fields = Constant::$arrayOptionsFieldsKey;

        if ($fields) {
            foreach ($fields as $field) {
                delete_option('options_' . $field);
            }
        }

        wp_send_json([
            'status' => 'success',
            'message' => ''
        ]);

    } catch (Exception $exception) {
        wp_send_json([
            'status' => 'Failed',
            'message' => $exception->getMessage(),
        ]);
    }

}

/**
 * Check current user agent come from IOS,Android,WebOS devices
 * @Author: ThienLD
 * @return string - device type by constant
 */
function getUserAgentComeFromByDeviceType()
{
    //Detect special conditions devices
    $iPod = stripos($_SERVER['HTTP_USER_AGENT'], 'iPod');
    $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone');
    $iPad = stripos($_SERVER['HTTP_USER_AGENT'], 'iPad');
    $Android = stripos($_SERVER['HTTP_USER_AGENT'], 'Android');
    $webOS = stripos($_SERVER['HTTP_USER_AGENT'], 'webOS');


    //do something with this information
    if ($iPod || $iPhone || $iPad) {
        return Constant::USER_AGENT_IOS_DEVICES;
    } else if ($Android) {
        return Constant::USER_AGENT_ANDROID_DEVICES;
    } else if ($webOS) {
        return Constant::USER_AGENT_WEB_OS_DEVICES;
    }
    return '';
}

/**
 * Check if user agent comes from IOS devices
 * @Author: ThienLD
 * @return boolean
 */
function isIOSUserAgent()
{
    return getUserAgentComeFromByDeviceType() === Constant::USER_AGENT_IOS_DEVICES;
}

function getLogoImage()
{
    $logo = Settings::getFieldByName('logo', '', 'option');
    return !empty($logo) && !empty($logo['url']) ? $logo['url'] : '';
}


function getPageSlug($post_slug = '')
{
    if (empty($post_slug)) {
        return '';
    }
    if (function_exists('wpml_object_id_filter')) {
        $post = get_page_by_path($post_slug);
        if (!empty($post)) {
            $post_id = wpml_object_id_filter($post->ID, 'page', false, ICL_LANGUAGE_CODE);
            $post_obj = get_post($post_id);
            if (!empty($post_obj)) {
                return $post_obj->post_name;
            }
        }
    }
    return $post_slug;

}

function postPagingNav()
{
    global $wp_query;
    $rewrite = new WP_Rewrite();
    // Don't print empty markup if there's only one page.
    if ($wp_query->max_num_pages < 2) {
        return;
    }

    $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
    $pagenum_link = html_entity_decode(get_pagenum_link());
    $query_args = [];
    $url_parts = explode('?', $pagenum_link);

    if (isset($url_parts[1])) {
        wp_parse_str($url_parts[1], $query_args);
    }

    $pagenum_link = remove_query_arg(array_keys($query_args), $pagenum_link);
    $pagenum_link = trailingslashit($pagenum_link) . '%_%';

    $format = $rewrite->using_index_permalinks() && !strpos($pagenum_link, 'index.php') ? 'index.php/' : '';
    $format .= $rewrite->using_permalinks() ? user_trailingslashit($rewrite->pagination_base . '/%#%', 'paged') : '?paged=%#%';

    // Set up paginated links.
    $links = paginate_links([
        'base' => $pagenum_link,
        'format' => $format,
        'total' => $wp_query->max_num_pages,
        'current' => $paged,
        'mid_size' => 3,
        'type' => 'list',
        'add_args' => array_map('urlencode', $query_args),
        'prev_text' => '<',
        'next_text' => '>',
    ]);

    if ($links) :

        ?>
        <nav class="paging" role="navigation">
            <?php echo $links; ?>
        </nav><!-- .navigation -->
        <?php
    endif;
}


function merge_option_with_theme_settings($post_id)
{
    if ($post_id == 'options') {
        $options = [
            'show_on_front',
            'default_comment_status',
        ];

        foreach ($options as $option) {
            $value = get_field($option, 'option');

            if (is_array($value) && count($value) > 0) {
                $value = $value[0];
            }
            update_option($option, $value);
        }

        $media_options = [
            'thumbnail_size_w',
            'thumbnail_size_h',
            'thumbnail_crop',
            'medium_size_w',
            'medium_size_h',
            'large_size_w',
            'large_size_h',
            'image_default_size',
            'image_default_align',
            'image_default_link_type',
        ];
        foreach ($media_options as $option) {
            $option = trim($option);
            $value = null;
            if (isset($_POST[$option])) {
                $value = $_POST[$option];
                if (!is_array($value)) {
                    $value = trim($value);
                }
                $value = wp_unslash($value);
            }
            update_option($option, $value);
        }

        if (isset($_GET['page']) && $_GET['page'] == 'theme-installation') {
            update_option('theme_installed', true);

            if (get_option('show_on_front') == 'page' && get_option('page_on_front') != null) {
                wp_redirect(get_home_url());
            } else {
                wp_redirect(admin_url('admin.php?page=theme-options'));
            }
            exit;
        }
    }
}

add_action('acf/save_post', __NAMESPACE__ . '\\merge_option_with_theme_settings', 120, 1);

function redirect_to_theme_options_if_page_installed()
{
    if (isset($_GET['page']) && $_GET['page'] == 'theme-installation' && get_option('theme_installed')) {
        wp_redirect(admin_url('admin.php?page=theme-options'));
        exit;
    }
}

add_action('init', __NAMESPACE__ . '\\redirect_to_theme_options_if_page_installed');

function callback_change_theme_options($post_id)
{
    if ($post_id == 'options') {
        $fields = get_field_objects($post_id);

        foreach ($fields as $key => $field) {
            do_action($key . '_handler', $field, $key);
        }
    }
}

add_action('acf/save_post', __NAMESPACE__ . '\\callback_change_theme_options', 20, 1);

add_filter('acf/settings/save_json', function () {
    return get_stylesheet_directory() . '/acf-json';
});

add_filter('acf/settings/load_json', function () {
    $paths = [get_template_directory() . '/acf-json'];

    if (is_child_theme()) {
        $paths[] = get_stylesheet_directory() . '/acf-json';
    }

    return $paths;
});

add_action('admin_footer', __NAMESPACE__ . '\\hide_default_field_groups', 120);

function hide_default_field_groups()
{
    add_action('admin_enqueue_scripts', function () {
        wp_register_script('deploy-js', get_template_directory_uri() . WP_ELI_ADMIN_SCRIPTS_DIR . '/acf.js', ['jquery'], false, true);
        wp_enqueue_script('deploy-js');
    });
}