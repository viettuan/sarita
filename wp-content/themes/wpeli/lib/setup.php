<?php

namespace Roots\Sage\Setup;

use Eli\Autoload;
use Eli\Helpers\Common;
use Roots\Sage\Assets;
use Roots\Sage\Extras;

/**
 * Theme setup
 */
function setup()
{
    // Enable features from Soil when plugin is activated
    // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'footer_menu' => __('Footer Menu', 'wp_eli'),
        'header_menu' => __('Header Menu', 'wp_eli'),
    ]);

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');

    // Enable post formats
    // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    // add_editor_style(Assets\asset_path('styles/main.css'));

}

add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init()
{
//    register_sidebar([
//        'name' => __('Primary', 'sage'),
//        'id' => 'sidebar-primary',
//        'before_widget' => '<section class="widget %1$s %2$s">',
//        'after_widget' => '</section>',
//        'before_title' => '<h3>',
//        'after_title' => '</h3>'
//    ]);

    register_sidebar([
        'name' => __('Post Details', 'sage'),
        'id' => 'sidebar-details',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);

//
//    register_sidebar([
//        'name' => __('Footer', 'sage'),
//        'id' => 'sidebar-footer',
//        'before_widget' => '<section class="widget %1$s %2$s">',
//        'after_widget' => '</section>',
//        'before_title' => '<h3>',
//        'after_title' => '</h3>'
//    ]);
}

//add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar()
{
    static $display;

    isset($display) || $display = !in_array(true, [
        // The sidebar will NOT be displayed if ANY of the following return true.
        // @link https://codex.wordpress.org/Conditional_Tags
        is_404(),
        is_front_page(),
        is_page_template('template-custom.php'),
    ]);

    return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets()
{
    //wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

//    $scripts = array_diff(scandir(WP_ELI_SCRIPTS_DIR), ['.', '..']);
//    foreach ($scripts as $script) {
//        if(!empty($script)) {
//            $filename = basename($script, '.js');
//            wp_enqueue_script($filename, Assets\asset_path('scripts/' . $script), ['jquery'], null, true);
//        }
//    }

    wp_localize_script('main', 'WP_ELI_VARIABLES', Extras\arrayGlobal());
}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

function assets_admin()
{
    $scripts_admin = array_diff(scandir(WP_ELI_ADMIN_SCRIPTS_DIR), ['.', '..']);
    foreach ($scripts_admin as $script) {
        $filename = basename($script, '.js');
        $enqueuedFilePath = Assets\asset_path('scripts/' . $script);
        if(file_exists($enqueuedFilePath)){
            wp_enqueue_script($filename, $enqueuedFilePath, ['jquery'], null, true);
        }
    }
    wp_localize_script('admin', 'WP_ELI_VARIABLES', Extras\arrayGlobal());
}

add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\assets_admin');

if (function_exists('acf_add_options_page')) {

    $option_page = acf_add_options_page([
        'page_title' => __('Theme Options', 'wp_eli'),
        'menu_title' => __('Theme Options', 'wp_eli'),
        'menu_slug' => 'theme-options',
        'capability' => 'edit_posts',
        'redirect' => false
    ]);

    if (function_exists('acf_add_options_sub_page') && !get_option('theme_installed', false)) {
        acf_add_options_sub_page([
            'page_title' => __('Theme Installation', 'wp_eli'),
            'menu_title' => __('Theme Installation', 'wp_eli'),
            'menu_slug' => 'theme-installation',
            'parent_slug' => $option_page['menu_slug'],
            'update_button' => __('Install', 'wp_eli'),
        ]);
    }

    if (function_exists('acf_add_options_sub_page')) {
        acf_add_options_sub_page([
            'page_title' => __('Theme Activation', 'wp_eli'),
            'menu_title' => __('Re-active Theme', 'wp_eli'),
            'parent_slug' => $option_page['menu_slug'],
        ]);
    }

}

add_action('init', [Autoload::class, 'init'], 0);

function process_after_active_theme()
{
    global $pagenow;
    if (is_admin() && 'themes.php' == $pagenow && isset($_GET['activated'])) {

        Common::activePlugins([
            'advanced-custom-fields-pro/acf.php',
            'acf-pro-sync/acf-sync.php',
            'acf-button/acf-button.php',
            'acfml/wpml-acf.php',
            'classic-editor/classic-editor.php',
            'user-role-editor/user-role-editor.php',
            'qtranslate-x/qtranslate.php',
            'acf-qtranslate/acf-qtranslate.php'
        ]);

        if (is_plugin_active('acf-pro-sync/acf-sync.php') && function_exists('acf_sync_import_json_folder')) {
            acf_sync_import_json_folder();
        }

        if (!get_option('theme_installed', false)) {
            wp_redirect(admin_url('admin.php?page=theme-installation'));
        } else {
            wp_redirect(admin_url('admin.php?page=theme-options'));
        }
    }
}

add_action('init', __NAMESPACE__ . '\\process_after_active_theme');
