<?php

namespace Eli\Handler;

use Eli\Helpers\Common;

function enable_gravity_forms_handler($field)
{
    if ($field['value'] == 1) {
        Common::activePlugins([
            'gravityforms/gravityforms.php',
        ]);

        if (get_field('enable_multi_languages_supports', 'option') == 1) {
            Common::activePlugins([
                'gravityforms-multilingual/plugin.php',
            ]);
        }
    } else {
        deactivate_plugins([
            'gravityforms/gravityforms.php',
        ]);

        if (get_field('enable_multi_languages_supports', 'option') == 0) {
            deactivate_plugins([
                'gravityforms-multilingual/plugin.php',
            ]);
        }
    }
}

add_action('enable_gravity_forms_handler', __NAMESPACE__ . '\\enable_gravity_forms_handler');


function enable_cache_functionality_handler($field)
{
    if ($field['value'] == 1) {
        Common::activePlugins([
            'w3-total-cache/w3-total-cache.php',
        ]);
    } else {
        deactivate_plugins([
            'w3-total-cache/w3-total-cache.php',
        ]);
    }
}

add_action('enable_cache_functionality_handler', __NAMESPACE__ . '\\enable_cache_functionality_handler');

function enable_multi_languages_supports_handler($field)
{
    if ($field['value'] == 1) {
        Common::activePlugins([
            'sitepress-multilingual-cms/sitepress.php',
            'acfml/wpml-acf.php',
            'wpml-all-import/wpml-all-import.php',
            'wpml-cms-nav/plugin.php',
            'wpml-media/plugin.php',
            'wpml-sticky-links/plugin.php',
            'wpml-string-translation/plugin.php',
            'wpml-translation-management/plugin.php',
        ]);
    } else {
        deactivate_plugins([
            'sitepress-multilingual-cms/sitepress.php',
            'acfml/wpml-acf.php',
            'wpml-all-import/wpml-all-import.php',
            'wpml-cms-nav/plugin.php',
            'wpml-media/plugin.php',
            'wpml-sticky-links/plugin.php',
            'wpml-string-translation/plugin.php',
            'wpml-translation-management/plugin.php',
        ]);
    }
}

add_action('enable_multi_languages_supports_handler', __NAMESPACE__ . '\\enable_multi_languages_supports_handler');

function enable_seo_functionality_handler($field)
{
    if ($field['value'] == 1) {
        Common::activePlugins([
            'wordpress-seo/wp-seo.php',
        ]);
    } else {
        deactivate_plugins([
            'wordpress-seo/wp-seo.php',
        ]);
    }
}

add_action('enable_seo_functionality_handler', __NAMESPACE__ . '\\enable_seo_functionality_handler');

function enable_site_security_supports_handler($field)
{
    if ($field['value'] == 1) {
        Common::activePlugins([
            'better-wp-security/better-wp-security.php',
        ]);
    } else {
        deactivate_plugins([
            'better-wp-security/better-wp-security.php',
        ]);
    }
}

add_action('enable_site_security_supports_handler', __NAMESPACE__ . '\\enable_site_security_supports_handler');

function enable_page_builder_functionality_handler($field)
{
    if ($field['value'] == 1) {
        Common::activePlugins([
            'Ultimate_VC_Addons/Ultimate_VC_Addons.php',
        ]);
    } else {
        deactivate_plugins([
            'Ultimate_VC_Addons/Ultimate_VC_Addons.php',
        ]);
    }
}

add_action('enable_page_builder_functionality_handler', __NAMESPACE__ . '\\enable_page_builder_functionality_handler');