(function( $ ) {
/**
 * START - ONLOAD - JS
 * Updated by Vy Le Oai
 * Date: 12/12/2016
 * 
 * 1. toggleMb()        :   Toggle menu on mobile
 * 2. chartSlider()    :   Create chart slider
 * 3. showSubmenu()     :   Show submenu
 * 4. makeChart()       :   Make donut chart
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */
var allpage = {};

// 1. toggleMb(): Toggle menu on mobile: 
allpage.toggleMb = function() {
    if(!$(".mobile-nav a").length) { return; }

    $(".mobile-nav a").on("click", function(e) {
        if($(".mobile-nav").hasClass("open")) {
            $(this).parent().removeClass("open");
            $(".main-nav").removeClass("open");
            $("body").removeClass("bodyover");
            $(".mobile-backdrop").remove();
        } else {
            $(this).parent().addClass("open");
            $(".main-nav").addClass("open");
            $("body").addClass("bodyover");
            $("body").append("<div class='mobile-backdrop'></div>");

            $(".mobile-backdrop").on("click", function(e) {
                $(".mobile-nav a").click();
            });
        }
    });
}

// 2. chartSlider(): Create chart slider
allpage.chartSlider = function() {
    if(!$(".chart-list").length) { return; }

    $(".chart-list").owlCarousel({
        items : 4, //10 items above 1000px browser width
        itemsDesktop : false, //5 items between 1000px and 901px
        itemsDesktopSmall : [1024, 3], // betweem 900px and 601px
        itemsTablet: false, //2 items between 600 and 0
        itemsMobile : [768, 1], // itemsMobile disabled - inherit from itemsTablet option
        navigation : true,
        pagination: false,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    
        // "singleItem:true" is a shortcut for:
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false
    });
}

// 3. showSubmenu(): Show sub menu
allpage.showSubmenu = function() {
    if(!$(".lang-btn").length) { return; }
    
    if($(window).width() <= 767) {
        var $lang = $(".lang-btn").closest(".language");

        $(".lang-btn").on("click", function(e) {
            e.preventDefault();

            if($lang.hasClass("open")) {
                $lang.removeClass("open");
                $lang.closest(".language").find(".sub-menu").hide();
            } else {
                $lang.addClass("open");
                $lang.closest(".language").find(".sub-menu").show();
            }
        });
    }

    // $(".language").on("clickoutside", function(e) {
    //     $(this).removeClass("open");
    // })
}

// 4. closeMobileMd: Turn off menu bar when showing modal on mobile
allpage.closeMobileMd = function() {
    if(!$("#login").length) { return; }

    if($(window).width() <= 767) {
        $("#login").on("show.bs.modal", function(e) {
            $(".mobile-nav a").click();
        });
    }
}


allpage.makeChart = function() {
    var chart = new Chartist.Pie('.ct-chart', {
    series: [10, 20, 50, 20, 5, 50, 15],
    labels: [1, 2, 3, 4, 5, 6, 7]
    }, {
    donut: true,
    showLabel: false
    });

    chart.on('draw', function(data) {
    if(data.type === 'slice') {
        // Get the total path length in order to use for dash array animation
        var pathLength = data.element._node.getTotalLength();

        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
        'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
        });

        // Create animation definition while also assigning an ID to the animation for later sync usage
        var animationDefinition = {
        'stroke-dashoffset': {
            id: 'anim' + data.index,
            dur: 1000,
            from: -pathLength + 'px',
            to:  '0px',
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: 'freeze'
        }
        };

        // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if(data.index !== 0) {
        animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
        }

        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
        data.element.attr({
        'stroke-dashoffset': -pathLength + 'px'
        });

        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
    }
    });

    // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    chart.on('created', function() {
    if(window.__anim21278907124) {
        clearTimeout(window.__anim21278907124);
        window.__anim21278907124 = null;
    }
    window.__anim21278907124 = setTimeout(chart.update.bind(chart), 10000);
    });

}

allpage.openModal = function (title, modalBody, config, yesCb, noCb) {
    // $('#myModal .title').html(title)
    // $('#myModal .modal-body').html(modalBody)
    // $('#myModal').modal('show')
    // if (yesCb) {
    //     $('myModal').modal('hide')
    //     yesCb()
    // }
    // if (noCb) {
    //     $('myModal').modal('hide')
    //     noCb()
    // }
}

/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    allpage.toggleMb();
    allpage.chartSlider();
    allpage.showSubmenu();
    allpage.closeMobileMd();
    // allpage.makeChart();
    // allpage.openModal('asdasd', $('.asd'));
    // allpage.openModal('asdasd', $('.asd-asd'));
    // $.ajax({
    //     url: 'http://aps.com/api',
    //     success: function(resp) {
    //         allpage.openModal()
    //     }
    // })
    // $('#modal1').modal()
    // $('#modal2').on('bs-modal-show')

});
/* OnLoad Window */
var init = function () {
};
window.onload = init;

})(jQuery);
