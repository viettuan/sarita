(function ($) {
    window.ASK =  window.ASK || {};
    window.ASK.ModalHandler = (function(){
        var module = {};
        module.MD_RESET_PASSWORD_NAME = 'ask-modal-reset-password';
        module.MD_LOGIN_NAME = 'ask-modal-login';
        module.MD_REGISTER_NAME = 'ask-modal-register';

        module.init = function(){
            $(document).ready( function() {
                $('#ask-login').off('click').on('click',module.openModalLoginHandler);
                $('#ask-logout').off('click').on('click',module.logoutHandler);
                $('#ask-reset-password').off('click').on('click',module.openResetPasswordModalHandler);
                $('#ask-register').off('click').on('click',module.openModalRegisterHandler);


            });
            return module;
        };

        module.openModalLoginHandler = function (e) {
            e.preventDefault();

            module.openModalHandler(module.MD_LOGIN_NAME);
            return false;
        };

        module.openModalRegisterHandler = function (e) {
            e.preventDefault();
            module.openModalHandler(module.MD_REGISTER_NAME);
            return false;
        };
        module.openResetPasswordModalHandler = function (e) {
            e.preventDefault();
            module.openModalHandler(module.MD_RESET_PASSWORD_NAME);
            return false;
        };


        module.openModalHandler = function (modalName) {
            var openModalName = modalName;
            switch (modalName) {
                case module.MD_LOGIN_NAME:
                    openModalName = '#modalLogin';
                    break;
                case module.MD_REGISTER_NAME:
                    openModalName = '#modalRegister';
                    break;
                case module.MD_RESET_PASSWORD_NAME:
                    openModalName = '#modalResetPassword';
                    break;
            }
            if($(openModalName).length > 0){
                module.closeModalHandler();
                $(openModalName).modal('show');
            }
        };

        module.closeModalHandler = function (modalName) {
            if( typeof modalName === 'undefined' || modalName == null ){
                $('.wp-eli-bootstrap .modal').modal('hide');
                return;
            }
            var closeModalName = modalName;
            switch (modalName) {
                case module.MD_LOGIN_NAME:
                    closeModalName = '#modalLogin';
                    break;
                case module.MD_REGISTER_NAME:
                    closeModalName = '#modalRegister';
                    break;
                case module.MD_RESET_PASSWORD_NAME:
                    closeModalName = '#modalResetPassword';
                    break;
            }
            if($(closeModalName).length > 0){
                $(closeModalName).modal('hide');
            }
        };

        module.logoutHandler = function (e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: WP_ELI_VARIABLES.ajax_url,
                data: {
                    'action': 'ask_user_logout'
                },
                success: function (data) {
                    if (data.status === 'success') {
                        window.location.href = data.url;
                    }
                }
            });
        };



        return module.init();
    })();
})(jQuery);