/**
 * Created by truong on 01/12/2016.
 */


(function ($) {

    $(document).ready(function () {

        $('.acf-button #restore-default').on('click', function (e) {
            e.preventDefault();
            if (confirm(WP_ELI_VARIABLES.restore_default_message)) {

                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: WP_ELI_VARIABLES.ajax_url,
                    data: {
                        'action': 'restore_default_theme_options'
                    },
                    success: function (data) {
                        if (data.status === 'success') {
                            location.reload();
                        } else {

                        }
                    }
                });
            }

        });

        if($('body').hasClass('post-type-silodata')) {

            $('.of-color').wpColorPicker(); // WpColorPicker
        }

    });

})(jQuery);