(function($){
    $('#the-list tr.type-acf-field-group').each(function() {
        var item = $(this).find('.row-title');
        if ($.inArray(item.text(), ['Options', 'Other options', 'Re-active Theme', 'Theme Options', 'Theme Setting']) !== -1) {
            item.prop('href', '#');
            item.css({'color': '#333'});
            item.closest('.title').find('.edit').remove();
            item.closest('.title').find('.trash').remove();
            var duplicate_btn = item.closest('.title').find('.acf-duplicate-field-group');
            duplicate_btn.html(duplicate_btn.html().replace('|', ''));

        }
    });
})(jQuery);