/*
 * GUIDE BE LAYOUT
 * Created on 08/12/2016
 */

1. Normal layout:
    1.1. Layout with sidebar:
        <section class="main has-sidebar">
            <div class="container">
                <div class="content">
                    ...
                </div>
                <div class="sidebar">
                    ...
                </div>
            </div>
        </section>
    1.1. Layout without sidebar
        <section class="main">
            <div class="container">
                <div class="content">
                    ...
                </div>
            </div>
        </section>

2/ Full width layout:
Add class f-w 

<section class="main f-w">
    <div class="container">
        ...
    </div>
</section>
