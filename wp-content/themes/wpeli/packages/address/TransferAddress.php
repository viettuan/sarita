<?php

class TransferAddress
{
    public static function getFileJson($file)
    {
        $result = file_get_contents(__DIR__.'/json/'. $file);
        return json_decode($result, true);
    }

    /**
     * Get list countries
     * @return array|mixed|object
     */
    public static function getListCountries()
    {
        return self::getFileJson('countries.json');
    }

    /**
     * Get list countries
     * @return array|mixed|object
     */
    public static function getListCountriesFull()
    {
        return self::getFileJson('countries-full.json');
    }

    /**
     * Get list states by country name
     * @param $countryName
     * @return array
     */
    public static function getStatesByCountryName($countryName)
    {
        $states = self::getFileJson('states.json');
        $result = [];
        foreach ($states as $state) {
            if ($state['countryName']==$countryName) {
                $result = $state['regions'];
                break;
            }
        }
        return $result;
    }

    /**
     * Get list cities by country name
     * @param $countryName
     * @return array
     */
    public static function getCitiesByCountryName($countryName)
    {
        $cities = self::getFileJson('cities.json');
        return $cities[$countryName] ?? [];
    }

    /**
     * Get list phone countries
     * @return array|mixed|object
     */
    public static function getPhoneCountries()
    {
        return self::getFileJson('phoneCountries.json');
    }
}
