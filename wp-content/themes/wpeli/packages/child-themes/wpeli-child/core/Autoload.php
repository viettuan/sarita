<?php

namespace EliChild;

use EliChild\Email\SampleEmail;
use EliChild\Modules\Sample\Admin\SampleAdminModule;

class Autoload
{
    /**
     * Auto new class in core
     * @author Sang Nguyen
     */
    public static function init()
    {
        SampleEmail::initialize();
        SampleAdminModule::initialize();
    }
}