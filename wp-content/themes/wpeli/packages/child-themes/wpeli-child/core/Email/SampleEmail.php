<?php

namespace EliChild\Email;

use Eli\Email\EmailAbstract;

class SampleEmail extends EmailAbstract
{
    /**
     * SampleEmail constructor.
     * @author Sang Nguyen
     */
    public function __construct()
    {
        $this->setDefaultData();

        parent::__construct();
    }

    /**
     * @param $email
     * @return bool
     * @author Sang Nguyen
     */
    public function trigger($email)
    {
        $this->recipient = $email;
        return $this->send($this->getRecipient(), $this->getSubject(), $this->wrapMessage($this->getContent(), $this->heading), $this->getHeaders());
    }


    /**
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getContent()
    {
        return $this->getTemplateFile();
    }

    /**
     * @author Sang Nguyen
     */
    public function setDefaultData()
    {

        $this->email_type = 'html';

        $this->subject = sprintf(__('Email send from %s', 'wp-eli-child'), get_option('blogname'));

        $this->heading = sprintf(__('This is just sample email %s', 'wp-eli-child'), get_option('blogname'));

        $this->footer_text = get_option('blogname');
        $this->id = 'sample';
        $this->template_html = __DIR__ . '/templates/sample-template.php';
    }

    /**
     * @return SampleEmail
     * @author Sang Nguyen
     */
    public static function initialize()
    {
        return new self();
    }
}
