<?php

namespace EliChild\Widgets\Sample;

use Eli\Widgets\Widget;

class SampleWidget extends Widget
{
    const BASE_URI = WP_ELI_PARENT_DIR_URI . '/core/Widgets/Sample';

    /**
     * SampleWidget constructor.
     * @author Sang Nguyen
     */
    public function __construct()
    {
        parent::__construct(

            // ID of widget
            'eli_sample_widget',

            // Widget name
            __('Sample Widget', 'wp-eli-child'),

            // Widget description
            [
                'description' => __('Sample Widget', 'wp-eli-child'),
            ]
        );

        $this->adminTemplatePath = __DIR__ . '/views/admin.php';
        $this->widgetTemplatePath = __DIR__ . '/views/frontend.php';

        add_action( 'wp_enqueue_scripts', [$this, 'registerScripts'], 120 );
        add_action( 'wp_enqueue_scripts', [$this, 'registerStyles'], 120 );
        add_action( 'wp_ajax_ajax_sample_widget_action', [$this, 'sampleHandleAjax'] );
    }

    /**
     * This just a sample handler for ajax request
     * @author Sang Nguyen
     */
    public function sampleHandleAjax()
    {
        // TODO: Process data here

        wp_send_json(['success' => true, 'message' => 'Successfully!']);
    }

    /**
     * Updating widget replacing old instances with new
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     * @author Sang Nguyen
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        if (!empty($new_instance['title'])) {
             $instance['title'] = strip_tags($new_instance['title']);
        }
        return $instance;
    }

    /**
     * Register scripts for upload widget
     *
     * @author Sang Nguyen
     */
    public function registerScripts()
    {
        wp_register_script('sample-widget-js', self::BASE_URI . '/assets/js/main.js', ['jquery'], false, true);
        wp_enqueue_script('sample-widget-js');
    }

    /**
     * Register stylesheets for upload widget
     *
     * @author Sang Nguyen
     */
    public function registerStyles()
    {
        //wp_enqueue_style('sample-widget-css', self::BASE_URI . '/assets/css/main.css');
    }
}

