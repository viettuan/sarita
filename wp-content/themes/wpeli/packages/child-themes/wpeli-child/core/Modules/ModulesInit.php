<?php

namespace EliChild\Modules;

use EliChild\Helpers\ChildConstant;
use EliChild\Modules\Sample\SampleModule;
use Eli\Modules\ModulesInit;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


class ChildModulesInit extends ModulesInit
{

    /**
     * ModulesInit constructor.
     * @param $type
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function __construct($type)
    {
        parent::__construct($type);

        switch ($type) {
            case ChildConstant::SAMPLE_MODULE:
                SampleModule::render();
                break;
        }
    }
}
