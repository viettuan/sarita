<?php

namespace EliChild\Modules\Sample;

use EliChild\Modules\Sample\Models\SampleModel;
use Roots\Sage\Extras;

class SampleModule
{

    public static function render()
    {
        Extras\getTemplatePartCustom(__DIR__ . '/templates/sample.php', new SampleModel());
    }
}