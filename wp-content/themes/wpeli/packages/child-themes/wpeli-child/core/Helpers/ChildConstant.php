<?php

namespace EliChild\Helpers;

use Eli\Helpers\Constant;

class ChildConstant extends Constant
{
    const SAMPLE_MODULE = 'sample-module';
    const SAMPLE_WIDGET = 'eli_sample_widget';

    public static $available_widgets= [
        self::SAMPLE_WIDGET,
    ];
}