<?php

namespace EliChild\Setup;

use EliChild\Autoload;
use EliChild\Helpers\ChildConstant;
use EliChild\Widgets\Sample\SampleWidget;
use Roots\Sage\Extras;

add_action('init', [Autoload::class, 'init'], 0);

function add_widgets()
{
    register_widget(SampleWidget::class);

    foreach (ChildConstant::$available_widgets as $widget) {
        $widget_registered = get_option('widget_' . $widget);
        if (empty($widget_registered)) {
            switch ($widget) {
                case ChildConstant::SAMPLE_WIDGET:
                    Extras\add_widget($widget);
                    break;
            }
        }
    }
}

add_action('init', __NAMESPACE__ . '\\add_widgets', 0);


function global_variables($vars)
{
    return array_merge($vars, [
        'sample message' => __('Sample message', 'wp-eli-child'),
    ]);
}

add_filter('wp_eli_global_variables', __NAMESPACE__ . '\\global_variables', 120, 1);