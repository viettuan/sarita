<?php

$wp_eli_child_includes = [
    'vendor/autoload.php',  // Scripts and stylesheets
    'lib/extras.php',      // Custom functions
    'lib/setup.php',      // Theme setup
];

foreach ($wp_eli_child_includes as $file) {
    $file = __DIR__ . DIRECTORY_SEPARATOR . $file;
    if (!file_exists($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'wp-eli-child'), $file), E_USER_ERROR);
    }

    require_once $file;
}