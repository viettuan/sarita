## Building core instruction
- Wiki: http://redmine.elidev.info/projects/arskan-site/wiki

## Deployment
- To install dummy data:
```
php wp-cli.phar deploy install
```

If you need more instruction, please run `php wp-cli.phar deploy --help`.