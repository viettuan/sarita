<?php

namespace Eli\GravityForm;

use GFFormsModel;
use Roots\Sage\Extras;

/**
 * Handling form which uses gravity form
 */
abstract class FormAbsHandler
{

    protected $_formId = 0;

    protected $_fieldIds = [];

    /**
     * @var array
     * Value of field index must be string.
     */
    protected $_fieldIndex = [];

    /**
     * FormAbsHandler constructor.
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function __construct()
    {
        add_filter('gform_post_submission_' . $this->_formId, [$this, 'submitForm'], 10, 2);
        add_action('gform_after_submission_' . $this->_formId, [$this, 'validateAndRedirectHandler'], 10, 2);
        add_filter('gform_validation_' . $this->_formId, [$this, 'customValidation']);
        add_filter('gform_validation_message_' . $this->_formId, [$this, 'customMessage'], 10, 2);
        add_filter('gform_confirmation_' . $this->_formId, [$this, 'customConfirmation']);
        add_filter('gform_disable_notification', [$this, 'disableNotification'], 10, 4);
        add_filter('gform_form_tag', [$this, 'formTagAutoComplete'], 11, 2);
        add_filter('gform_field_content', [$this, 'formInputAutoComplete'], 11, 5);

    }

    /**
     * Custom validation
     *
     * @param $validation_result
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    abstract public function customValidation($validation_result);


    /**
     * Save/update data to database
     *
     * @param $entry
     * @param $form
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    abstract public function submitForm($entry, $form);

    /**
     * @param $entry
     * @param $form
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    abstract public function validateAndRedirectHandler($entry, $form);

    /**
     * @param $confirmation
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    abstract public function customConfirmation($confirmation);

    /**
     * @param $message
     * @param $form
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function customMessage($message, $form)
    {

    }

    /**
     * Disable all notifications when after submit form
     * @param $is_disabled
     * @param $notification
     * @param $form
     * @param $entry
     * @return bool
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function disableNotification($is_disabled, $notification, $form, $entry)
    {
        return true;
    }

    /**
     * @param $form_tag
     * @param $form
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function formTagAutoComplete($form_tag, $form)
    {
        if (is_admin()) {
            return $form_tag;
        }
        if (GFFormsModel::is_html5_enabled() && Extras\isIOSUserAgent()) {
            $form_tag = str_replace('>', ' autocomplete="off"      >', $form_tag);
        } elseif (GFFormsModel::is_html5_enabled()) {
            $form_tag = str_replace('>', ' autocomplete="off" readonly  onfocus="this.removeAttribute(\'readonly\'); this.click(); " >', $form_tag);
        }
        return $form_tag;
    }

    /**
     * @param $input
     * @param $field
     * @param $value
     * @param $lead_id
     * @param $form_id
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function formInputAutoComplete($input, $field, $value, $lead_id, $form_id)
    {
        if (is_admin()) {
            return $input;
        }
        if (GFFormsModel::is_html5_enabled() && Extras\isIOSUserAgent()) {
            $input = preg_replace('/<(input|textarea)/', '<${1} autocomplete="off" ', $input);
        } elseif (GFFormsModel::is_html5_enabled()) {
            $input = preg_replace('/<(input|textarea)/', '<${1} autocomplete="off"  readonly  onfocus="this.removeAttribute(\'readonly\'); this.click();" ', $input);
        }
        return $input;
    }
}
