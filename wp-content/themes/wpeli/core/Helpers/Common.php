<?php

namespace Eli\Helpers;

class Common
{
    public static function activePlugins(array $plugins)
    {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';

        foreach ($plugins as $item) {
            $plugin = ABSPATH . 'wp-content/plugins/' . $item;
            if (is_plugin_inactive($plugin)) {
                activate_plugin($plugin);
            }
        }
    }
}