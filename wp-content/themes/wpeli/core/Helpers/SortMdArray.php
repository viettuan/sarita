<?php

namespace Eli\Helpers;

class SortMdArray
{
    // this class help to sort multidimensional array by key.
    public $sort_order = 'asc'; // default

    public $sort_key = 'position'; // default

    /**
     * @param $array ]
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function sortByKey(&$array)
    {
        usort($array, [__CLASS__, 'sortByKeyCallback']);
    }

    /**
     * @param $a
     * @param $b
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function sortByKeyCallback($a, $b)
    {
        $return = '';
        $v1 = is_object($a) ? $a->{$this->sort_key} : $a[$this->sort_key];
        $v2 = is_object($b) ? $b->{$this->sort_key} : $b[$this->sort_key];

        if ($this->sort_order == 'asc') {
            $return = $v1 - $v2;
        } else if ($this->sort_order == 'desc') {
            $return = $v2 - $v1;
        }
        return $return;
    }
}