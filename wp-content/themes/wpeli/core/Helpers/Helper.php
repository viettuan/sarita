<?php

namespace Eli\Helpers;

use WP_Query;

class Helper
{
    /**
     * @param $string
     * @param string $format
     * @return false|string
     * @author Sang Nguyen
     */
    public static function formatDateFromString($string, $format = 'Y/m/d')
    {
        return date_format(date_create_from_format('m/d/Y', $string), $format);
    }

    /**
     * @param string $value
     * @param string $post_type
     * @return false|int
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */
    public static function getIdByMetaValue($value = '', $post_type = 'post')
    {

        $id = 0;
        if (empty($value)) {
            return 0;
        }
        $args = [
            'post_type' => $post_type,
            'meta_query' => [
                [
                    'value' => $value
                ]
            ]
        ];
        $query = new WP_Query($args);
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $id = get_the_ID();
            }
        }
        wp_reset_postdata();

        return $id;
    }
}