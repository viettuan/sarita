<?php

namespace Eli\Helpers;

class Constant
{
    /**
     * Default pages
     */
    const RE_ACTIVE_THEME_PAGE = 'acf-options-re-active-theme';

    const USER_AGENT_IOS_DEVICES = 'wp_eli_ios_devices';
    const USER_AGENT_ANDROID_DEVICES = 'wp_eli_android_devices';
    const USER_AGENT_WEB_OS_DEVICES = 'wp_eli_web_os_devices';

    /**
     * Theme options
     */

    const TEST_FIELD = 123;

    public static $arrayOptionsFieldsKey = [
        self::TEST_FIELD
    ];

    /**
     * Post Type
     */
    const POST_PT = 'post';

    const POST_CATEGORY_COLOR = 'eli_post_category_color';
}