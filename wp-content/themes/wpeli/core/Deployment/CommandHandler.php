<?php

namespace Eli\Deployment;

class CommandHandler
{
    /**
     * Process command install dummy data
     * @author Sang Nguyen
     */
    public function install()
    {
        update_option('deployment_status', 'processing');

        @unlink(get_stylesheet_directory() . Deploy::BASE_PATH . '/logs/deploy.log');

        $command = 'php ' . locate_template(Deploy::BASE_PATH . '/wp-cli.phar') . ' deploy install';

        if (function_exists('system')) {
            system($command);
        } else {
            exec($command);
        }

        wp_send_json(['success' => true]);
    }

    /**
     * Check deploy status: "processing" or "done"
     * @author Sang Nguyen
     */
    public function checkDeployProcessing()
    {
        wp_send_json(['success' => true, 'type' => get_option('deployment_status'), 'message' => self::readLogFile()]);
    }

    /**
     * Read log file to show deploy process
     *
     * @return array|mixed|string
     * @author Sang Nguyen
     */
    protected static function readLogFile()
    {
        if (!file_exists(get_stylesheet_directory() . Deploy::BASE_PATH . '/logs/deploy.log')) {
            return [];
        }
        $content = @file_get_contents(get_stylesheet_directory() . Deploy::BASE_PATH. '/logs/deploy.log');
        $content = str_replace("\n", '<br />', $content);
        return $content;
    }
}