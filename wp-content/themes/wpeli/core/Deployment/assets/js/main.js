(function ($) {
    'use strict';
    var WpEliDeployment = {
        props: {
            installButton: $('.install_btn'),
            deployFlag: 'processing_deploy_install',
            buttonText: null
        },
        init: function () {

            this.props.buttonText = this.props.installButton.text();

            this.props.installButton.on('click', function (event) {
                event.preventDefault();

                localStorage.setItem(WpEliDeployment.props.deployFlag, 1);

                $.ajax({
                    type: 'POST',
                    url: WP_ELI_VARIABLES.ajax_url,
                    data: {
                        action: 'deploy_install'
                    },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        $(this).text(buttonText);
                    }
                });

                WpEliDeployment.checkingDeployProcess();
            });
        },
        checkingDeployProcess: function () {
            var deployInterval = 0;

            $('.deploy-process-notice').remove();

            if (localStorage.getItem(this.props.deployFlag) == 1) {
                WpEliDeployment.props.installButton.text('Processing ' + WpEliDeployment.props.buttonText);
                 deployInterval = setInterval(function () {
                    $.ajax({
                        type: 'POST',
                        url: WP_ELI_VARIABLES.ajax_url,
                        data: {
                            action: 'check_deploy_processing'
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data.type == 'done') {
                                clearInterval(deployInterval);
                                localStorage.setItem(WpEliDeployment.props.deployFlag, 0);
                                if ($('#wpbody-content .deploy-process-notice').length == 0) {
                                    $('#wpbody-content').prepend('<div class="notice notice-success deploy-process-notice" style="margin: 40px 0 5px;"><p>Deployment is done!</p></div>');
                                }
                                WpEliDeployment.props.installButton.text(WpEliDeployment.props.buttonText);
                            }
                            $('.deploy-process-detail').html('<p>' + data.message + '</p>');
                        },
                        error: function () {
                            clearInterval(deployInterval);
                            localStorage.setItem(WpEliDeployment.props.deployFlag, 0);
                        }
                    });
                }, 2000);
            } else {
                clearInterval(deployInterval);
                localStorage.setItem(this.props.deployFlag, 0);
            }
        }
    };

    WpEliDeployment.init();
    WpEliDeployment.checkingDeployProcess();
})(jQuery);