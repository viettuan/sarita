<?php

namespace Eli\Deployment;

use WP_CLI;
use WP_CLI_Command;

class Command extends WP_CLI_Command
{
    /**
     * Process install dummy data
     *
     * @author Sang Nguyen
     */
    public function install()
    {
        update_option('deployment_status', 'processing');

        $dummyDataObject = new DummyData();
        $this->log('Start deployment - 0%');

        $this->log('Deleted all pages and posts 3%');
        $dummyDataObject->resetData();
        $this->log('Reset old data - 5%');

        $this->log('Creating sample categories... - 6.45%');
        $dummyDataObject->dummyDataCategories();
        $this->log('Dummy data for categories - 7%');

        $dummyDataObject->dummyDataPost();
        $this->log('Dummy data for posts - 11%');

        $dummyDataObject->dummyDataPage();
        $this->log('Dummy data for pages - 17%');

        $dummyDataObject->dummyDataMenu();
        $this->log('Dummy data for menu - 47%');

        $this->activePlugin();
        $this->log('Active required plugin - 85%');

        $this->syncAcfPro();
        $this->log('Sync data for ACF Pro - 94%');

        do_action('wp_eli/install_dummy_data');

        $this->log('Create dummy data successfully! - 100%');

        update_option('deployment_status', 'done');
    }

    /**
     * Active default plugins
     * @author Sang Nguyen
     */
    private function activePlugin()
    {
        foreach ($this->getDataJson('plugin.json') as $plugin) {
            activate_plugin(ABSPATH . 'wp-content/plugins/' . $plugin['path']);
            $this->log('Active plugin ' . $plugin['name'] . ' successfully!');
        }
    }

    /**
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */
    private function syncAcfPro()
    {
        if (is_plugin_active('acf-pro-sync/acf-sync.php') && function_exists('acf_sync_import_json_folder')) {
            acf_sync_import_json_folder();
        }
    }

    /**
     * Save error to log file
     * @param $message
     * @param string $type - success, error
     * @author Sang Nguyen
     */
    protected function log($message, $type = 'success')
    {
        if (in_array($type, ['success', 'error', 'warning', 'log'])) {
            call_user_func_array([WP_CLI::class, $type], [$message]);
        }

        $path = get_stylesheet_directory() . Deploy::BASE_PATH . '/logs';
        if (!is_dir($path)) {
            wp_mkdir_p($path);
        }

        ini_set('error_log', $path . '/deploy.log');
        error_log($message);
    }

    /**
     * Run contractor, it's called in Autoload
     *
     * @author Sang Nguyen
     * @throws \Exception
     */
    public static function initialize()
    {
        WP_CLI::add_command('deploy', self::class, [
            'shortdesc' => 'Deployment command: install - install dummy data for fresh site',
            'when' => 'after_wp_load',
        ]);
    }
}
