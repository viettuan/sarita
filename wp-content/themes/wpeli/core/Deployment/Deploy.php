<?php

namespace Eli\Deployment;

class Deploy
{

    const BASE_PATH = '/core/Deployment';

    public function __construct()
    {
        add_action('admin_enqueue_scripts', [$this, 'registerScripts'], 130);
        add_action('wp_ajax_deploy_install', [CommandHandler::class, 'install']);
        add_action('wp_ajax_check_deploy_processing', [CommandHandler::class, 'checkDeployProcessing']);
        add_action('admin_menu', [$this, 'addAdminMenu']);
    }

    /**
     * Register scripts
     *
     * @author Sang Nguyen
     */
    public function registerScripts()
    {
        wp_register_script('deploy-js', get_template_directory_uri() . self::BASE_PATH . '/assets/js/main.js', ['jquery'], false, true);
        wp_enqueue_script('deploy-js');
    }

    /**
     * Add menu "Dummy Data" to menu Appearance
     * @author Sang Nguyen
     */
    public function addAdminMenu()
    {
        add_theme_page(
            'Dummy Data',
            'Dummy Data',
            'manage_options',
            'dummy-data',
            [$this, 'showDummyDataPage']
        );
    }

    /**
     * Show dummy data page
     * @author Sang Nguyen
     */
    public function showDummyDataPage()
    {
        require_once locate_template(self::BASE_PATH . '/templates/index.php');
    }

    /**
     * Run contractor, it's called in Autoload
     *
     * @author Sang Nguyen
     */
    public static function initialize()
    {
        return new self();
    }
}