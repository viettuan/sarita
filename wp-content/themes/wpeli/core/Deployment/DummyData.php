<?php

namespace Eli\Deployment;

use Eli\Helpers\Helper;
use WP_Query;

class DummyData
{
    /**
     * Reset all data
     * @author Sang Nguyen
     */
    public function resetData()
    {
        $the_query = new WP_Query([
            'post_status' => ['publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'],
            'post_type' => get_post_types(),
            'posts_per_page' => -1,
            'field' => 'ids',
        ]);

        while ($the_query->have_posts()) {
            $the_query->the_post();
            wp_delete_post(get_the_ID(), true);
        }

        $taxonomies = get_taxonomies(['public' => true, '_builtin' => false], 'names', 'and');
        if (!empty($taxonomies)) {
            foreach ($taxonomies as $taxonomy) {
                $terms = get_terms($taxonomy, ['fields' => 'ids', 'hide_empty' => false]);
                foreach ($terms as $value) {
                    wp_delete_term($value, $taxonomy);
                }
            }
        }

        foreach ($this->getDataJson('menu.json') as $menu) {
            foreach (array_keys($menu) as $item) {
                wp_delete_nav_menu($item);
            }
        }
    }

    /**
     * Random image in folder /data/images
     * @return string
     * @author Sang Nguyen
     */
    protected function getRandomImage()
    {
        return locate_template(Deploy::BASE_PATH . '/data/images/' . rand(1, 12) . '.jpg');
    }

    /**
     * Set feature image for post
     *
     * @param $image_url
     * @param $post_id
     * @author Sang Nguyen
     */
    protected function setFeaturedImage($image_url, $post_id)
    {
        $upload_dir = wp_upload_dir();
        $image_data = @file_get_contents($image_url);
        $filename = basename($image_url);

        if (wp_mkdir_p($upload_dir['path'])) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }
        @file_put_contents($file, $image_data);

        $wp_file_type = wp_check_filetype($filename, null);
        $attachment = [
            'post_mime_type' => $wp_file_type['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        ];
        $attach_id = wp_insert_attachment($attachment, $file, $post_id);

        require_once ABSPATH . 'wp-admin/includes/image.php';

        $attach_data = wp_generate_attachment_metadata($attach_id, $file);
        wp_update_attachment_metadata($attach_id, $attach_data);
        set_post_thumbnail($post_id, $attach_id);
    }

    public function dummyDataCategories()
    {
        foreach ($this->getDataJson('category.json') as $category) {
            wp_insert_category($category);
        }
    }

    public function dummyDataPost()
    {
        $this->migratePosts($this->getDataJson('post.json'));
    }

    public function dummyDataPage()
    {
        $this->migratePosts($this->getDataJson('page.json'));
    }

    private function migratePosts($data)
    {
        foreach ($data as $post) {
            $postExist = get_posts([
                'meta_key' => 'post_key',
                'meta_value' => $post['meta_data']['post_key'],
                'post_type' => $post['post_data']['post_type'],
                'posts_per_page' => 1
            ]);

            if (empty($postExist)) {
                $id = wp_insert_post($post['post_data']);
                if (empty($post['feature_image'])) {
                    $feature_image = $this->getRandomImage();
                } else {
                    $feature_image = $post['feature_image'];
                }

                $this->setFeaturedImage($feature_image, $id);

            } else {
                $id = $postExist[0]->ID;
                wp_update_post([
                    'ID' => $id,
                    'post_title' => $post['post_data']['post_title'],
                    'post_content' => $post['post_data']['post_content'] ?? '',
                ]);
            }

            // get all meta_data from page item in json
            if (!empty($post['meta_data'])) {
                foreach ($post['meta_data'] as $key => $value) {
                    update_post_meta($id, $key, $value);
                }
            }

            if (array_key_exists('page_on_front', $post) && $post['page_on_front'] == true) {
                update_option('page_on_front', $id);
                update_option('show_on_front', 'page');
            }
        }
    }

    public function dummyDataMenu()
    {
        foreach ($this->getDataJson('menu.json') as $menu) {
            foreach ($menu as $menuNameRoot => $menuData) {
                if(function_exists('qtrans_getSortedLanguages')) {
                    $langs = qtrans_getSortedLanguages();
                } else {
                    $langs = ['en'];
                }

                foreach ($langs as $lang) {
                    $menuName = "[" . strtoupper($lang) . "] " . $menuNameRoot;
                    $menu_exists = wp_get_nav_menu_object($menuName);

                    if (!$menu_exists) {
                        $menuId = wp_create_nav_menu($menuName); // create Menu
                    } else {
                        $menuId = $menu_exists->term_id;
                    }

                    if ($menuId) {
                        $navLocations = get_theme_mod('nav_menu_locations');
                        if (!empty($menuData['menu-info']['location'])) {
                            $navLocations[$menuData['menu-info']['location']] = $menuId;
                            set_theme_mod('nav_menu_locations', $navLocations); // add menu to theme location
                        }

                        $postIds = collection(json_decode(json_encode(wp_get_nav_menu_items($menuId)), true))->extract('object_id')->toArray();
                        foreach ($menuData['menu-data'] as $data) {
                            if (!empty($data['extra-data'])) {
                                $id = Helper::getIdByMetaValue($data['extra-data']['menu-item-object-key'], $data['item-data']['menu-item-object']);

                                if (!in_array($id, $postIds)) {
                                    $post = get_post($id);

                                    if (!empty($post)) {
                                        $data['item-data']['menu-item-object-id'] = $id;
                                        $data['item-data']['menu-item-title'] = $post->post_title;
                                        $data['item-data']['menu-item-position'] = $post->menu_order;
                                        $data['item-data']['menu-item-url'] = get_permalink($id);
                                    }

                                    wp_update_nav_menu_item($menuId, 0, $data['item-data']); // add new items into menu
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Get data array from json file
     *
     * @param $file
     * @return array|mixed|object
     * @author Sang Nguyen
     */
    protected function getDataJson($file)
    {
        if (!file_exists(locate_template(Deploy::BASE_PATH . '/data/' . $file))) {
            return [];
        }
        return json_decode(@file_get_contents(locate_template(Deploy::BASE_PATH . '/data/' . $file)), true);
    }
}
