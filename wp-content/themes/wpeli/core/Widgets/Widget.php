<?php
/**
 * Created by PhpStorm.
 * User: truong
 * Date: 24/11/2016
 * Time: 15:12
 */
namespace Eli\Widgets;

use WP_Widget;

class Widget extends WP_Widget
{

    protected $adminTemplatePath;

    protected $widgetTemplatePath;

    /**
     * Widget constructor.
     * @param string $widgetID
     * @param string $widgetName
     * @param array $widgetDesc
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function __construct($widgetID, $widgetName, $widgetDesc)
    {
        parent::__construct($widgetID, $widgetName, $widgetDesc);
    }


    /**
     * @param array $instance
     * @return void
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function form($instance)
    {
        ob_start();
        include $this->adminTemplatePath;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function update($new_instance, $old_instance)
    {
        // TODO: implement this function to update widget settings in sidebar area
    }

    /**
     * @param array $args
     * @param array $instance
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function widget($args, $instance)
    {
        ob_start();
        include $this->widgetTemplatePath;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }
}