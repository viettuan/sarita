<style>
    .form-table input[type=number] {
        width: 65px !important;
    }
</style>

<table class="form-table">
    <tr>
        <th scope="row"><?php _e('Thumbnail size') ?></th>
        <td>
            <label for="thumbnail_size_w"><?php _e('Width'); ?></label>
            <input name="thumbnail_size_w" type="number" step="1" min="0" id="thumbnail_size_w" value="<?php echo get_option('thumbnail_size_w', 150); ?>" class="small-text" />
            <label for="thumbnail_size_h"><?php _e('Height'); ?></label>
            <input name="thumbnail_size_h" type="number" step="1" min="0" id="thumbnail_size_h" value="<?php echo get_option('thumbnail_size_h', 150); ?>" class="small-text" />
            <div style="margin-top: 10px; font-size: 12px">
                <input name="thumbnail_crop" type="checkbox" id="thumbnail_crop" value="1" <?php checked('1', get_option('thumbnail_crop')); ?>/>
                <label for="thumbnail_crop"><?php _e('Crop thumbnail to exact dimensions (normally thumbnails are proportional)'); ?></label>
            </div>
        </td>
    </tr>

    <tr>
        <th scope="row"><?php _e('Medium size') ?></th>
        <td>
            <fieldset>
                <legend class="screen-reader-text"><span><?php _e('Medium size'); ?></span></legend>
                <label for="medium_size_w"><?php _e('Max Width'); ?></label>
                <input name="medium_size_w" type="number" step="1" min="0" id="medium_size_w" value="<?php echo get_option('medium_size_w', 450); ?>" class="small-text" />
                <label for="medium_size_h"><?php _e('Max Height'); ?></label>
                <input name="medium_size_h" type="number" step="1" min="0" id="medium_size_h" value="<?php echo get_option('medium_size_h', 350); ?>" class="small-text" />
            </fieldset>
        </td>
    </tr>

    <tr>
        <th scope="row"><?php _e('Large size') ?></th>
        <td>
            <fieldset>
                <legend class="screen-reader-text"><span><?php _e('Large size'); ?></span></legend>
                <label for="large_size_w"><?php _e('Max Width'); ?></label>
                <input name="large_size_w" type="number" step="1" min="0" id="large_size_w" value="<?php echo get_option('large_size_w', 1024) ?>" class="small-text" />
                <label for="large_size_h"><?php _e('Max Height'); ?></label>
                <input name="large_size_h" type="number" step="1" min="0" id="large_size_h" value="<?php echo get_option('large_size_h', 700) ?>" class="small-text" />
            </fieldset>
        </td>
    </tr>
</table>

<?php do_settings_fields('media', 'default'); ?>

<p class="description"><?php printf( __( 'Set beginning size for theme', 'wp-eli' ) ); ?></p>