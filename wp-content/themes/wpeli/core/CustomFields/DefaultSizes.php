<?php

namespace Eli\CustomFields;

use acf_field;

class  DefaultSizes extends acf_field
{

    protected $label;

    /*
    *  This function will setup the field type data
    *
    *  @param	n/a
    *  @return	n/a
    */
    public function __construct()
    {

        /*
        *  name (string) Single word, no spaces. Underscores allowed
        */
        $this->name = 'WP Eli Custom Field - Setup Default Image Sizes';

        /*
        *  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
        */
        $this->category = 'WP Eli';

        $this->label = 'Image sizes';

        // do not delete!
        parent::__construct();

    }

    /*
    *  Create the HTML interface for your field
    *
    *  @param	$field (array) the $field being rendered
    *  @param	$field (array) the $field being edited
    *  @return	n/a
    */
    public function render_field()
    {
        require_once __DIR__ . '/templates/media.php';
    }

    public static function initialize()
    {
        return new self();
    }
}