<?php
namespace Eli\Email;

use Roots\Sage\Extras;

class EmailAbstract
{

    protected $id;

    protected $title;

    /** @var string html template path */
    protected $template_html;

    /** @var string template path */
    protected $template_base;

    /** @var string recipients for the email */
    protected $recipient;

    /** @var string subject for the email */
    protected $subject;

    /** @var  string Email type */
    protected $email_type;

    /** @var  string Email heading */
    protected $heading;

    /** @var  string Email footer */
    protected $footer_text;

    /** @var  boolean Email Confirmation Feature */
    protected $enabled_email_confirmation;

    /** @var array strings to find in subjects/headings */
    protected $find;

    /** @var array strings to replace in subjects/headings */
    protected $replace;

    /**
     * EmailAbstract constructor.
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function __construct()
    {
        add_action('eli_email_header', [$this, 'emailHeader']);
        add_action('eli_email_footer', [$this, 'emailFooter']);
        add_filter('eli_email_subject_' . $this->id, [$this, 'changeSubject']);
    }

    /**
     * @param $subject
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function changeSubject($subject)
    {
        return str_replace(['{site_domain}'], [get_site_url()], $subject);
    }


    /**
     * Send the email.
     *
     * @access public
     * @param mixed $to
     * @param mixed $subject
     * @param mixed $message
     * @param string $headers
     * @param string $attachments
     * @return bool
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function send($to, $subject, $message, $headers, $attachments = '')
    {

        add_filter('wp_mail_from', [$this, 'getFromEmailAddress'], 99, 1);
        add_filter('wp_mail_from_name', [$this, 'getFromName']);
        add_filter('wp_mail_content_type', [$this, 'getContentType']);

        $return = wp_mail($to, $subject, $message, $headers, $attachments);

        remove_filter('wp_mail_from', [$this, 'getFromEmailAddress']);
        remove_filter('wp_mail_from_name', [$this, 'getFromName']);
        remove_filter('wp_mail_content_type', [$this, 'getContentType']);

        return $return;
    }

    /**
     * Get from email address
     * @param $from_email
     * @access public
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getFromEmailAddress($from_email)
    {
        $val = sanitize_email(get_option('admin_email'));
        return $val;
    }

    /**
     * Get from name
     *
     * @access public
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getFromName()
    {
        $from = get_option('blogname');

        return wp_specialchars_decode(esc_html(str_replace('{site_title}', get_bloginfo(), $from)), ENT_QUOTES);
    }

    /**
     * get_type function.
     *
     * @access public
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getEmailType()
    {
        return 'html';
        //return $this->email_type ? $this->email_type : 'plain';
    }

    /**
     * getContentType function.
     *
     * @access public
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getContentType()
    {
        switch ($this->getEmailType()) {
            case "html" :
                return 'text/html';
            case "multipart" :
                return 'multipart/alternative';
            default :
                return 'text/plain';
        }
    }

    /**
     * get_recipient function.
     *
     * @access public
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getRecipient()
    {
        return apply_filters('eli_email_recipient_' . $this->id, $this->recipient);
    }

    /**
     * get_subject function.
     *
     * @access public
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getSubject()
    {
        return apply_filters('eli_email_subject_' . $this->id, $this->subject);
    }

    /**
     * Get email content
     *
     * @access public
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getContent()
    {

    }

    /**
     * get_headers function.
     *
     * @access public
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getHeaders()
    {
        return apply_filters('eli_email_headers', "Content-Type: " . $this->getContentType() . "\r\n");
    }

    /**
     * Wraps a message in the  mail template.
     *
     * @access public
     * @param mixed $email_heading
     * @param mixed $message
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function wrapMessage($message, $email_heading = '')
    {
        // Buffer
        ob_start();

        do_action('eli_email_header', $email_heading);

        echo $message;

        do_action('eli_email_footer');

        // Get contents
        $message = ob_get_clean();

        return $message;
    }

    /**
     * @param string $email_heading
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function emailHeader($email_heading = '')
    {
        $this->heading = str_replace('{site_domain}', '<a style="color: #6495ed; text-decoration: none;" href="' . get_home_url() . '">' . get_bloginfo() . '</a>', $email_heading);
        require_once(__DIR__ . '/templates/email-header.php');
    }

    /**
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function emailFooter()
    {
        require_once(__DIR__ . '/templates/email-footer.php');
    }

    /**
     * @param array $arg
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getTemplateFile($arg = [])
    {
        $fileContent = Extras\getTemplatePartCustom($this->template_html, $arg, true);

        return $fileContent;
    }

    /**
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function numberParams()
    {
    }
}