<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- So that mobile will display zoomed in -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- enable media queries for windows phone 8 -->
    <meta name="format-detection" content="telephone=no">
    <!-- disable auto telephone linking in iOS -->
    <title><?php echo $this->heading ?></title>
    <style type="text/css">
    body {
        margin: 0;
        padding: 0;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
    }

    table {
        border-spacing: 0;
    }

    table td {
        border-collapse: collapse;
    }

    .ExternalClass {
        width: 100%;
    }

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
        line-height: 100%;
    }

    .ReadMsgBody {
        width: 100%;
        background-color: #ebebeb;
    }

    table {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
    }

    img {
        -ms-interpolation-mode: bicubic;
    }

    .yshortcuts a {
        border-bottom: none !important;
    }

    @media screen and (max-width: 599px) {
        table[class="force-row"],
        table[class="container"] {
            width: 100% !important;
            max-width: 100% !important;
        }
    }

    @media screen and (max-width: 599px) {
        td[class*="container-padding"] {
            padding-left: 12px !important;
            padding-right: 12px !important;
        }
    }

    .ios-footer a {
        color: #aaaaaa !important;
        text-decoration: underline;
    }
    </style>
</head>

<body style="margin:0; padding:0;" bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <!-- 100% background wrapper (grey background) -->
    <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
        <tr>
            <td align="center" valign="top" bgcolor="#ffffff" style="background-color: #ffffff;">
                <br>
                <!-- 600px container (white background) -->
                <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
                    <tr>
                        <td class="container-padding header" align="left">
                            <!--[if mso]>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr><td width="50%" valign="top"><![endif]-->
                                <table width="300" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row">
                                    <tr>
                                        <td class="col" valign="top">
                                            <img src="" alt="" />
                                        </td>
                                    </tr>
                                </table>
                                <!--[if mso]></td><td width="50%" valign="top"><![endif]-->
                                <table width="300" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row">
                                    <tr>
                                        <td class="col" valign="middle" align="left" style="padding-top:27px;color:#6495ed;font-weight:bold;font-size:18px;font-style:italic;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                            <?php echo $this->heading ?>
                                        </td>
                                    </tr>
                                </table>
                            <!--[if mso]></td></tr></table><![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size:0;line-height:0">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="20" style="border-top:3px solid #6495ed; font-size:0;line-height:0">&nbsp;</td>
                    </tr>
                    
                    <tr><td>