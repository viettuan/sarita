<div class="image" >
  <?php the_post_thumbnail( 'medium' ); ?>
</div>

<div class="content">
    <?php the_content(); ?>
</div>