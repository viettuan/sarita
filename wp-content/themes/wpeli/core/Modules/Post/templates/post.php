<div class="content-wrapper">
    <div class="main-content">
        <div class="dasboard">
            <h2> <?php _e($model->getNameArea('DASHBOARD'), 'wp_eli'); ?> </h2>
            <?php
                echo $model->renderDashBoard();
            ?>
        </div>
        <div class="silodata">
            <h2> <?php _e($model->getNameArea('SILODATA'), 'wp_eli'); ?></h2>
            <div class="event-btn search-event-name">
                <form method="get"   class="form-search form-search-event-name">
                    <input name="search" type="text" value="<?php echo  isset($_GET['search']) ? $_GET['search'] : "";?>"
                           placeholder="<?php _e('Search ...', 'wp_eli');?>" class="inp inp-txt solr-search">
                    <button type="submit" class="btn btn-link"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                </form>
            </div>

            <div class="blog-bkl" style="margin-top: 20px;">
                <nav class="blog-list">
                    <?php echo  $model->renderContentPost(); ?>
                </nav>
            </div>
            <?php
            $next_link = get_next_posts_link();
            $prev_link = get_previous_posts_link();

            if ($next_link || $prev_link):
                ?>
                <div class="blog-bkl">
                    <div class="blk-paging">
                        <span class="pg-tt"><?php _e('Pages', 'wp_eli') ?>:
                            <?php Roots\Sage\Extras\postPagingNav() ?>
                        </span>
                    </div>
                </div>

            <?php endif; ?>
        </div>
    </div>
</div>