<?php
/**
 * Class SM_Artist_Mod
 */
namespace Eli\Modules\Post;

use Eli\Modules\Post\Models\PostModel;
use Roots\Sage\Extras;

class PostModule
{

    public static function renderPostTemplate()
    {
        $askPostModel = new PostModel();
        if (is_single()) {
            $file = __DIR__ . '/templates/post-details.php';
        } else {
            $file = __DIR__ . '/templates/post.php';
        }
        Extras\getTemplatePartCustom($file, $askPostModel);
    }
}