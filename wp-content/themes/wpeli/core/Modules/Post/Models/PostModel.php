<?php
/**
 * Created by PhpStorm.
 * User: truong
 * Date: 23/09/2016
 * Time: 16:17
 */
namespace Eli\Modules\Post\Models;

use Eli\Helpers\Constant;
use Eli\Modules\Model;
use Roots\Sage\Extras;

class PostModel extends Model
{

    public $postId;

    /**
     * PostModel constructor.
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function __construct()
    {
        parent::__construct();
        $this->getDataPost();
    }

    /**
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    protected function getDataPost()
    {

    }

    /**
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function renderDashBoard()
    {

        $file = WP_ELI_MODULES_DIR . '/Post/templates/partials/dashboard.php';
        $data = Extras\getTemplatePartCustom($file, $this, true);

        return $data;
    }


    public function renderContentPost()
    {
        $data = '';
        $args = $this->getArgsForQueryPosts();
        query_posts($args);
        if (have_posts()) {
            while (have_posts()) : the_post();
                $this->postId = get_the_ID();

                $file = WP_ELI_MODULES_DIR . '/Post/templates/partials/content-post.php';
                $data .= Extras\getTemplatePartCustom($file, $this, true);
            endwhile;

            wp_reset_postdata();

        } else {
            $data = __("Don't have any post !", 'wp_eli');
        }

        return $data;
    }

    /**
     * @return array
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getArgsForQueryPosts()
    {

        $args = [
            'post_type' => ['post'],
            'post_status' => 'publish',
            'showposts' => get_option('posts_per_page'),
            'paged' => get_query_var('paged'),

        ];
        if (isset($_GET['search']) && !empty($_GET['search'])) {
            $keyword = $this->getKeyWord($_GET['search']);
            $args['s'] = $keyword;
        }
        if (!empty(get_queried_object()) && intval(get_queried_object()->cat_ID) > 0) {
            $args['cat'] = get_queried_object()->cat_ID;
        }

        return $args;
    }

    /**
     * @param $name
     * @return string
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */
    public function getNameArea($name)
    {
        if (empty($name)) {
            return '';
        }
        if (!empty(get_queried_object()) && get_queried_object()->name) {
            $name .= ' ' . get_queried_object()->name;
        }

        return strtoupper($name);

    }

    /**
     * @param $postId
     * @return string
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */
    public function getColorByCategory($postId)
    {
        if (empty($postId)) {
            return '';
        }
        $value = '';
        $categories = get_the_category($postId);
        if (!empty($categories)) {
            $term_id = $categories[0]->term_id;
            $term_meta = get_option("taxonomy_term_$term_id");
            $value = $term_meta ? $term_meta[Constant::POST_CATEGORY_COLOR] : '';
        }

        return $value;

    }

    public function getNumberObject($type)
    {
        switch ($type) {

            case 'interactive':
                return 32;
            case  'linked':
                return 32;
                break;
            case 'visit':
                return 32;
                break;
            default:
                return 32;
                break;
        }
    }

    public function getNumberScene($type)
    {
        switch ($type) {

            case 'related':
                return 15;
            case  'scripted':
                return 15;
                break;
            case 'interactive':
                return 15;
                break;
            default:
                return 15;
                break;
        }
    }

    public function getNumberGallery($type)
    {
        switch ($type) {

            case 'sceneries':
                return 15;
            case  'objects':
                return 15;
                break;
            case 'scenarios':
                return 15;
                break;
            default:
                return 15;
                break;
        }
    }


    public function getNumberVisit($type)
    {
        switch ($type) {

            case 'scenes':
                return 15;
            case  'object':
                return 15;
                break;
            case 'gallery':
                return 15;
                break;
            default:
                return 15;
                break;
        }
    }

}