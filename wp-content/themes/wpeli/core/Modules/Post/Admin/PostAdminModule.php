<?php

namespace Eli\Modules\Post\Admin;

/**
 * Removes and replaces the built-in taxonomy metabox with our radio-select metabox.
 * @link  http://codex.wordpress.org/Function_Reference/add_meta_box#Parameters
 */
class PostAdminModule
{

    // Post types where metabox should be replaced (defaults to all post_types associated with taxonomy)
    public $post_types = array();
    // Taxonomy slug
    public $slug = '';
    // Taxonomy object
    public $taxonomy = false;
    // New metabox title. Defaults to Taxonomy name
    public $metabox_title = '';
    // Metabox priority. (vertical placement)
    // 'high', 'core', 'default' or 'low'
    public $priority = 'high';
    // Metabox position. (column placement)
    // 'normal', 'advanced', or 'side'
    public $context = 'side';
    // Set to true to hide "None" option & force a term selection
    public $force_selection = false;


    /**
     * Initiates our metabox action
     * @param string $tax_slug Taxonomy slug
     * @param array $post_types post-types to display custom metabox
     */
    public function __construct($tax_slug, $post_types = array())
    {

        $this->slug = $tax_slug;
        $this->post_types = is_array($post_types) ? $post_types : array($post_types);

        add_action('add_meta_boxes', array($this, 'add_select_box'));
        add_action('save_post', array($this, 'save_taxonomy_data'));


    }

    /**
     * Removes and replaces the built-in taxonomy metabox with our own.
     */
    public function add_select_box()
    {
        foreach ($this->post_types() as $key => $cpt) {
            // remove default category type metabox
            remove_meta_box($this->slug . 'div', $cpt, 'side');
            // remove default tag type metabox
            remove_meta_box('tagsdiv-' . $this->slug, $cpt, 'side');
            // add our custom select box
            add_meta_box($this->slug . '_radio', $this->metabox_title(), array($this, 'select_box'), $cpt, $this->context, $this->priority);
        }
    }

    /**
     * Displays our taxonomy radio box metabox
     */
    public function select_box()
    {

        // uses same noncename as default box so no save_post hook needed
        wp_nonce_field('taxonomy_' . $this->slug, 'taxonomy_noncename');

        // get terms associated with this post
        $names = wp_get_object_terms(get_the_ID(), $this->slug);
        // get all terms in this taxonomy
        $terms = (array)get_terms($this->slug, 'hide_empty=0');
        // filter the ids out of the terms
        $existing = (!is_wp_error($names) && !empty($names))
            ? (array)wp_list_pluck($names, 'term_id')
            : array();
        // Check if taxonomy is hierarchical
        // Terms are saved differently between types
        $h = $this->taxonomy()->hierarchical;

        // default value
        $default_val = $h ? 0 : '';
        // input name
        $name = $h ? 'tax_input[' . $this->slug . '][]' : 'tax_input[' . $this->slug . ']';

        ?>

        <select name='<?php echo $name ?>' id='in-<?php echo $this->slug ?>_tax-0'>
            <!-- Display themes as options -->
            <option class='theme-option' value='' <?php if (!count($names)) echo "selected"; ?>>None</option>
            <?php
            foreach ($terms as $term) {
                if (!is_wp_error($names) && !empty($names) && !strcmp($term->slug, $names[0]->slug))
                    echo "<option class='theme-option' value='" . $term->slug . "' selected>" . $term->name . "</option>\n";
                else
                    echo "<option class='theme-option' value='" . $term->slug . "'>" . $term->name . "</option>\n";
            }
            ?>
        </select>
        <?php

    }

    /**
     * Gets the taxonomy object from the slug
     * @return object Taxonomy object
     */
    public function taxonomy()
    {
        $this->taxonomy = $this->taxonomy ? $this->taxonomy : get_taxonomy($this->slug);
        return $this->taxonomy;
    }

    /**
     * Gets the taxonomy's associated post_types
     * @return array Taxonomy's associated post_types
     */
    public function post_types()
    {
        $this->post_types = !empty($this->post_types) ? $this->post_types : $this->taxonomy()->object_type;
        return $this->post_types;
    }

    /**
     * Gets the metabox title from the taxonomy object's labels (or uses the passed in title)
     * @return string Metabox title
     */
    public function metabox_title()
    {
        $this->metabox_title = !empty($this->metabox_title) ? $this->metabox_title : $this->taxonomy()->labels->name;
        return $this->metabox_title;
    }

    /**
     * Save data
     * @param $post_id
     * @return mixed
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */
    function save_taxonomy_data($post_id)
    {
        // verify this came from our screen and with proper authorization.
        if (isset($_POST['taxonomy_noncename'])) {

            if (!wp_verify_nonce($_POST['taxonomy_noncename'], 'taxonomy_' . $this->slug)) {
                return $post_id;
            }
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
                return $post_id;
            }

            // Check permissions
            if ('page' == $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id)) {
                    return $post_id;
                }
            } else {
                if (!current_user_can('edit_post', $post_id)) {
                    return $post_id;
                }
            }

            $post = get_post($post_id);
            if (($post->post_type == 'post') || ($post->post_type == 'page')) {
                // OR $post->post_type != 'revision'
                if (!empty($_POST['tax_input'][$this->slug][0])) {
                    $category = $_POST['tax_input'][$this->slug][0];
                    wp_set_object_terms($post_id, $category, $this->slug);
                }

            }

        }

        return $post_id;
    }

    public static function initialize()
    {
        return new self('category');
    }
}