<?php
/**
 * Created by PhpStorm.
 * User: truong
 * Date: 28/09/2016
 * Time: 15:12
 */
namespace Eli\Modules;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Model
{

    /**
     * Model constructor.
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function __construct()
    {

    }

    /**
     * @param $obj
     * @return bool|string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getTitle($obj)
    {

        if (!$obj) return false;
        return $this->titleFilter($obj->post_title);
    }

    /**
     * Title rule
     *
     * @param $title
     * @return string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function titleFilter($title)
    {
        //we apply for listing page
        if (!is_single()) {
            $maxWords = 50;
            if (strlen($title) >= $maxWords) {
                return substr($title, 0, $maxWords) . '...';
            } else {
                return $title;
            }
        }
        return $title;
    }

    /**
     * @param string $taxonomy
     * @param $defaultValue
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getDefaultValueForTerm($taxonomy = '', $defaultValue)
    {
        $term = '';
        if (is_single() && !empty($taxonomy)) {
            global $post;
            $terms = get_the_terms($post, $taxonomy);
            $term = !empty($terms) ? $terms[0] : '';
        }
        return !empty($term) && is_object($term) ? $term->term_id : $defaultValue;
    }

    /**
     * @param string $post_id
     * @return false|string
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getImageUrl($post_id = '')
    {
        if (empty($post_id)) {
            return '';
        }
        $url = get_the_post_thumbnail_url($post_id, 'thumbnail');

        return $url ? $url : '';
    }

    /**
     * @param $keyWord
     * @return mixed
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function getKeyWord($keyWord)
    {
        global $wpdb;
        $wpdb->escape_by_ref($keyWord);

        return $keyWord;
    }
}
