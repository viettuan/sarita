<?php

namespace Eli\Modules;

use Eli\Helpers\Constant;
use Eli\Modules\Post\PostModule;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


class ModulesInit
{

    /**
     * ModulesInit constructor.
     * @param $type
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function __construct($type)
    {
        switch ($type) {
            case Constant::POST_PT:
                PostModule::renderPostTemplate();
                break;
        }
    }
}
