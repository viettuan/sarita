<?php

namespace Eli\ModuleAbstract;

class ModuleAdminAbstract
{
    protected $enable = true;
    protected $moduleName;
    protected $postType;
    protected $args;

    public function __construct($args = [])
    {
        $this->args = $args;

        add_action('init', array($this, 'registerPostType'), 5);
    }

    public static function initialize()
    {
        return new self();
    }

    public function registerPostType()
    {
        if (!$this->enable) {
            return false;
        }

        $this->createPostType($this->postType, $this->moduleName, $this->moduleName);
    }

    protected function createPostType($type, $singName, $pluralName)
    {
        $args = $this->args;
        if (post_type_exists($type)) {
            return;
        }

        $registerPostTypeData = [
            'labels' => [
                'name' => __($pluralName, 'wpeli'),
                'singular_name' => __($singName, 'wpeli'),
                'menu_name' => _x($pluralName, 'Admin menu name', 'wpeli'),
                'add_new' => __('Add New', 'wpeli'),
                'add_new_item' => __('Add New ' . $singName . '', 'wpeli'),
                'edit' => __('Edit', 'wpeli'),
                'edit_item' => __('Edit ' . $singName . '', 'wpeli'),
                'new_item' => __('New ' . $singName . ' ', 'wpeli'),
                'view' => __('View ' . $singName . '', 'wpeli'),
                'view_item' => __('View ' . $singName . '', 'wpeli'),
                'search_items' => __('Search ' . $pluralName . ' ', 'wpeli'),
                'not_found' => __('No ' . $pluralName . ' found', 'wpeli'),
                'not_found_in_trash' => __('No ' . $pluralName . ' found in trash', 'wpeli'),
                'parent' => __('Parent ' . $singName . '', 'wpeli')
            ],
            'description' => '',
            'public' => false,
            'has_archive' => true,
            'show_ui' => true,
            'map_meta_cap' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'hierarchical' => false, // Hierarchical causes memory issues - WP loads all records!
            'query_var' => true,
            'supports' => isset($this->args['support_fields']) ? $this->args['support_fields'] : ['title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes', 'revisions', 'publicize'],
            'show_in_nav_menus' => true,
            'menu_icon' => 'dashicons-buddicons-pm',
            'capabilities' => [
                'create_posts' => isset($this->args['enable_create_post']) ? ($this->args['enable_create_post']) : true,
            ],
        ];

        if (!empty($args)) {
            $registerPostTypeData = array_replace($registerPostTypeData, $args);
        }

        register_post_type($type,
            apply_filters('wpeli_register_post_type_' . $type . '', $registerPostTypeData)
        );
    }

    protected function createTaxonomy($args)
    {
        $type = $this->postType;

        if (taxonomy_exists($type)) {
            return;
        }

        register_taxonomy($type,
            apply_filters("wpeli_objects_{$type}", [$type]),
            apply_filters("wpeli_args_{$type}", [
                    'hierarchical' => true,
                    'label' => $this->moduleName,
                    'labels' => [
                        'name' => $args['tax_name'],
                        'singular_name' => $args['single_name'],
                        'menu_name' => $args['menu_name'],
                        'search_items' => $args['search_items'],
                        'all_items' => $args['all_items'],
                        'parent_item' => $args['parent_items'],
                        'parent_item_colon' => $args['parent_item_colon'],
                        'edit_item' => $args['edit_item'],
                        'update_item' => $args['update_item'],
                        'add_new_item' => $args['add_new_item'],
                        'new_item_name' => $args['new_item_name']
                    ],
                    'show_ui' => true,
                    'query_var' => true,
                    'public' => true,
                ]
            )
        );
    }
}
