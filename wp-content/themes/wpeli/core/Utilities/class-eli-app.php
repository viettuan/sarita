<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Eli_App
{
    public static function safeRedirect($url)
    {
        if (!headers_sent()) {
            wp_redirect($url);
        } else {
            echo "<script type='text/javascript'> window.location.href='" . $url . "' </script>";
            exit;
        }
    }
}
