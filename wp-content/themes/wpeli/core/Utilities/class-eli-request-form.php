<?php

if (!defined('ABSPATH')) exit;

class EliValidForm
{
    public function validate($request, $required)
    {
        $data = [];
        foreach ($required as $field => $condition) {
            $conditions = explode('|', $condition);
            if (!(strpos($condition, 'null') !== false && empty($request->get($field)))) {
                foreach ($conditions as $value) {
                    $error = $this->handleRequired($value, $request, $field);
                    if (!empty($error)) {
                        $data[$field] = $error;
                    }
                }
            }
        }

        return $data;
    }

    private function handleRequired($condition, $request, $field)
    {
        $error = '';
        $positionMiddle = strpos($condition, ':');
        if (empty($positionMiddle)) {
            $required['key'] = $condition;
        } else {
            $required['key'] = substr($condition, 0, $positionMiddle);
            $required['value'] = substr($condition, $positionMiddle + 1);
        }

        if (!empty($required['key'])) {
            $valueCheck = $request->get($field, null);
            $fieldTitle = str_replace('_', ' ', ucwords($field));
            switch ($required['key']) {
                case 'required':
                    if (empty($valueCheck))
                        $error = $fieldTitle . ' is required';
                    break;

                case 'limit':
                    if (!empty($required['value']))
                        if (strlen($valueCheck) > $required['value'])
                            $error = $fieldTitle . ' must be shorter in length ' . $required['value'] . ' words';
                    break;

                case 'unique':
                    if (!empty($required['value'])) {
                        global $wpdb;
                        $positionMiddle2 = strpos($required['value'], ':');
                        $check = null;
                        if (empty($positionMiddle2)) {
                            $where = $field . "= '" . $valueCheck . "'";
                            $table = $required['value'];
                        } else {
                            $id = substr($required['value'], $positionMiddle2 + 1);
                            $table = substr($required['value'], 0, $positionMiddle2);
                            $where = "id!=" . $id . ' AND ' . $field . "= '" . $valueCheck . "'";

                        }
                        $check = $wpdb->get_results(" SELECT * FROM {$table} WHERE {$where} LIMIT 1");

                        if (!empty($check)) {
                            $error = $fieldTitle . ' already exists';
                        }
                    }
                    break;

                case 'same' :
                    $fieldSame = $request->get($required['value'], null);

                    if (!empty($fieldSame) && (empty($valueCheck) || $valueCheck != $fieldSame)) {
                        $error = $fieldTitle . ' need same ' . $required['value'];
                    }
                    break;
            }
        }

        return $error;
    }
}
