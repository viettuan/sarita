<?php
/**
 * Created by PhpStorm.
 * User: truong
 * Date: 29/11/2016
 * Time: 15:02
 */
namespace Eli\ThemeOptions;

use Eli\Helpers\Constant;
use Roots\Sage\Setup;

class Settings
{
    /**
     * Settings constructor.
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function __construct()
    {
        add_action('acf/save_post', [$this, 'beforeAcfSavePost'], 1); // run before ACF saves the $_POST['fields'] data
        add_action('acf/save_post', [$this, 'afterAcfSavePost'], 20); // run after ACF saves the $_POST['fields'] data
    }

    /**
     * @param string $name
     * @param string $defaultValue
     * @param string $postId
     * @return mixed|null|string|void
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public static function getFieldByName($name = '', $defaultValue = '', $postId = '')
    {

        if (empty($postId)) {
            $post = get_post();
            $postId = !empty($post) ? $post->ID : 'option';
        }
        if (empty($name)) {
            return $defaultValue;
        }
        if (function_exists('get_field')) {
            $field = get_field($name, $postId);
        }

        return !empty($field) ? $field : $defaultValue;

    }

    /**
     * @param $post_id
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function afterAcfSavePost($post_id)
    {
        if (isset($_GET['page']) && $_GET['page'] == Constant::RE_ACTIVE_THEME_PAGE) {
            do_action('wp_eli_re_active_theme');
            Setup\widgets_init();
        }
    }

    /**
     * @param $post_id
     * @author TruongHN <truong.hoangnhat@elinext.com>
     */
    public function beforeAcfSavePost($post_id)
    {

    }

    /**
     * @return Settings
     * @author Sang Nguyen
     */
    public static function initialize()
    {
        return new self();
    }
}