<?php

namespace Eli;

use Eli\CustomFields\DefaultSizes;
use Eli\Deployment\Command;
use Eli\Deployment\Deploy;
use Eli\ThemeOptions\Settings;

class Autoload
{
    /**
     * Auto new class in core
     * @author Sang Nguyen
     */
    public static function init()
    {
        Settings::initialize();
        Deploy::initialize();

        if (class_exists('WP_CLI', false)) {
            Command::initialize();
        }

        if (class_exists('acf_field', false)) {
            DefaultSizes::initialize();
        }
    }
}
