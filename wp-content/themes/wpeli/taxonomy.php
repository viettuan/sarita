<?php
/**
 * Created by PhpStorm.
 * User: truong
 * Date: 12/12/2016
 * Time: 11:44
 */

use Eli\Modules\ModulesInit;

$postType = get_post_type();
if (!empty($postType)) {
    new ModulesInit($postType);
}
