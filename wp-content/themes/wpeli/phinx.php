<?php

$migrationsPathRegister = [
    '%%PHINX_CONFIG_DIR%%/db/migrations'
];

$seedPathRegister = [
    '%%PHINX_CONFIG_DIR%%/db/seeds'
];

return [
    'paths' => [
        'migrations' => $migrationsPathRegister,
        'seeds' => $seedPathRegister
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'development',
        'production' => [
            'adapter' => 'mysql',
            'host' => 'localhost',
            'name' => 'production_db',
            'user' => 'root',
            'pass' => '',
            'port' => '3306',
            'charset' => 'utf8',
            'table_prefix' => 'wp_'
        ],
        'development' => [
            'adapter' => 'mysql',
            'host' => 'localhost',
            'name' => 'sarita',
            'user' => 'root',
            'pass' => '123456',
            'port' => '3306',
            'charset' => 'utf8',
            'table_prefix' => 'wp_'
        ],
        'testing' => [
            'adapter' => 'mysql',
            'host' => 'localhost',
            'name' => 'testing_db',
            'user' => 'root',
            'pass' => '',
            'port' => '3306',
            'charset' => 'utf8',
            'table_prefix' => 'wp_'
        ]
    ],
    'version_order' => 'creation'
];