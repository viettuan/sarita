<?php

$wp_eli_includes = [
    'vendor/autoload.php',      // Scripts and stylesheets
    'lib/assets.php',          // Scripts and stylesheets
    'lib/extras.php',         // Custom functions
    'lib/setup.php',         // Theme setup
    'lib/titles.php',       // Page titles
    'lib/wrapper.php',     // Theme wrapper class
    'lib/customizer.php', // Theme customizer
    'lib/constants.php', // Define Constant
    'lib/handler.php', // Define Constant
    'lib/utilities.php',// Define Utilities
];

foreach ($wp_eli_includes as $file) {
    $file = __DIR__ . DIRECTORY_SEPARATOR . $file;
    if (!file_exists($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'wp-eli'), $file), E_USER_ERROR);
    }

    require_once $file;
}

if(!function_exists('dd')){
    function dd($args)
    {
        echo "<pre>";
        print_r($args);
        echo "</pre>";

        die;
    }
}

function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

function custom_body_classes( $classes ){
    if ( is_home()) {
        $classes[] = 'home-page';
    }
    return $classes;
}
add_filter( 'body_class', 'custom_body_classes' );

