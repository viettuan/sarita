
<?php
$attachmentBgImage    =  get_the_post_thumbnail_url($post->ID, 'full' );
$bgImageUrl = $attachmentBgImage  ? $attachmentBgImage : ""; ?>

<div  class="top-content" style="background-image: url('<?php echo $bgImageUrl?>'); background-size: cover;"  >
    <?php the_content(); ?>
</div>
<?php

 if( have_rows('features') ): ?>

    <ul class="slides">

        <?php while( have_rows('features') ): the_row();

            // vars
            $image = get_sub_field('feature_image');
            $title = get_sub_field('feature_title');
            $desc = get_sub_field('feature_description');

            ?>
            <li class="slide">

                <?php if( $image ): ?>
                <a href="#">
                    <img src="<?php echo $image; ?>" alt="<?php echo $image ?>" />
                </a>
                <?php endif; ?>

                <span><?php echo $title; ?></span>
                <p><?php echo $desc; ?></p>
            </li>

        <?php endwhile; ?>
    </ul>

<?php endif; ?>


