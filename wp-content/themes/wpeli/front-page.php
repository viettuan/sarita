<?php while (have_posts()) : the_post(); ?>
<!--    --><?php //get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'home'); ?>
<?php endwhile; ?>
<?php if ( is_active_sidebar( 'sidebar-primary' ) ) : ?>
    <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
        <?php dynamic_sidebar( 'sidebar-primary' ); ?>
    </div>
<?php endif; ?>