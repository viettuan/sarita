<?php

define('WP_CLI', true);
putenv('APP_DEBUG=false');
putenv('RUNNING_COMMAND=true');

if (WP_CLI) {
    $_SERVER['HTTP_HOST'] = 'host.local';
}
require_once __DIR__ . '/../../../../wp-load.php';

function checkExitsOrCreate($pathFile)
{
    if (!file_exists($pathFile)) {
        mkdir($pathFile, 0777, true);
    }
}


$pathFile = ROOT_CHILD_WPELI . '/admin/files/';
$enabledLanguages = get_option('qtranslate_enabled_languages');

checkExitsOrCreate($pathFile);

foreach ($enabledLanguages as $language){
    $pathJsonFE = $pathFile.$language.'.json';
    $pathJsonBE = $pathFile.'be_'.$language.'.json';
    checkExitsOrCreate($pathJsonFE);
    checkExitsOrCreate($pathJsonBE);

    $fileBe = json_decode(file_get_contents($pathJsonBE), true);
    $fileFe = json_decode(file_get_contents($pathJsonFE), true);

    $mergeArray = array_merge($fileFe, $fileBe);
    ksort($mergeArray);
    $mergeArray = json_encode($mergeArray, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    file_put_contents($pathJsonFE, $mergeArray);
}

