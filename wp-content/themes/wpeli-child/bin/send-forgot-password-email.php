<?php

use EliChild\Modules\StoreFront\Mails\UpdatePasswordMail;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../app.php';


(new UpdatePasswordMail())->sendMail([
    'email' => 'example@gmail.com',
    'name' => 'Kaka',
]);