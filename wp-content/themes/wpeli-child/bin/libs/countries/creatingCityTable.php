<?php

$statesJson = file_get_contents(__DIR__ . "/json/cities.json");
$statesArray = json_decode($statesJson, true);
// print_r($statesArray);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$conn->query("DROP TABLE IF EXISTS `cities`;");
$conn->query("CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `country_id` mediumint(8) UNSIGNED NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

createCityTable($statesArray, $conn);

//$conn->close();

function createCityTable($data, $conn)
{
    foreach ($data as $countryName => $countryCities) {
        $sql = "SELECT * FROM countries WHERE name='" . $countryName . "'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $countryData = $result->fetch_assoc();

            foreach ($countryCities as $cityName) {
                $cityName = mysqli_real_escape_string($conn, $cityName);
                $sql = "INSERT INTO cities (name, country_id) VALUES ('" . $cityName . "', '" . $countryData['id'] . "')";

                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully" . PHP_EOL;
                } else {
                    echo "Error: " . $sql . " " . $conn->error . PHP_EOL;
                }

                echo PHP_EOL;
            }
        }
    }
    echo '-------------------------------------' . PHP_EOL;
    echo 'Records Check Ends' . PHP_EOL;
    echo '-------------------------------------' . PHP_EOL;
}

?>
