<?php

$countriesJson = file_get_contents(__DIR__ . "/json/countries-full.json");
$countriesArray = json_decode($countriesJson, true);
// print_r($countriesArray);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$conn->query("DROP TABLE IF EXISTS `countries`;");
$conn->query("CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `iso2` char(2) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;");

createCountryTable($countriesArray, $conn);

$conn->close();

function createCountryTable($countriesArray, $conn)
{
    echo '-------------------------------------' . PHP_EOL;
    echo 'Records Check Starts' . PHP_EOL;
    echo '-------------------------------------' . PHP_EOL;

    foreach ($countriesArray as $country) :
        extract($country);

        $sql = "INSERT INTO countries (name, iso3, iso2, phonecode) VALUES ('$name', '$iso3', '$iso', '$phonecode')";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully" . PHP_EOL;
        } else {
            echo "Error: " . $sql . " " . $conn->error . PHP_EOL;
        }

        echo PHP_EOL;
    endforeach;

    echo '-------------------------------------' . PHP_EOL;
    echo 'Records Check Ends' . PHP_EOL;
    echo '-------------------------------------' . PHP_EOL;
}

?>