<?php

$statesJson = file_get_contents(__DIR__ . "/json/states.json");
$statesArray = json_decode($statesJson, true);
// print_r($statesArray);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$conn->query("DROP TABLE IF EXISTS `states`;");
$conn->query("CREATE TABLE `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(50) NOT NULL,
  `country_code` varchar(20) NOT NULL,
  `country_id` mediumint(8) UNSIGNED NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

createStateTable($statesArray, $conn);

$conn->close();

function createStateTable($data, $conn)
{
    foreach ($data as $countryState) {
        $sql = "SELECT * FROM countries WHERE iso2='" . ($countryState['countryShortCode'] ?? '') . "'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $countryData = $result->fetch_assoc();
            foreach ($countryState['regions'] as $state) {
                if (!empty($state['shortCode'])) {
                    $stateName = mysqli_real_escape_string($conn, $state['name']);
                    $sql = "INSERT INTO states (name, code, country_code, country_id) VALUES ('" . $stateName . "', '" . $state['shortCode'] . "', '" . $countryState['countryShortCode'] . "', '" . $countryData['id'] . "')";

                    if ($conn->query($sql) === TRUE) {
                        echo "New record created successfully" . PHP_EOL;
                    } else {
                        echo "Error: " . $sql . " " . $conn->error . PHP_EOL;
                    }
                }

                echo PHP_EOL;
            }
        }
    }
    echo '-------------------------------------' . PHP_EOL;
    echo 'Records Check Ends' . PHP_EOL;
    echo '-------------------------------------' . PHP_EOL;
}

?>