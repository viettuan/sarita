<?php
    use Eli\ThemeOptions\Settings;
?>
<footer class="content-info">
  <nav class="nav-footer">
    <?php

    if (has_nav_menu('footer_menu')) :
      wp_nav_menu(['theme_location' => 'footer_menu', 'menu_class' => 'nav']);
    endif;
    ?>
  </nav>
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
  <div class="copy-right">
      <p><?php echo Settings::getFieldByName('copyright','','option');  ?></p>
  </div>

</footer>
