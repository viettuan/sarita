<?php
use \Roots\Sage\Extras;
?>

<header class="banner">
  <div class="container">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
      <img src="<?php echo Extras\getLogoImage();?>" >
    </a>
    <nav class="nav-primary">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>
    <?php if(is_user_logged_in()) {  ?>
      <a href="javascript:void(0)" id="ask-logout">Logout</a>
    <?php } else{ ?>
      <a href="javascript:void(0)" id="ask-login">Login</a>
    <?php } ?>
  </div>
</header>
