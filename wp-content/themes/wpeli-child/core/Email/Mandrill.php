<?php

namespace EliChild\Email;

use EliChild\Helpers\Helper;

abstract class Mandrill
{
    protected $mandrill = null;

    protected function send($data)
    {
        try {
            $this->mandrill = new \Mandrill(getenv('MANDRILL_APIKEY'));

            $templateName = $data['template_name'];
            $templateContent = $data['template_content'] ?? [];
            $email = $data['email'] ?? '';
            $name = $data['name'] ?? '';
            $subject = $data['subject'] ?? '';
            $variables = array_merge($data['vars'] ?? [], $this->getVariablesDefault());
            $emailsCC = Helper::getEmailCC($templateName);

            $emailsTo = [
                [
                    'email' => $email,
                    'name' => $name,
                    'type' => 'to'
                ]
            ];

            if(count($emailsCC) > 0) {
                foreach ($emailsCC as $emailCC) {
                    $emailsTo[] = [
                        'email' => $emailCC,
                        'name' => $emailCC,
                        'type' => 'cc'
                    ];
                }
            }

            $message = [
                'subject' => $subject,
                'from_email' => Helper::getEmailFrom(),
                'to' => $emailsTo,
                'global_merge_vars' => $variables
            ];

            //$response = $this->mandrill->templates->render($templateName, $templateContent, $variables);
            $this->mandrill->messages->sendTemplate($templateName, $templateContent, $message);
        } catch (\Exception $exception) {

        }
    }

    private function getVariablesDefault()
    {
        $variables = [
            [
                'name' => 'current_year',
                'content' => date('Y')
            ],
            [
                'name' => 'list_address_html',
                'content' => 'kontakt@sarita.dk'
            ]
        ];

        return $variables;
    }
}