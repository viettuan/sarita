<?php

namespace EliChild\Email;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

abstract class EliMailer
{
    protected function send($data)
    {
        $email = $data['email'] ?? '';
        $name = $data['name'] ?? '';
        $subject = $data['subject'] ?? '';
        $html = $data['html'] ?? '';

        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 0;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host = getenv('MAIL_HOST');                          // Set the SMTP server to send through
            $mail->SMTPAuth = true;                                     // Enable SMTP authentication
            $mail->Username = getenv('MAIL_USERNAME');                  // SMTP username
            $mail->Password = getenv('MAIL_PASSWORD');                  // SMTP password
            $mail->SMTPSecure = getenv('MAIL_ENCRYPTION');              // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = getenv('MAIL_PORT');                          // TCP port to connect to

            //Recipients
            $mail->setFrom(getenv('MAIL_FROM'), getenv('MAIL_FROM_NAME'));
            $mail->addAddress($email, $name);     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $html;

            $mail->send();

            return true;
        } catch (Exception $e) {
            return false;
            //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}