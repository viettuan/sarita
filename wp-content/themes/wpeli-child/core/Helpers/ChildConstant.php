<?php

namespace EliChild\Helpers;

use Eli\Helpers\Constant;


class ChildConstant extends Constant
{
    const SAMPLE_MODULE = 'sample-module';
    const SAMPLE_WIDGET = 'eli_sample_widget';

    public static $available_widgets = [
        self::SAMPLE_WIDGET,
    ];

    /**
     * Pearl module
     */
    const PEARL_MODULE_ENABLE = true;
    const PEARL_MODULE_NAME = 'Pearl Design';
    const PEARL_MODULE_POST_TYPE = 'pearl';

    /**
     * Customer module
     */
    const CUSTOMER_MODULE_ENABLE = true;
    const CUSTOMER_MODULE_NAME = 'Customers';
    const CUSTOMER_MODULE_POST_TYPE = 'customer';

    /**
     * Accessory
     */
    const ACCESSORY_PT_ENABLE = true;
    const ACCESSORY_PT_NAME = 'Accessories';
    const ACCESSORY_PT_KEY = 'accessory';

    /**
     * Testimonial
     */
    const TESTIMONIAL_PT_ENABLE = true;
    const TESTIMONIAL_PT_NAME = 'Testimonials';
    const TESTIMONIAL_PT_KEY = 'testimonial';

    /**
     * Order module
     */
    const ORDER_MODULE_ENABLE = true;
    const ORDER_MODULE_NAME = 'Orders';
    const ORDER_MODULE_POST_TYPE = 'orders';

    /**
     * Api logs module
     */
    const API_LOGS_MODULE_ENABLE = true;
    const API_LOGS_MODULE_NAME = 'Api Logs';
    const API_LOGS_MODULE_SLUG = 'api-logs';
}