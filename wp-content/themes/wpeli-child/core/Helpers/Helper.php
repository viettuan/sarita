<?php

namespace EliChild\Helpers;


class Helper
{
    public static function getValueByMeta($meta, $name, $valueDefault = '')
    {
        return $meta[$name][0] ?? $valueDefault;
    }

    public static function getToday()
    {
        return date('Y-m-d');
    }

    public static function getEmailFrom()
    {
        $email = get_field('email_from', 'option');
        if (empty($email)) {
            $email = getenv('MANDRILL_FROM_EMAIL');
        }

        return $email;
    }

    public static function getEmailCC($templateName)
    {
        if (in_array($templateName, ['purchase-email-with-password', 'purchase-email-for-existing-customers', 'cancelled-subscription'])) {
            $emails = get_field('email_cc', 'option');
            if (empty($emails)) {
                $emails = getenv('MANDRILL_CC');
            }

            return explode(",", $emails);
        }

        return [];
    }
}