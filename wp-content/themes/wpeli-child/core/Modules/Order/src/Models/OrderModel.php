<?php

namespace EliChild\Modules\Order\Models;

use EliChild\Helpers\ChildConstant;
use EliChild\Helpers\Helper;
use EliChild\Modules\Customer\Models\CustomerModel;
use EliChild\Modules\Order\Helpers\OrderConstant;
use EliChild\Modules\StoreFront\Events\CancelledSubscriptionEvent;
use EliChild\Modules\StoreFront\Events\OrderPurchaseEvent;
use WP_Query;

class OrderModel
{
    protected $id;
    protected $title;
    protected $status;
    protected $date;
    protected $validUntil;
    protected $saritaId;
    protected $customerId;
    protected $name;
    protected $surname;
    protected $email;
    protected $phoneCode;
    protected $phone;
    protected $address;
    protected $country;
    protected $state;
    protected $city;
    protected $zipcode;
    protected $productId;
    protected $pearlName;
    protected $pearlDesignCode;
    protected $prePayment;
    protected $monthlyPayment;
    protected $currency;
    protected $paymentMethod;
    protected $subscriptionType;
    protected $pearlId;
    protected $saritaSubscriptionId;

    protected $postType = ChildConstant::ORDER_MODULE_POST_TYPE;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param mixed $validUntil
     */
    public function setValidUntil($validUntil): void
    {
        $this->validUntil = $validUntil;
    }

    /**
     * @return mixed
     */
    public function getSaritaId()
    {
        return $this->saritaId;
    }

    /**
     * @param mixed $saritaId
     */
    public function setSaritaId($saritaId): void
    {
        $this->saritaId = $saritaId;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->getName() . ' ' . $this->getSurname();
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    /**
     * @param mixed $phoneCode
     */
    public function setPhoneCode($phoneCode): void
    {
        $this->phoneCode = $phoneCode;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode): void
    {
        $this->zipcode = $zipcode;
    }

    public function getFullAddress()
    {
        $state = !empty($this->getState()) ? $this->getState() . ',' : '';

        return $this->getAddress() . ', ' .
            $this->getCity() . ', '
            . $state .
            $this->getZipcode() . ', ' .
            $this->getCountry();
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getPearlName()
    {
        return $this->pearlName;
    }

    /**
     * @param mixed $pearlName
     */
    public function setPearlName($pearlName): void
    {
        $this->pearlName = $pearlName;
    }

    /**
     * @return mixed
     */
    public function getPearlDesignCode()
    {
        return $this->pearlDesignCode;
    }

    /**
     * @param mixed $pearlDesignCode
     */
    public function setPearlDesignCode($pearlDesignCode): void
    {
        $this->pearlDesignCode = $pearlDesignCode;
    }

    /**
     * @return mixed
     */
    public function getPrePayment()
    {
        return $this->prePayment;
    }

    /**
     * @param mixed $prePayment
     */
    public function setPrePayment($prePayment): void
    {
        $this->prePayment = $prePayment;
    }

    /**
     * @return mixed
     */
    public function getMonthlyPayment()
    {
        return $this->monthlyPayment;
    }

    /**
     * @param mixed $monthlyPayment
     */
    public function setMonthlyPayment($monthlyPayment): void
    {
        $this->monthlyPayment = $monthlyPayment;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     */
    public function setPaymentMethod($paymentMethod): void
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionType()
    {
        return $this->subscriptionType;
    }

    /**
     * @param mixed $subscriptionType
     */
    public function setSubscriptionType($subscriptionType): void
    {
        $this->subscriptionType = $subscriptionType;
    }

    /**
     * @return mixed
     */
    public function getPearlId()
    {
        return $this->pearlId;
    }

    /**
     * @param mixed $pearlId
     */
    public function setPearlId($pearlId): void
    {
        $this->pearlId = $pearlId;
    }

    /**
     * @return mixed
     */
    public function getSaritaSubscriptionId()
    {
        return $this->saritaSubscriptionId;
    }

    /**
     * @param mixed $saritaSubscriptionId
     */
    public function setSaritaSubscriptionId($saritaSubscriptionId): void
    {
        $this->saritaSubscriptionId = $saritaSubscriptionId;
    }

    public function __construct($orderId = null)
    {
        if (!empty($orderId)) {
            $this->setDetail($orderId);
        }
    }

    public function setDetail($id)
    {
        $post = get_post($id);
        $metas = get_post_meta($id, null, true);

        $this->setId($post->ID ?? '');
        $this->setName($post->post_title ?? '');
        $this->setStatus(Helper::getValueByMeta($metas, 'status'));
        $this->setDate($post->post_date);
        $this->setValidUntil(Helper::getValueByMeta($metas, 'valid_until'));
        $this->setSaritaId(Helper::getValueByMeta($metas, 'sarita_id'));
        $this->setCustomerId(Helper::getValueByMeta($metas, 'customer_id'));
        $this->setName(Helper::getValueByMeta($metas, 'name'));
        $this->setSurname(Helper::getValueByMeta($metas, 'surname'));
        $this->setEmail(Helper::getValueByMeta($metas, 'email'));
        $this->setPhoneCode(Helper::getValueByMeta($metas, 'phoneCode'));
        $this->setPhone(Helper::getValueByMeta($metas, 'phone'));
        $this->setAddress(Helper::getValueByMeta($metas, 'address'));
        $this->setCountry(Helper::getValueByMeta($metas, 'country'));
        $this->setState(Helper::getValueByMeta($metas, 'state'));
        $this->setCity(Helper::getValueByMeta($metas, 'city'));
        $this->setZipcode(Helper::getValueByMeta($metas, 'zipcode'));
        $this->setProductId(Helper::getValueByMeta($metas, 'product_id'));
        $this->setPearlDesignCode(Helper::getValueByMeta($metas, 'pearl_design_code'));
        $this->setPrePayment(Helper::getValueByMeta($metas, 'pre_payment'));
        $this->setMonthlyPayment(Helper::getValueByMeta($metas, 'monthly_payment'));
        $this->setCurrency(Helper::getValueByMeta($metas, 'currency'));
        $this->setPaymentMethod(Helper::getValueByMeta($metas, 'payment_method'));
        $this->setSubscriptionType(Helper::getValueByMeta($metas, 'subscription_type'));
        $this->setPearlName(Helper::getValueByMeta($metas, 'pearl_name'));
        $this->setPearlId(Helper::getValueByMeta($metas, 'pearl_id'));
        $this->setSaritaSubscriptionId(Helper::getValueByMeta($metas, 'sarita_subscription_id'));
    }

    public function createOrder($data)
    {
        $orderId = wp_insert_post([
            'post_title' => 'Order #',
            'post_content' => '',
            'post_status' => 'publish',
            'post_type' => $this->postType
        ]);

        $data['status'] = OrderConstant::STATUS_OPEN;
        $this->updateOrderAttributes($orderId, $data);

        return $orderId;
    }

    public function updateOrder($orderId, $data)
    {
        $this->updateOrderAttributes($orderId, $data);
    }

    private function updateOrderAttributes($orderId, $data)
    {
        wp_update_post([
            'ID' => $orderId,
            'post_title' => 'Order #' . $orderId,
        ]);

        update_post_meta($orderId, 'sarita_id', $data['customer']['sarita_id'] ?? '');
        update_post_meta($orderId, 'customer_id', $data['customer']['id'] ?? '');
        update_post_meta($orderId, 'name', $data['customer']['name'] ?? '');
        update_post_meta($orderId, 'surname', $data['customer']['surname'] ?? '');
        update_post_meta($orderId, 'email', $data['customer']['email'] ?? '');
        update_post_meta($orderId, 'phoneCode', $data['customer']['phoneCode'] ?? '');
        update_post_meta($orderId, 'phone', $data['customer']['phone'] ?? '');

        update_post_meta($orderId, 'address', $data['shipping']['address'] ?? '');
        update_post_meta($orderId, 'country', $data['shipping']['country'] ?? '');
        update_post_meta($orderId, 'state', $data['shipping']['state'] ?? '');
        update_post_meta($orderId, 'city', $data['shipping']['city'] ?? '');
        update_post_meta($orderId, 'zipcode', $data['shipping']['zipcode'] ?? '');

        update_post_meta($orderId, 'billing_address', $data['billing-address']['address'] ?? '');
        update_post_meta($orderId, 'billing_country', $data['billing-address']['country'] ?? '');
        update_post_meta($orderId, 'billing_state', $data['billing-address']['state'] ?? '');
        update_post_meta($orderId, 'billing_city', $data['billing-address']['city'] ?? '');
        update_post_meta($orderId, 'billing_zipcode', $data['billing-address']['zipcode'] ?? '');

        update_post_meta($orderId, 'pre_payment', $data['pre_payment'] ?? '');
        update_post_meta($orderId, 'monthly_payment', $data['monthly_payment'] ?? '');
        update_post_meta($orderId, 'currency', $data['currency'] ?? '');
        update_post_meta($orderId, 'pearl_id', $data['pearl_id'] ?? '');
        update_post_meta($orderId, 'pearl_name', $data['pearl_name'] ?? '');
        update_post_meta($orderId, 'pearl_design_code', $data['pearl_code'] ?? '');
        update_post_meta($orderId, 'subscription_type', $data['subscription_type'] ?? '');
        update_post_meta($orderId, 'payment_method', $data['payment_method'] ?? '');
        if (isset($data['status'])) {
            update_post_meta($orderId, 'status', $data['status']);
        }
    }

    public function awaitingToPaymentOrder($orderId)
    {
        update_post_meta($orderId, 'status', OrderConstant::STATUS_WAITING);
    }

    public function paidOrder($orderId, $data)
    {
        $this->setDetail($orderId);
        $isGuest = false;

        if ($this->getStatus() != OrderConstant::STATUS_COMPLETED) {
            switch ($this->getSubscriptionType()) {
                case OrderConstant::SUBSCRIPTION_ONE_OFF:
                    $dateValidUntil = null;
                    break;

                case OrderConstant::SUBSCRIPTION_MONTHLY:
                    $dateValidUntil = Date('Y-m-d', strtotime("+1 months"));;
                    break;

                case OrderConstant::SUBSCRIPTION_ANNUAL:
                    $dateValidUntil = Date('Y-m-d', strtotime("+1 years"));
                    break;

                default:
                    $dateValidUntil = null;
                    break;
            }

            update_post_meta($orderId, 'status', OrderConstant::STATUS_COMPLETED);
            update_post_meta($orderId, 'valid_until', $dateValidUntil);
            update_post_meta($orderId, 'payment_info', json_encode($data ?? []));

            //create new customer for guest
            if (empty($this->getSaritaId())) {
                $customerModel = new CustomerModel();
                $customerModel->createCustomerFromDraft($this->getCustomerId());
                $isGuest = true;
            }

            event(new OrderPurchaseEvent([
                'order_id' => $orderId,
                'customer_guest' => $isGuest
            ]));

            return true;
        }

        return false;
    }

    public function cancelledOrder($orderId)
    {
        $checkCancel = $this->checkConditionCancel($orderId);
        if (!$checkCancel) {
            return false;
        }

        update_post_meta($orderId, 'status', OrderConstant::STATUS_CANCELED);

        event(new CancelledSubscriptionEvent([
            'order_id' => $orderId,
        ]));

        return true;
    }

    public function checkConditionCancel($orderId)
    {
        $this->setDetail($orderId);
        if ($this->getSubscriptionType() == OrderConstant::SUBSCRIPTION_ANNUAL) {
            $cycleDate = date('Y-m-d', strtotime('+1 year', strtotime($this->getDate())));
            if (strtotime($cycleDate) < time()) {
                return false;
            }
        }
        return true;
    }

    public function getSubscriptionQuickPay($orderId)
    {
        $meta = get_post_meta($orderId, 'subscription_quickpay', true);

        if ($meta) {
            $meta = json_decode($meta);

            return $meta->id;
        }

        return null;
    }

    public function getOrderSummary()
    {
        $data = [
            'status' => [],
            'sub' => [],
            'total' => 0
        ];
        $statusList = [
            OrderConstant::STATUS_OPEN,
            OrderConstant::STATUS_WAITING,
            OrderConstant::STATUS_COMPLETED,
            OrderConstant::STATUS_CANCELED
        ];

        $subList = [
            OrderConstant::SUBSCRIPTION_MONTHLY,
            OrderConstant::SUBSCRIPTION_ONE_OFF,
            OrderConstant::SUBSCRIPTION_ANNUAL
        ];

        foreach ($statusList as $status) {
            $args = [
                'posts_per_page' => -1,
                'post_type' => $this->postType,
                'meta_key' => 'status',
                'meta_value' => $status
            ];

            $totalPost = (new WP_Query($args))->found_posts;
            $data['status'][$status] = $totalPost;
            $data['total'] += $totalPost;
        }

        foreach ($subList as $subType) {
            $args = [
                'posts_per_page' => -1,
                'post_type' => $this->postType,
                'meta_key' => 'subscription_type',
                'meta_value' => $subType
            ];

            $totalPost = (new WP_Query($args))->found_posts;
            $data['sub'][$subType] = $totalPost;
        }

        return $data;
    }

    public function getSubscriptionIDs($customerId)
    {
        $orders = $this->getFieldsByCondition([
            'customer_id' => $customerId
        ]);
        return $orders;
    }

    private function getAllFields()
    {
        $fields = [];
        $groups = acf_get_field_groups([
            'post_type' => $this->postType
        ]);

        if (isset($groups[0]['ID'])) {
            $fields = acf_get_fields($groups[0]['ID']);
        }

        return $fields;
    }

    /**
     * Get fields by condition
     * @param $conditions : array conditions
     * @param $limit : number post
     * @return array|mixed
     */
    public function getFieldsByCondition($conditions, $limit = null)
    {
        $where = [];
        if (!empty($conditions)) {
            foreach ($conditions as $key => $arg) {
                $where[] = [
                    'key' => $key,
                    'value' => $arg
                ];
            }
            if (count($conditions) > 1) {
                $where['relation'] = 'AND';
            }
        }

        $query = [
            'post_type' => $this->postType,
            'meta_query' => $where,
            'status' => 'published'
        ];
        if ($limit) {
            $query['numberposts'] = $limit;
        }

        $result = new \WP_Query($query);
        $result = $result->get_posts();

        $fields = [];
        foreach ($result as $key => $item) {
            $fields[$key] = $item->ID;
        }
        return $fields;
    }
}
