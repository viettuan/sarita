<?php

namespace EliChild\Modules\Order\Providers;

use EliChild\Modules\Base\Traits\LoadAndPublishDataTrait;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class OrderServiceProvider implements ServiceProviderInterface, BootableProviderInterface
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Silex\Application
     */
    protected $app;

    public function boot(Application $app)
    {
        $this->app = $app;

        $this->setIsInConsole($this->runningInConsole())
            ->setNamespace('modules.Order')
            ->loadMigrations();
    }

    public function register(Container $container)
    {

    }
}
