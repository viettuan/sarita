<?php

namespace EliChild\Modules\Order\Helpers;

class OrderConstant
{
    /**
     * Order status
     */
    const STATUS_OPEN = 'open';
    const STATUS_WAITING = 'waiting-to-payment';
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELED = 'canceled';

    /**
     * Type subscription
     */
    const SUBSCRIPTION_ONE_OFF = 'one_off';
    const SUBSCRIPTION_ANNUAL = 'annual';
    const SUBSCRIPTION_MONTHLY = 'monthly';

    /**
     * Checkout detail
     */
    const DELIVERY_FEE = '0,00';
    const SHIPPING_FEE = '0,00';
}