<?php

namespace EliChild\Modules\Order;

use Eli\ModuleAbstract\ModuleAdminAbstract;
use EliChild\Helpers\ChildConstant;
use EliChild\Modules\Order\Helpers\OrderConstant;
use EliChild\Modules\Order\Models\OrderModel;
use PayPal\Api\Order;

class OrderAdminModule extends ModuleAdminAbstract
{
    protected $status;
    protected $subscriptionType;

    public static function initialize()
    {
        return new self();
    }

    public function __construct($tax_slug = '', $post_types = array())
    {
        $this->enable = ChildConstant::ORDER_MODULE_ENABLE;
        $this->moduleName = ChildConstant::ORDER_MODULE_NAME;
        $this->postType = ChildConstant::ORDER_MODULE_POST_TYPE;

        parent::__construct([
            'menu_icon' => 'dashicons-art',
            'support_fields' => ['title', 'revisions'],
            'enable_create_post' => 'do_not_allow'
        ]);

        $this->status = [
            OrderConstant::STATUS_OPEN => 'Open',
            OrderConstant::STATUS_WAITING => 'Awaiting to payment',
            OrderConstant::STATUS_COMPLETED => 'Completed',
            OrderConstant::STATUS_CANCELED => 'Canceled',
        ];

        $this->subscriptionType = [
            OrderConstant::SUBSCRIPTION_ONE_OFF => 'Once off',
            OrderConstant::SUBSCRIPTION_ANNUAL => 'Annual',
            OrderConstant::SUBSCRIPTION_MONTHLY => 'Monthly',
        ];

        //Admin column
        add_filter('manage_edit-' . $this->postType . '_columns', array($this, 'editColumns'));
        add_action('manage_' . $this->postType . '_posts_custom_column', array($this, 'customColumns'), 2);
        add_action('post_row_actions', array($this, 'removeRowActions'), 2);

        //Edit fields
        add_filter('acf/load_field', array($this, 'disableAcfLoadField'));
        add_filter('acf/load_field/name=status', array($this, 'acfLoadFieldChoicesStatus'));

        add_action('pre_post_update', array($this, 'updateOrder'));

        //for dashboard
        add_action('wp_dashboard_setup', array($this, 'myDashboardWidgets'));
    }

    public function myDashboardWidgets($args = [])
    {
        $orderSummary = (new OrderModel())->getOrderSummary();

        //TODO
        wp_add_dashboard_widget('custom_help_widget', 'Order activities', function () use ($orderSummary) {
            echo '<table class="wp-list-table widefat fixed striped pages">
                <tr>
                    <th class="column-title manage-column">Total order</th>
                    <td>' . $orderSummary['total'] . '</td>
                </tr>
               <tr>
                    <th class="column-title manage-column">Completed</th>
                    <td>' . $orderSummary['status'][OrderConstant::STATUS_COMPLETED] . '</td>
                </tr>
                <tr>
                    <th class="column-title manage-column">Waiting for payment</th>
                    <td>' . $orderSummary['status'][OrderConstant::STATUS_WAITING] . '</td>
                </tr>
                 <tr>
                    <th class="column-title manage-column">Subscription monthly</th>
                    <td>' . $orderSummary['sub'][OrderConstant::SUBSCRIPTION_MONTHLY] . '</td>
                </tr>
                <tr>
                    <th class="column-title manage-column">Subscription Once off</th>
                    <td>' . $orderSummary['sub'][OrderConstant::SUBSCRIPTION_ONE_OFF] . '</td>
                </tr>
                <tr>
                    <th class="column-title manage-column">Subscription annual</th>
                    <td>' . $orderSummary['sub'][OrderConstant::SUBSCRIPTION_ANNUAL] . '</td>
                </tr>
            </table>';
        });
    }

    public function removeRowActions()
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            return [];
        }
    }

    public function editColumns($existing_columns)
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            $columns = [];
            $columns["cb"] = "<input type=\"checkbox\" />";
            $columns["title"] = __('Title', 'sarita');
            $columns["subscription_type"] = __('Subscription Type', 'sarita');
            $columns["pre_payment"] = __('Pre payment', 'sarita');
            $columns["monthly_payment"] = __('Monthly payment', 'sarita');
            $columns["payment_method"] = __('Payment method', 'sarita');
            $columns["status"] = __('Status', 'sarita');
            $columns["date"] = __('Order date', 'sarita');

            return $columns;
        }

        return $existing_columns;
    }

    public function customColumns($column)
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            $order = new OrderModel($post->ID);

            switch ($column) {
                case "subscription_type" :
                    echo $this->subscriptionType[$order->getSubscriptionType()] ?? 'Unknown';
                    break;

                case "pre_payment" :
                    echo $order->getPrePayment() . ' ' . $order->getCurrency();
                    break;

                case "monthly_payment" :
                    echo $order->getMonthlyPayment() . ' ' . $order->getCurrency();
                    break;

                case "status" :
                    echo $this->status[$order->getStatus()] ?? 'Unknown';
                    break;

                case "payment_method" :
                    echo $order->getPaymentMethod();
                    break;
            }
        }
    }

    public function disableAcfLoadField($field)
    {
        global $post;

        if ($post && $post->post_type == $this->postType && $field['name'] != 'status') {
            $field['disabled'] = 1;
        }

        return $field;
    }

    public function acfLoadFieldChoicesStatus($field)
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            $order = new OrderModel($post->ID);

            switch ($order->getStatus()) {
                case OrderConstant::STATUS_OPEN:
                    $field['choices'] = $this->status;
                    break;

                case OrderConstant::STATUS_WAITING:
                    $field['choices'] = [
                        OrderConstant::STATUS_WAITING => 'Awaiting to payment',
                        OrderConstant::STATUS_COMPLETED => 'Completed',
                        OrderConstant::STATUS_CANCELED => 'Canceled',
                    ];
                    break;

                case OrderConstant::STATUS_COMPLETED:
                    $field['choices'] = [
                        OrderConstant::STATUS_COMPLETED => 'Completed',
                        OrderConstant::STATUS_CANCELED => 'Canceled',
                    ];
                    break;

                case OrderConstant::STATUS_CANCELED:
                    $field['choices'] = [
                        OrderConstant::STATUS_COMPLETED => 'Completed',
                        OrderConstant::STATUS_CANCELED => 'Canceled',
                    ];
                    break;
            }
        }

        return $field;
    }

    public function updateOrder($post_id)
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            //TODO
            $request = $this->getRequestAcf();
            $newStatus = $request['status'] ?? '';
            $oldStatus = get_field('status', $post->ID);

            if ($oldStatus != $newStatus) {
                //TODO
                if ($newStatus == OrderConstant::STATUS_CANCELED) {
                    (new OrderModel())->cancelledOrder($post->ID);
                } elseif ($newStatus == OrderConstant::STATUS_COMPLETED) {
                    (new OrderModel())->paidOrder($post->ID, []);
                }
            }
        }
    }

    private function getRequestAcf()
    {
        $request = $_POST['acf'] ?? [];
        $result = [];
        if (isset($result)) {
            foreach ($request as $key => $value) {

                $field = get_field_object($key);
                if (isset($field['name'])) {
                    $result[$field['name']] = $value;
                }
            }
        }

        return $result;
    }
}