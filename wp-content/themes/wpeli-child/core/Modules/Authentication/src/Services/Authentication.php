<?php

namespace EliChild\Modules\Authentication\Services;

use EliChild\Modules\Customer\Models\CustomerModel;
use EliChild\Modules\StoreFront\Events\ResetPasswordEvent;

class Authentication
{
    public static function check()
    {
        if (isset($_SESSION[SESSION_AUTH])) {
            return true;
        }

        return null;
    }

    public static function user()
    {
        if (self::check()) {
            $sessionData = $_SESSION[SESSION_AUTH];
            return $sessionData;
        }

        return null;
    }

    public static function login($username, $password)
    {
        $response = (new CustomerModel())->getDetailByEmailPassword($username, $password);

        if ($response) {
            $_SESSION[SESSION_AUTH] = $response;
            return true;
        }

        return false;
    }

    public static function reGetUser()
    {
        $userDetail = self::user();

        if ($userDetail) {
            $response = (new CustomerModel($userDetail['id']))->getDetail();

            if ($response) {
                $_SESSION[SESSION_AUTH] = $response;
            }
        }
    }

    public static function logout()
    {
        if (self::check()) {
            unset($_SESSION[SESSION_AUTH]);
        }
    }

    public static function forgetPassword($email)
    {
        $customerModel = new CustomerModel();
        $customerExist = $customerModel->checkCustomerActiveExistByEmail($email);

        if ($customerExist) {
            $newPassword = strtoupper(substr(md5(uniqid(rand())), 0, 10));
            $customerUpdate = $customerModel->updatePassword($customerExist->ID, $newPassword);

            if ($customerUpdate) {
                $customerModel->setDetail($customerExist->ID);
                event(new ResetPasswordEvent([
                    'name' => $customerModel->getFullName(),
                    'email' => $customerModel->getEmail(),
                    'password' => $newPassword
                ]));

                return $newPassword;
            }
        }

        return false;
    }

    public static function getGuestId($id = null)
    {
        if (!empty($id)) {
            $_SESSION[SESSION_GUEST_ID] = $id;
        }

        return $_SESSION[SESSION_GUEST_ID] ?? null;
    }
}
