<?php

use Facebook\Facebook;

define('STATUS_DISABLE', '0');
define('STATUS_ACTIVE', '1');

define('STATUS_POST_PUBLISH', 'publish');
define('STATUS_POST_DRAFT', 'draft');

/**
 * @throws \Facebook\Exceptions\FacebookSDKException
 */
function getLinkLoginFacebook()
{
    $fb = new Facebook(array(
        'app_id' => FB_APP_ID,
        'app_secret' => FB_APP_SECRET_LOGIN,
        'default_graph_version' => 'v4.0',
    ));
    // Get redirect login helper
    $helper = $fb->getRedirectLoginHelper();

    $permissions = ['email']; // Optional permissions
    $loginURL = $helper->getLoginUrl(FB_REDIRECT_URL, $permissions);

    // Render Facebook login button
    $output = htmlspecialchars($loginURL);
    echo $output;
}

function customer_check_login()
{
    if (isset($_SESSION[SESSION_AUTH_CUSTOMER])) {
        return $_SESSION[SESSION_AUTH_CUSTOMER];
    }
    return false;
}

function urlencode_custom($string)
{
    if ($string) {
        echo urlencode($string);
    }
    echo '';
}
