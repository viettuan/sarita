<?php

namespace EliChild\Modules\Authentication\Providers;

use EliChild\Modules\Base\Traits\LoadAndPublishDataTrait;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class AuthenticationServiceProvider implements ServiceProviderInterface, BootableProviderInterface
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Silex\Application
     */
    protected $app;

    /**
     * Bootstrap the application events.
     * @param Application $app
     */
    public function boot(Application $app)
    {
        $this->app = $app;

        $this->setIsInConsole($this->runningInConsole())
            ->setNamespace('modules.Authentication');
    }

    /**
     * Register the service provider.
     *
     * @param Container $container
     * @return void
     */
    public function register(Container $container)
    {
        $helpers = [
            __DIR__ . '/../Helpers/helpers.php'
        ];

        foreach ($helpers as $helper) {
            require_once $helper;
        }
    }
}
