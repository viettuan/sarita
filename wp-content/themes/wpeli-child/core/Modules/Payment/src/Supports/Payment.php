<?php

namespace EliChild\Modules\Payment\Supports;

use EliChild\Modules\Payment\GateWay\Paypal\PaypalGateWay;
use EliChild\Modules\Payment\GateWay\Quickpay\QuickpayGateWay;

class Payment
{
    /**
     * @var PaymentLog
     */
    public $log;

    public function __construct()
    {

    }

    public static function create($gateWay, $args = [])
    {
        $gatWayPayment = null;
        switch ($gateWay) {
            case 'credit_card':
                $gatWayPayment = new QuickpayGateWay();
                break;

            case 'paypal':
                $gatWayPayment = new PaypalGateWay();
                break;
        }

        return $gatWayPayment;
    }

}