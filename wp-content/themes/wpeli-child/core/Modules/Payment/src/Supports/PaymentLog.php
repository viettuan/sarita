<?php
namespace EliChild\Modules\Payment\Supports;

use WC_Logger;

/**
 * PaymentLog class
 */
class PaymentLog
{
    /* The WC_logger instance */
    private $logger;

    /* The domain handler used to name the log */
    private $domain = 'sarita-quickpay';

    /**
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->logger = new WC_logger();
    }

    /**
     * Uses the build in logging method in WooCommerce.
     * Logs are available inside the System status tab
     * @param $param
     * @param null $file
     * @param null $line
     */
    public function add($param, $file = null, $line = null)
    {
        $message = '';

        if ($file) {
            $message .= sprintf('File: %s -> ', $file);
        }

        if ($line) {
            $message .= sprintf('Line: %s -> ', $line);
        }

        if (is_array($param)) {
            $message .= print_r($param, TRUE);
        }

        $this->logger->add($this->domain, $message);
    }

    /**
     * clear function.
     * Clears the entire log file
     * @access public
     * @return bool
     */
    public function clear()
    {
        return $this->logger->clear($this->domain);
    }

    /**
     * separator function.
     * Inserts a separation line for better overview in the logs.
     * @access public
     * @return void
     */
    public function separator()
    {
        $this->add('--------------------');
    }

    /**
     * Returns a link to the log files in the WP backend.
     */
    public function get_admin_link()
    {
        $log_path = wc_get_log_file_path($this->domain);
        $log_path_parts = explode('/', $log_path);
        return add_query_arg(array(
            'page' => 'wc-status',
            'tab' => 'logs',
            'log_file' => end($log_path_parts)
        ), admin_url('admin.php'));
    }
}