<?php

namespace EliChild\Modules\Payment\GateWay\Quickpay;

use EliChild\Modules\ApiClient\Events\LogCallApiEvent;
use QuickPay\QuickPay;

class QuickpayGateWay
{
    protected $client;

    public function __construct()
    {
        $apiKey = getenv('QUICKPAY_API_USER_KEY');

        //Initialize client
        $this->client = new QuickPay(":{$apiKey}");
    }

    public function process($args)
    {
        $cardInfo = $args['card-info'];
        $subscriptionQPId = $args['subscriptionQPId'];

        if (isset($cardInfo['card-id'])) {
            $cardId = $cardInfo['card-id'];
        } else {
            $savedCard = $this->processCreateNewCard($cardInfo);
            if ($savedCard['error']) {
                return $savedCard;
            }

            $cardId = $savedCard['data']['id'];
        }

        //Create card token
        $cardTokenData = $this->createCardToken($cardId);
        if ($cardTokenData['error']) {
            return $cardTokenData;
        }

        $cardToken = $cardTokenData['data']['token'];

        //Authorize the subscription
        $authorizeSubscription = $this->authorizeSubscription($subscriptionQPId, [
            'amount' => $args['monthly_payment'] * 100,
            'card_token' => $cardToken
        ]);

        if ($authorizeSubscription['error']) {
            return $authorizeSubscription;
        }

        //Create recurring payment
        $recurringPayment = $this->createRecurringPayment($subscriptionQPId, [
            'order_id' => $args['order_id'],
            'amount' => $args['first_payment_amount'] * 100,
            'description' => $args['subscription_type'] ?? 'Subscription',
        ]);

        if ($recurringPayment['error']) {
            return $recurringPayment;
        }

        $response = [
            'error' => false,
            'data' => [
                'recurringPayment' => $recurringPayment,
            ]
        ];

        return $response;
    }

    public function processCreateNewCard($cardInfo)
    {
        //Create saved card
        $savedCard = $this->createSavedCard();
        if ($savedCard['error']) {
            return $savedCard;
        }

        //Authorize saved card
        $authorizeSavedCard = $this->authorizeSavedCard($savedCard['data']['id'], $cardInfo);
        if ($authorizeSavedCard['error']) {
            return $authorizeSavedCard;
        }

        $response = [
            'type' => 'CreateCard',
            'error' => false,
            'status' => 'success'
        ];
        $this->writeLog($this->getActualLink(), '', $response);
        return $savedCard;
    }

    private function createSavedCard()
    {
        $url = '/cards';
        $card = $this->client->request->post($url);

        if ($card->httpStatus() === 201) {
            $response = [
                'error' => false,
                'data' => $card->asArray()
            ];
            return $response;
        }
        $this->writeLog($url, '', $card->asArray());

        return [
            'error' => true,
            'message' => trans('Error'),
            'data' => $card->asArray()
        ];
    }

    public function getSavedCard($cardId)
    {
        $url = '/cards/' . $cardId;
        $card = $this->client->request->get($url);
        if ($card->httpStatus() == 200) {
            return [
                'error' => false,
                'data' => $card->asArray()['metadata'] ?? []
            ];
        }

        $this->writeLog($url, '', $card->asArray());
        return [
            'error' => true,
            'message' => trans('Error'),
            'data' => $card->asArray()
        ];
    }

    private function authorizeSavedCard($cardId, $cardInfo)
    {
        $input = [
            'card' => [
                'number' => $cardInfo['number'],
                'expiration' => $cardInfo['expiration'],
                'cvd' => $cardInfo['cvd'],
            ],
            'acquirer' => 'nets',
        ];

        if (!empty($cardInfo['name'])) {
            $input['card']['issued_to'] = $cardInfo['name'];
        }

        $url = sprintf('cards/%d/authorize', $cardId);
        $card = $this->client->request->post(sprintf('cards/%d/authorize', $cardId), $input);

        if ($card->httpStatus() === 202) {
            return [
                'error' => false,
                'data' => $card->asArray()
            ];
        }

        $responseData = $card->asArray();
        $this->writeLog($url, '', $responseData);

        return [
            'error' => true,
            'message' => $responseData['message'] ?? trans('Error'),
            'data' => $responseData['errors'] ?? []
        ];
    }

    private function createCardToken($cardId)
    {
        $url = sprintf('cards/%d/tokens', $cardId);
        $card = $this->client->request->post($url);

        if ($card->httpStatus() === 201) {
            return [
                'error' => false,
                'data' => $card->asArray()
            ];
        }

        $responseData = $card->asArray();
        $this->writeLog($url, '', $responseData);

        return [
            'error' => true,
            'message' => $responseData['message'] ?? trans('Error'),
            'data' => $responseData['errors'] ?? []
        ];
    }

    public function createSubscription($orderId, $args)
    {
        $dataInputSubscription = [
            'order_id' => $orderId,
            'currency' => $args['currency'],
            'description' => $args['description'] ?? 'Subscription',
        ];

//        if (!empty($args['billing-address'])) {
//            $dataInputSubscription['invoice_address'] = [
//                'street' => $args['billing-address']['address'] ?? '',
//                'city' => $args['billing-address']['city'] ?? '',
//                'region' => $args['billing-address']['state'] ?? '',
//                'zip_code' => $args['billing-address']['zipcode'] ?? '',
//                'country_code' => $args['billing-address']['country_code'] ?? '',
//            ];
//        }

        $url = '/subscriptions';
        $card = $this->client->request->post($url, $dataInputSubscription);

        if ($card->httpStatus() === 201) {
            return [
                'error' => false,
                'data' => $card->asArray()
            ];
        }

        $responseData = $card->asArray();
        $this->writeLog($url, $dataInputSubscription, $responseData);
        return [
            'error' => true,
            'message' => $responseData['message'] ?? trans('Payment error'),
            'data' => $responseData['errors'] ?? []
        ];
    }

    private function authorizeSubscription($subId, $args)
    {
        $url = sprintf('subscriptions/%d/authorize', $subId);
        $card = $this->client->request->post($url, [
            'amount' => $args['amount'],
            'card' => [
                'token' => $args['card_token']
            ]
        ]);

        //Determine if payment was created successfully
        if ($card->httpStatus() === 202) {
            return [
                'error' => false,
                'data' => $card->asArray()
            ];
        }

        $responseData = $card->asArray();
        $dataInputSubscription = ['amount' => $args['amount']];
        $this->writeLog($url, $dataInputSubscription, $responseData);
        return [
            'error' => true,
            'message' => $responseData['message'] ?? trans("Payment error"),
            'data' => $responseData['errors'] ?? []
        ];
    }

    private function createRecurringPayment($subId, $args)
    {
        $dataInputSubscription = [
            'amount' => $args['amount'],
            'order_id' => 'rec_' . $args['order_id'],
            'auto_capture' => true,
            'autofee' => false,
        ];
        $url = sprintf('subscriptions/%d/recurring?synchronized', $subId);
        $card = $this->client->request->post($url, $dataInputSubscription);

        //Determine if payment was created successfully
        if ($card->httpStatus() === 200) {
            return [
                'error' => false,
                'data' => $card->asArray()
            ];
        }

        $responseData = $card->asArray();
        $this->writeLog($url, $dataInputSubscription, $responseData);
        return [
            'error' => true,
            'message' => $responseData['message'] ?? trans('Payment error'),
            'data' => $responseData['errors'] ?? []
        ];
    }

    private function writeLog($url, $args, $response)
    {
        if (getenv('ENABLE_WRITE_API_LOG')) {
            $response['status'] = $response['status'] ?? trans('Error') ;
            event(new LogCallApiEvent([
                'type' => $response['type'] ?? '',
                'request' => [
                    'url' => $url,
                    'args' => $args,
                ],
                'response' => $response,
            ]));
        }
    }

    private function getActualLink()
    {
        return $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }
}