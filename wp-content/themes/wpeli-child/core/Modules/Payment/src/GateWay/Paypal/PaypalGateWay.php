<?php

namespace EliChild\Modules\Payment\GateWay\Paypal;

use EliChild\Modules\ApiClient\Events\LogCallApiEvent;
use Exception;
use PayPal\Api\Agreement;
use PayPal\Api\Amount;
use PayPal\Api\Currency;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Common\PayPalModel;
use PayPal\Rest\ApiContext;

class PaypalGateWay
{
    protected $apiContext;

    public function __construct()
    {
        $clientId = getenv('PAYPAL_API_CLIENT_ID');
        $clientSecret = getenv('PAYPAL_API_CLIENT_SECRET');

        $this->apiContext = new ApiContext(new OAuthTokenCredential($clientId, $clientSecret));
    }

    public function process($args)
    {
        if ($args["monthly_payment"] > 0) {
            $response = $this->createPlan([
                'order_id' => $args['order_id'],
                'plan_name' => $args['subscription_type'],
                'plan_description' => $args['subscription_description'],
                'first_payment_amount' => $args['first_payment_amount'],
                'monthly_payment' => $args['monthly_payment'],
                'currency' => $args['currency'],
                'success-link' => $args['success-link'],
                'cancel-link' => $args['cancel-link'],
            ]);
        } else {
            $response = $this->payment([
                'order_id' => $args['order_id'],
                'plan_name' => $args['subscription_type'],
                'plan_description' => $args['subscription_description'],
                'amount' => $args['pre_payment'],
                'currency' => $args['currency'],
                'success-link' => $args['success-link'],
                'cancel-link' => $args['cancel-link'],
            ]);
        }
        $this->writeLog($args, $response, 'processPaymentPaypal');

        return $response;
    }

    public function execute($data)
    {
        if ($data['status'] == "true") {
            if (!empty($data['paymentId'])) {
                $response = $this->executePayment($data);
            } else {
                $response = $this->executeSubscription($data);
            }

        } else {
            $response = [
                'error' => true,
                'message' => trans("User canceled agreement")
            ];
        }
        $this->writeLog($data, $response, 'executePaymentPaypal');

        return $response;
    }

    private function payment($data)
    {
        $response = [
            'error' => true,
            'link' => '',
            'message' => ''
        ];
        $successLink = $data['success-link'];
        $cancelLink = $data['cancel-link'];

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item = new Item();
        $item->setName($data['plan_name'])
            ->setDescription($data['plan_description'])
            ->setCurrency($data['currency'])
            ->setQuantity(1)
            ->setPrice($data['amount']);
        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $details = new Details();
        $details->setTax(0)
            ->setSubtotal($data['amount']);

        // Payment Amount
        $amount = new Amount();
        $amount->setCurrency($data["currency"])
            ->setTotal($data['amount'])
            ->setDetails($details);

        // Transaction is created with a `Payee` and `Amount` types
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Sarita subscription: ' . $data['plan_description'])
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($successLink)
            ->setCancelUrl($cancelLink);

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        try {
            $payment->create($this->apiContext);

            return [
                'error' => false,
                'approval_url' => $payment->getApprovalLink(),
            ];
        } catch (Exception $ex) {
            $response['message'] = $ex->getMessage();
            $response['data'] = json_decode($ex->getData(), true);
        }

        return $response;
    }

    private function createPlan($data)
    {
        $response = [
            'error' => true,
            'link' => '',
            'message' => ''
        ];

        $successLink = $data['success-link'];
        $cancelLink = $data['cancel-link'];

        $plan = new Plan();
        $plan->setName($data["plan_name"])
            ->setDescription($data["plan_description"])
            ->setType('fixed');

        // Set billing plan definitions
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
            ->setType('REGULAR')
            ->setFrequency('Month')
            ->setFrequencyInterval("1")
            ->setCycles("12")
            ->setAmount(new Currency(['value' => $data["monthly_payment"], 'currency' => $data["currency"]]));

        // Set merchant preferences
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl($successLink)
            ->setCancelUrl($cancelLink)
            ->setAutoBillAmount("yes")
            ->setInitialFailAmountAction("CONTINUE")
            ->setMaxFailAttempts("0")
            ->setSetupFee(new Currency(['value' => $data["first_payment_amount"], 'currency' => $data["currency"]]));

        $plan->setPaymentDefinitions(array(
            $paymentDefinition
        ));
        $plan->setMerchantPreferences($merchantPreferences);

        try {
            $createdPlan = $plan->create($this->apiContext);
            $approvalUrlData = $this->activePlan($createdPlan, $data);

            return $approvalUrlData;
        } catch (Exception $ex) {
            $response['message'] = $ex->getMessage();
            $response['data'] = json_decode($ex->getData(), true);
        }

        return $response;
    }

    private function activePlan($createdPlan, $data)
    {
        try {
            $patch = new Patch();
            $value = new PayPalModel('{"state":"ACTIVE"}');
            $patch->setOp('replace')
                ->setPath('/')
                ->setValue($value);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);
            $createdPlan->update($patchRequest, $this->apiContext);
            $patchedPlan = Plan::get($createdPlan->getId(), $this->apiContext);

            return $this->createBillingAgreement($patchedPlan, $data);
        } catch (Exception $ex) {
            return [
                'error' => true,
                'message' => $ex->getMessage(),
                'data' => json_decode($ex->getData(), true)
            ];
        }
    }

    private function createBillingAgreement($patchedPlan, $data)
    {
        // Create new agreement
        $startDate = date('c', time() + 3600);
        $agreement = new Agreement();
        $agreement->setName('Sarita Plan Subscription Agreement')
            ->setDescription('Sarita Plan Subscription Billing Agreement')
            ->setStartDate($startDate);

        // Set plan id
        $plan = new Plan();
        $plan->setId($patchedPlan->getId());
        $agreement->setPlan($plan);

        // Add payer type
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

        try {
            // Create agreement
            $agreement = $agreement->create($this->apiContext);

            // Extract approval URL to redirect user
            $approvalUrl = $agreement->getApprovalLink();

            return [
                'error' => false,
                'approval_url' => $approvalUrl,
            ];

        } catch (Exception $ex) {
            return [
                'error' => true,
                'message' => $ex->getMessage(),
                'data' => json_decode($ex->getData(), true),
            ];
        }
    }

    public function executePayment($args = [])
    {
        try {
            if (!empty($args['paymentId'] && !empty($args['token']) && !empty($args['payerID']))) {
                $payment = Payment::get($args['paymentId'], $this->apiContext);

                $execution = new PaymentExecution();
                $execution->setPayerId($args['payerID']);

                $paymentProcess = $payment->execute($execution, $this->apiContext);
                $paymentState = $paymentProcess->getState();
                if ($paymentState == 'approved') {
                    return [
                        'error' => false,
                        'data' => [
                            'paymentId' => $args['paymentId'],
                            'token' => $args['token'],
                            'PayerID' => $args['payerID'],
                        ],
                    ];
                }
            }
        } catch (Exception $ex) {
            return [
                'error' => true,
                'message' => $ex->getMessage(),
                'data' => json_decode($ex->getData(), true)
            ];
        }

        return [
            'error' => true,
            'message' => 'Invalid',
        ];
    }

    public function executeSubscription($args = [])
    {
        $token = $args['token'];
        $agreement = new Agreement();

        try {
            // Execute agreement
            $agreement->execute($token, $this->apiContext);
            $agreement = $agreement->get($agreement->getId(), $this->apiContext);

            return [
                'error' => false,
                'data' => [
                    'agreement_id' => $agreement->getId(),
                    'token' => $token,
                ],
            ];
        } catch (Exception $ex) {
            return [
                'error' => true,
                'message' => $ex->getMessage(),
                'data' => json_decode($ex->getData(), true)
            ];
        }
    }

    private function writeLog($request, $response, $type)
    {
        if (getenv('ENABLE_WRITE_API_LOG')) {

            $response['status'] = $response['error'] ? 'error' : 'success' ;
            event(new LogCallApiEvent([
                'type' => $type,
                'request' => $request,
                'response' => $response,
            ]));
        }
    }

}