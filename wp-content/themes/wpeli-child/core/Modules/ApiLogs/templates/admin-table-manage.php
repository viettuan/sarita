<div class="wrap clear">
    <fieldset>
        <div class="tablenav top">
            <form method="post"
                  action="<?php echo admin_url() ?>admin.php?page=<?php if(isset($this->listSlug)) echo $this->listSlug ?>&paged=<?php echo isset($_GET['paged']) ? $_GET['paged'] : 1 ?>">
                <div class="tablenav-pages one-page">
                    <span class="displaying-num"><?php echo sprintf('%d items', count($results)) ?></span>
                </div>
                <br class="clear">

                <table class="wp-list-table widefat posts apl-list-custom-table pbm-table-post fixed">
                    <thead>
                    <tr>
                        <?php
                        $title = '';
                        foreach ($fields as $v):
                            $title .= '<th>' . $v . '</th>';
                        endforeach;
                        echo $title;
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($results):
                        foreach ($results as $k => $r): ?>
                            <tr class="status-publish hentry <?php echo $k % 2 ? 'alternate' : '' ?> iedit author-self level-0">
                                <td><?php echo $r->created?></td>
                                <td><?php echo $r->type?></td>
                                <td>
                                    <?php echo $this->limitString($r->request, 120) ?>
                                </td>
                                <td><?php echo $this->limitString($r->response, 120) ?></td>

                                <td><?php echo $r->status ?></td>
                            </tr>
                        <?php endforeach;
                    else:
                        echo '<tr class="no-items"><td class="colspanchange" colspan="7">' . __('No page found', 'wpeli') . '</td></tr>';
                    endif;
                    ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <?php echo $title; ?>
                    </tr>
                    </tfoot>
                </table>

                <div class="tablenav bottom">
                    <div class="tablenav-pages">
                        <?php
                        $pagenum = isset($_GET['paged']) ? absint($_GET['paged']) : 1;
                        $num_of_pages = ceil($total_post / $pagesize);
                        $page_links = paginate_links(array(
                            'base' => add_query_arg('paged', '%#%'),
                            'format' => '',
                            'prev_text' => __('&laquo;', 'wpeli'),
                            'next_text' => __('&raquo;', 'wpeli'),
                            'total' => $num_of_pages,
                            'current' => $pagenum
                        ));
                        echo $page_links;
                        ?>
                    </div>
                    <br class="clear">
                </div>
            </form>
        </div>
    </fieldset>
</div>