<?php

namespace EliChild\Modules\ApiLogs\Admin;

use EliChild\Helpers\ChildConstant;
use EliChild\Modules\ApiClient\Models\ApiLogModel;

class ApiLogsAdminModule
{
    private $logSlug = ChildConstant::API_LOGS_MODULE_SLUG;
    private $logName = ChildConstant::API_LOGS_MODULE_NAME;
    private $logEnable = ChildConstant::API_LOGS_MODULE_ENABLE;
    private $pagesize = 10;

    public static function initialize()
    {
        return new self();
    }

    public function __construct()
    {
        if ($this->logEnable) {
            add_action('admin_menu', array($this, 'addMenu'));
        }
    }

    public function addMenu()
    {
        add_menu_page($this->logName, $this->logName, 'manage_options', $this->logSlug, array($this, 'columns'), 'dashicons-warning', 37);
    }

    /**
     * render data customer
     * @return mixed
     */
    public function columns()
    {
        $this->pagesize = get_option('posts_per_page');
        $pagenum = isset($_GET['paged']) ? absint($_GET['paged']) : 1;
        $offset = ($pagenum - 1) * $this->pagesize;
        $args = array(
            'posts_per_page' => get_option('posts_per_page'),
            'post_type' => 'page',
            'meta_key' => 'page_builder_module',
            'offset' => $offset,
        );

        $apiLogsModel = new ApiLogModel();
        $results = $apiLogsModel->getWhere('','*', $this->pagesize, 'ORDER BY created DESC');
        $total_post = $apiLogsModel->getTotal('');
        $pagesize = $this->pagesize;

        $fields = array(
            'created' => 'Request Datetime',
            'type' => 'Api type',
            'request' => 'Request body',
            'response' => 'Response',
            'status' => 'Status'
        );

        $view = include(ROOT_CHILD_MODULE . '/ApiLogs/templates/admin-table-manage.php');
        return $view;
    }

    public function limitString($string, $limit)
    {
        if (strlen($string) > $limit) {
            $stringCut = substr($string, 0, $limit);
            $string = $stringCut . '<span class="text-hide">' . substr($string, $limit) . '</span><a href="javascript:void(0)" class="read-more-text">...Read more</a>';
        }
        return $string;
    }
}