<?php

namespace EliChild\Modules\Sample\Models;

use Eli\Modules\Model;

class SampleModel extends Model
{
    /**
     * SampleModel constructor.
     * @author Sang Nguyen
     */
    public function __construct()
    {
        parent::__construct();
    }
}