<?php

namespace EliChild\Modules\ApiClient;

class ApiClientModule
{
    public static function initialize()
    {
        return new self();
    }

    public function __construct()
    {
        //flush the rewrite rules
        flush_rewrite_rules(true);

        //Modify url base from wp-json to 'api'
        add_filter('rest_url_prefix', array($this, 'my_theme_api_slug'));
    }

    public function my_theme_api_slug($slug = '')
    {
        return 'api';
    }

    public function loadRoutes($fileNames = ['web'])
    {
        if (!is_array($fileNames)) {
            $fileNames = [$fileNames];
        }
        foreach ($fileNames as $fileName) {
            require_once __DIR__ . '/../routes/' . $fileName . '.php';
        }
    }
}