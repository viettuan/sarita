<?php

namespace EliChild\Modules\ApiClient\Services;

use EliChild\Modules\Base\Supports\EliCurl;
use EliChild\Modules\Base\Abstracts\RepositoriesAbstract;

class SubscriptionCurl extends RepositoriesAbstract
{
    //Get my subscriptions
    public function getMySubscriptions($saritaUserId)
    {
        $apiLink = getenv('API_LIGHTHOUSE_URL') . '/subscriptions?sarita_user_id=' . $saritaUserId;
        $response = EliCurl::get($apiLink, [
            'sarita_user_id' => $saritaUserId,
            'type_request' => 'getMySubscriptions',
        ]);

        return $response;
    }

    //Create subscription
    public function createSubscription($saritaUserId, $args)
    {
        $args['type_request'] = 'createSubscription';
        $apiLink = getenv('API_LIGHTHOUSE_URL') . '/subscriptions?sarita_user_id=' . $saritaUserId;
        $response = EliCurl::post($apiLink, $args);

        return $response;
    }

    //Cancel subscription
    public function cancelSubscription($saritaSubscriptionId)
    {
        $apiLink = getenv('API_LIGHTHOUSE_URL') . '/subscriptions?sarita_subscription_id=' . $saritaSubscriptionId;
        $response = EliCurl::put($apiLink, [
            'sarita_subscription_id' => $saritaSubscriptionId,
            'type_request' => 'cancelSubscription'
        ]);

        return $response;
    }
}