<?php

namespace EliChild\Modules\ApiClient\Services;

use EliChild\Modules\ApiClient\Models\ApiLogModel;

class ApiLog
{
    /**
     * @var ApiLogModel
     */
    protected static $apiLogRepository;

    public function __construct(ApiLogModel $apiLogRepository)
    {
        self::$apiLogRepository = $apiLogRepository;
    }

    public static function writeLog(array $input)
    {
//        self::$apiLogRepository->insert($input);
        (new ApiLogModel())->insert($input);
    }
}