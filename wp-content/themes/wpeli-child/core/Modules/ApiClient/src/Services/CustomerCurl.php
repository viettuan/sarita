<?php

namespace EliChild\Modules\ApiClient\Services;

use EliChild\Modules\Base\Supports\EliCurl;
use EliChild\Modules\Base\Abstracts\RepositoriesAbstract;

class CustomerCurl extends RepositoriesAbstract
{
    //Get user
    public function getUser($email, $password)
    {
        $apiLink = getenv('API_LIGHTHOUSE_URL') . '/users';
        $response = EliCurl::get($apiLink, [
            'username' => $email,
            'password' => $password,
            'type_request' => 'getUser',
        ]);

        return $response;
    }

    //Create User
    public function createUser($args)
    {
        $args['type_request'] = 'createUser';
        $apiLink = getenv('API_LIGHTHOUSE_URL') . '/users';
        $response = EliCurl::post($apiLink, $args);

        return $response;
    }

    //Update User
    public function updateUser($saritaUserId, $args)
    {
        $args['type_request'] = 'updateUser';
        $apiLink = getenv('API_LIGHTHOUSE_URL') . '/users?sarita_user_id=' . $saritaUserId;
        $response = EliCurl::put($apiLink, $args);

        return $response;
    }

    //Delete User
    public function deleteUser($saritaUserId)
    {
        $args['type_request'] = 'deleteUser';
        $apiLink = getenv('API_LIGHTHOUSE_URL') . '/users?sarita_user_id=' . $saritaUserId;
        $response = EliCurl::delete($apiLink, $args);

        return $response;
    }

    //Change password
    public function changePassword($saritaUserId, $args)
    {
        $apiLink = getenv('API_LIGHTHOUSE_URL') . '/password?sarita_user_id=' . $saritaUserId;

        $response = EliCurl::put($apiLink, [
            'sarita_user_id' => $saritaUserId,
            'old_password' => $args['old_password'],
            'new_password' => $args['new_password'],
            'type_request' => 'changePassword',
        ]);

        return $response;
    }
}
