<?php

namespace EliChild\Modules\ApiClient\Listeners;

use EliChild\Modules\ApiClient\Events\LogCallApiEvent;
use EliChild\Modules\ApiClient\Services\ApiLog;

class LogCallApiHandler
{
    public function __invoke(LogCallApiEvent $event)
    {
        $args = $event->args;

        ApiLog::writeLog([
            'type' => $args['type'] ?? '',
            'request' => json_encode($args['request'] ?? []),
            'response' => json_encode($args['response'] ?? []),
            'status' => $args['response']['status'] ?? 'error',
        ]);
    }
}