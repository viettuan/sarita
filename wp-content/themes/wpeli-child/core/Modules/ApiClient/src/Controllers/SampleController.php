<?php

namespace EliChild\Modules\ApiClient\Controllers;

use WP_Error;
use WP_REST_Controller;
use WP_REST_Response;

class SampleController extends WP_REST_Controller
{
    public static function initialize()
    {
        return new self();
    }

    public function register_routes()
    {
        $namespace = 'api/v1';
        $path = 'latest-posts/(?P<category_id>\d+)';

        register_rest_route($namespace, '/' . $path, [
            array(
                'methods' => 'GET',
                'callback' => array($this, 'get_items'),
                'permission_callback' => array($this, 'get_items_permissions_check')
            ),
        ]);
    }

    public function get_items_permissions_check($request)
    {
        return true;
    }

    public function get_items($request)
    {

        $args = array(
            'category' => $request['category_id']
        );

        $posts = get_posts($args);

        if (empty($posts)) {
            return new WP_Error('empty_category', 'there is no post in this category', array('status' => 404));
        }

        return new WP_REST_Response($posts, 200);
    }
}