<?php

namespace EliChild\Modules\ApiClient\Events;

use EliChild\Modules\Base\Events\Event;

class ApiEvent extends Event
{
    public $args;

    public function __construct($args)
    {
        $this->args = $args;
    }
}