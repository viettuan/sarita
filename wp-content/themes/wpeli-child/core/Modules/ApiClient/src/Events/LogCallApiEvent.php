<?php

namespace EliChild\Modules\ApiClient\Events;

use EliChild\Modules\Base\Events\Event;

class LogCallApiEvent extends Event
{
    public $args;

    public function __construct($args)
    {
        $this->args = $args;
    }
}