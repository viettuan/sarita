<?php

namespace EliChild\Modules\ApiClient\Models;

use EliChild\Modules\Base\Abstracts\RepositoriesAbstract;

class ApiLogModel extends RepositoriesAbstract
{
    protected $fieldsAble = [
        'request',
        'response',
        'type',
        'status'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->table = $this->wpdb->prefix . 'api_logs';
    }

    public function fields()
    {
        return $this->fieldsAble;
    }
}
