<?php

namespace EliChild\Modules\ApiClient\Providers;

use EliChild\Modules\ApiClient\Events\LogCallApiEvent;
use EliChild\Modules\ApiClient\Listeners\LogCallApiHandler;
use EliChild\Modules\Base\Traits\LoadAndPublishDataTrait;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class ApiClientServiceProvider implements ServiceProviderInterface, BootableProviderInterface
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Silex\Application
     */
    protected $app;

    protected $listen = [
        LogCallApiEvent::class => LogCallApiHandler::class,
    ];

    public function boot(Application $app)
    {
        $this->app = $app;

        $this->setIsInConsole($this->runningInConsole())
            ->setNamespace('modules.ApiClient')
            ->loadEvents()
            ->loadAndPublishConfigurations(['config'])
            ->loadMigrations();
    }

    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $pimple A container instance
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
    }
}
