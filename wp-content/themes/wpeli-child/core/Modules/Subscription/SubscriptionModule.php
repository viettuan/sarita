<?php

namespace EliChild\Modules\Subscription;

class SubscriptionModule
{
    public static function initialize()
    {
        return new self();
    }

    public function __construct($tax_slug = '', $post_types = array())
    {
        add_action('init', array($this, 'registerTaxonomy'), 1);
    }

    public function registerTaxonomy()
    {
        if(function_exists('acf_add_options_page')) {
            acf_add_options_page([
                'page_title' => __('Subscriptions', 'wp_eli'),
                'menu_title' => __('Subscriptions', 'wp_eli'),
                'menu_slug' => 'subscriptions',
                'capability' => 'edit_posts',
                'redirect' => false,
                'position' => 26,
                'icon_url' => 'dashicons-category',
            ]);
        }
    }
}