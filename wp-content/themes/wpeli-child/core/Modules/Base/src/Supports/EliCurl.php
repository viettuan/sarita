<?php

namespace EliChild\Modules\Base\Supports;

use EliChild\Modules\ApiClient\Events\LogCallApiEvent;

class EliCurl
{
    public static function getHeader()
    {
        return [
            'Api-Version' => '1.0',
            'Api-Key' => getenv('API_LIGHTHOUSE_KEY'),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
    }

    public static function get($url, $args)
    {
        $username = $args['username'] ?? '';
        $password = $args['password'] ?? '';
        $headers = ['Authorization' => 'Basic ' . base64_encode("$username:$password")] + self::getHeader();
        $request = ['headers' => $headers];

        $data = wp_remote_get($url, $request);
        $response = self::response($data);
        $args['method'] = 'get';
        self::writeLog($url, $args, $response);

        return $response;
    }

    public static function post($url, $args)
    {
        $headers = self::getHeader();
        $request = [
            'headers' => $headers,
            'body' => json_encode($args),
        ];

        $data = wp_remote_post($url, $request);
        $response = self::response($data);
        $args['method'] = 'post';
        self::writeLog($url, $args, $response);

        return $response;
    }

    public static function put($url, $args)
    {
        $headers = self::getHeader();
        $request = [
            'headers' => $headers,
            'method' => 'PUT',
            'body' => json_encode($args),
        ];

        $data = wp_remote_request($url, $request);
        $response = self::response($data);
        $args['method'] = 'put';
        self::writeLog($url, $args, $response);

        return $response;
    }

    public static function delete($url, $args = [])
    {
        $headers = self::getHeader();
        $request = [
            'headers' => $headers,
            'method' => 'DELETE',
        ];

        $data = wp_remote_request($url, $request);
        $response = self::response($data);
        $args['method'] = 'delete';
        self::writeLog($url, $args, $response);

        return $response;
    }

    private static function writeLog($url, $args, $response)
    {
        if(getenv('ENABLE_WRITE_API_LOG')) {
            event(new LogCallApiEvent([
                'type' => isset($args['type_request'] ) ? ('lighthouse - ' . $args['type_request'])  : '',
                'request' => [
                    'url' => $url,
                    'args' => $args,
                ],
                'response' => $response,
            ]));
        }
    }

    private static function response($res)
    {
        if (!is_wp_error($res) && ($res['response']['code'] == 200 || $res['response']['code'] == 201)) {
            $status = 'success';
        } else {
            $status = 'error';
        }

        $res = (array) $res;
        return [
            'status' => $status,
            'code' => $res['response']['code'] ?? '',
            'data' => json_decode($res['body'] ?? '', true),
        ];
    }
}
