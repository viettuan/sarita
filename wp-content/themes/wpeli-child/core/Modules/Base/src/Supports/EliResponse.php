<?php

namespace EliChild\Modules\Base\Supports;

use Symfony\Component\HttpFoundation\Response;

class EliResponse extends Response
{
    /**
     * @var bool
     */
    protected $error = false;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var bool
     */
    protected $withInput = false;

    /**
     * @var int
     */
    protected $code = 200;

    /**
     * @param $error
     * @return EliResponse
     * @author Hieu Luong
     */
    public function setError(bool $error = true): self
    {
        $this->error = $error;
        if ($error) {
            $this->code = 422;
        }
        return $this;
    }

    /**
     * @param $message
     * @return EliResponse
     * @author Hieu Luong
     */
    public function setMessage($message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param $data
     * @return EliResponse
     * @author Hieu Luong
     */
    public function setData($data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param bool $with_input
     * @return EliResponse
     */
    public function withInput(bool $with_input = true): self
    {
        $this->withInput = $with_input;
        return $this;
    }

    /**
     * @param int $code
     * @return EliResponse
     */
    public function setCode(int $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->error;
    }

    /**
     * @author Hieu Luong
     */
    public function toResponse()
    {
        return 1;
        return wp_send_json([
            'error' => $this->error,
            'data' => $this->data,
            'message' => $this->message,
        ], $this->code);
    }

    /**
     * @param string $url
     * @author Hieu Luong
     * @return string
     */
    protected function responseRedirect($url)
    {
        return $url;
    }
}