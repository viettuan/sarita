<?php

namespace EliChild\Modules\Base\Supports;

use \DrewM\MailChimp\MailChimp;

class EliMailChimpSubscribe
{
    protected $mailChimp = null;
    protected $subscribeListId = null;

    public function subscribe($email)
    {
        try {
            $this->mailChimp = new MailChimp(getenv('MAILCHIMP_APIKEY'));
            $this->subscribeListId = getenv('MAILCHIMP_SUBSCRIBE_LIST_ID');


            $listId = $this->subscribeListId;

            $this->mailChimp->post("lists/$listId/members", [
                'email_address' => $email,
                'status' => 'subscribed',
            ]);

            if ($this->mailChimp->success()) {
                return [
                    'error' => false
                ];
            }

            return [
                'error' => true,
                'message' => trans("Email invalid or you are already subscribed to our newsletter")
            ];
        } catch (\Exception $exception) {
            return [
                'error' => true,
                'message' => trans("Error")
            ];
        }
    }
}
