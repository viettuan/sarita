<?php

use EliChild\Modules\Base\Queue\QueueFactory;
use EliChild\Modules\StoreFront\Models\ThemeModel;
use EliChild\Modules\StoreFront\StoreFrontModule;
use Bernard\Producer;
use Symfony\Component\EventDispatcher\EventDispatcher;

function renderView($file, $data = [])
{
    $data['global'] = ThemeModel::getData();
    echo StoreFrontModule::render($file, $data);
    die;
}

///**
// * Render page using Blade
// */
//add_filter('template_include', function ($template) {
//    if ($template) {
//        $data = [];
////        $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
////            return $data;
////        }, []);
//
//        //echo template($template, $data);
//    }
//
//    return $template;
//
//}, PHP_INT_MAX);
//

function feAssets($path)
{
    return get_stylesheet_directory_uri() . '/assets/' . $path;
}

function response($response, $statusCode = 200)
{
    return wp_send_json([
        'error' => $response['error'] ?? true,
        'message' => $response['message'] ?? null,
        'data' => $response['data'] ?? null,
    ], $statusCode);
}

function event($event, $dispatchNow = false)
{
    $queueFactory = new QueueFactory();
    $queueFactory = $queueFactory->factory();

    if ($queueFactory && !$dispatchNow) {
        $eventDispatcher = new EventDispatcher();
        $producer = new Producer($queueFactory, $eventDispatcher);
        $producer->produce($event, $event->queue);
    } else {
        global $globalData;
        $handlers = $globalData['events'];

        if (isset($handlers[$event->getName()])) {
            (new $handlers[$event->getName()]())->__invoke($event);
        }
    }
}

function trans($text)
{
    $langDefault = 'en_US';
    $lang = get_locale();

    try {
        $translateFilePath = wp_upload_dir()['basedir'] . "/lang/$lang.json";
        if (!file_exists($translateFilePath)) {
            $translateFilePath = THEME_PATH . "/lang/$lang.json";
        }
        if (!file_exists($translateFilePath)) {
            $translateFilePath = THEME_PATH . "/lang/$langDefault.json";
        }

        //TODO apply cache
        $dataTranslation = json_decode(file_get_contents($translateFilePath), true);
        return $dataTranslation[$text] ?? $text;
    } catch (\Exception $ex) {
        return $text;
    }
}