<?php

namespace EliChild\Modules\Base\Abstracts;

abstract class RepositoriesAbstract
{
    protected $wpdb;
    public $table;

    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    public function getNameTable()
    {
        return $this->table;
    }

    /**
     * @param array $data
     * @return bool|int
     */
    public function insert($data = array())
    {
        $fields = '';
        $valueFields = '';

        foreach ($data as $key => $value) {
            $fields .= !empty($fields) ? (',' . $key) : $key;
            $valueFields .= !empty($valueFields) ? (", '" . $value . "'") : "'" . $value . "'";
        }

        $result = $this->wpdb->query("INSERT INTO " . $this->table . " (" . $fields . ") VALUES (" . $valueFields . ")");

        if ($result) {
            return $this->wpdb->insert_id;
        }

        return false;
    }

    /**
     * @param $where : array | string
     * @return mixed|null
     */
    public function getFirst($where)
    {
        $data = $this->getWhere($where, '*', 1);

        return $data[0] ?? null;
    }

    /**
     * Update customer
     * @param $fields
     * @param $where
     * @param bool $all
     * @return bool|int|string
     */
    public function update($fields, $where, $all = false)
    {
        $type = $all ? '' : ' LIMIT 1';
        $where = $this->checkWhere($where);
        $update = $this->joinStringQuery($fields, ',');
        $update = $this->wpdb->query("UPDATE {$this->table} SET {$update} WHERE {$where} {$type}");

        return $update;
    }

    /**
     * Join string by array for query sql
     * @param $array
     * @param $join
     * @return string
     */
    private function joinStringQuery($array, $join)
    {
        $result = '';
        foreach ($array as $key => $value) {
            $result = !empty($result) ? ($result . " " . $join . " ") : $result;
            $result .= $key . '=' . "'" . $value . "'";
        }

        return $result;
    }

    /**
     * Update or create
     * @param array $data
     * @param $condition
     * @return bool|int|string
     */
    public function updateOrCreate($data = array(), $condition)
    {
        $first = $this->getFirst($condition);
        if ($first) {
            $result = $this->update($data, $condition);
        } else {
            $result = $this->insert($data);
        }

        return $result;
    }

    /**
     * @param $where : array | string
     * @param $select : string
     * @param $limit : number
     * @param string $order : string
     * @return array|object|null
     */
    public function getWhere($where, $select, $limit, $order = '')
    {
        $where = $this->checkWhere($where);
        $limit = !empty($limit) ? 'LIMIT ' . $limit : '';

        return $this->wpdb->get_results("SELECT {$select} FROM {$this->table} WHERE {$where}  {$order} {$limit}");
    }

    /**
     * check where array or string
     * @param $where
     * @return string
     */
    private function checkWhere($where)
    {
        if (!empty($where) && is_array($where)) {
            $where = $this->joinStringQuery($where, 'AND');
        }
        $where = !empty($where) ? $where : '1=1';

        return $where;
    }

    /**
     * @param $where : array | string
     * @return int
     */
    public function getTotal($where)
    {
        $where = $this->checkWhere($where);
        $total_data = $this->wpdb->get_row(" SELECT count(*) as total FROM {$this->table} WHERE {$where} ");

        return $total_data ? intval($total_data->total) : 0;
    }

    /**
     * @param $where : array | string
     * @return bool|int
     */
    public function delete($where)
    {
        $where = $this->checkWhere($where);
        $sql = $this->wpdb->query("DELETE FROM {$this->table} WHERE {$where}");

        return $sql;
    }

    public function deleteByNotIn($field, $data)
    {
        $this->wpdb->query("DELETE FROM " . $this->table . " WHERE $field NOT IN (" . implode(",", $data) . ")");
    }

    public function all()
    {
        $data = $this->wpdb->get_results("SELECT * FROM " . $this->table);

        return $data;
    }
}