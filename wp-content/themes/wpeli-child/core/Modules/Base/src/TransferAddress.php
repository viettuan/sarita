<?php

namespace EliChild\Modules\Base;

use Cake\Collection\Collection;

class TransferAddress
{
    public static function getAllCountries()
    {
        global $app;

        return $app['db']->fetchAll('SELECT * FROM countries');
    }

    public static function getListCountries($select = '*')
    {
        global $app;

        $countriesAvailable = self::getFieldAvailable('setting_countries_available');

        return $app['db']->fetchAll('SELECT ' . $select . ' FROM countries WHERE id IN (' . $countriesAvailable . ')');
    }

    public static function getAllPhoneCode()
    {
        global $app;
        $data = $app['db']->fetchAll('SELECT phonecode, iso2, id FROM countries order by iso2');

        return $data;
    }

    private static function getFieldAvailable($name)
    {
        $data = get_field($name, 'option');

        if (is_array($data)) {
            if (($key = array_search('[]', $data)) !== false) {
                unset($data[$key]);
            }

            $data = !empty($data) ? implode(',', $data) : 'NULL';
        } else {
            $data = 'NULL';
        }

        return $data;
    }

    public static function getListPhoneCode()
    {
        global $app;
        $phoneCodesAvailable = self::getFieldAvailable('setting_phone_codes_available');

        $rows = $app['db']->fetchAll('SELECT phonecode, iso2 FROM countries WHERE id IN (' . $phoneCodesAvailable . ') ORDER BY FIELD(id, ' . $phoneCodesAvailable . ')');
        $data = [];
        foreach ($rows as $row) {
            $data[] = [
                'value' => $row['phonecode'],
                'label' => $row['phonecode'],
                'iso2' => $row['iso2'],
            ];
        }

        return $data;
    }

    public static function getStatesByCountry($countryId)
    {
        global $app;
        if (empty($countryId)) {
            $countryId = 0;
        }
        $rows = $app['db']->fetchAll('SELECT id, name, code FROM states where country_id = ' . $countryId);
        $data = [];
        foreach ($rows as $row) {
            $data[] = [
                'value' => $row['code'],
                'label' => $row['name'],
            ];
        }

        return $data;
    }

    public static function getCitiesByCountry($countryId, $cityValue = null)
    {
        global $app;
        if (empty($countryId)) {
            $countryId = 0;
        }

        $query = 'SELECT distinct name, country_id FROM cities where country_id = ' . $countryId;
        if (!empty($cityValue)) {
            $query .= ' and name = "' . $cityValue . '"';
        }

        $rows = $app['db']->fetchAll($query);
        $data = [];
        foreach ($rows as $row) {
            $data[] = [
                'value' => $row['name'],
                'label' => $row['name'],
            ];
        }

        return $data;
    }

    public static function getCountryDetail($countryId)
    {
        global $app;

        return $app['db']->fetchAssoc('SELECT * FROM countries where id = ' . $countryId);
    }

    public static function getCountryDetailByPhoneCode($phoneCode)
    {
        global $app;

        return $app['db']->fetchAssoc('SELECT * FROM countries where phonecode = ' . $phoneCode);
    }

    public static function getCountryDetailByIso($iso)
    {
        global $app;

        return $app['db']->fetchAssoc('SELECT * FROM countries where iso2 = "' . $iso . '"');
    }
}
