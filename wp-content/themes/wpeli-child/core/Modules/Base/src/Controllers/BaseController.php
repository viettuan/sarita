<?php

namespace EliChild\Modules\Base\Controllers;

use Silex\Application;

class BaseController
{
    protected $app;

    public function __construct()
    {
        $this->app = new Application();
    }
}