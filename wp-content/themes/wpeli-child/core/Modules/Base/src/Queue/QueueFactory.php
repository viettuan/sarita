<?php

namespace EliChild\Modules\Base\Queue;

use Bernard\Driver\PredisDriver;
use Bernard\Normalizer\EnvelopeNormalizer;
use Bernard\QueueFactory\PersistentFactory;
use Bernard\Serializer;
use Normalt\Normalizer\AggregateNormalizer;
use Predis\Client;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Bernard\Driver\DoctrineDriver;
use Doctrine\DBAL\DriverManager;

class QueueFactory
{
    private $driver;

    public function __construct()
    {
        $this->createDriver();
    }

    private function createDriver()
    {
        $queueConnection = getenv('QUEUE_DRIVER', 'predis');

        switch ($queueConnection) {
            case 'database':
                try {
                    $connection = DriverManager::getConnection(array(
                        'dbname' => getenv('DB_DATABASE'),
                        'user' => getenv('DB_USERNAME'),
                        'password' => getenv('DB_PASSWORD'),
                        'host' => getenv('DB_HOST'),
                        'driver' => 'pdo_mysql',
                    ));

                    $this->driver = new DoctrineDriver($connection);
                } catch (\Exception $e) {
                }

                break;

            case 'predis':
                $predis = new Client('tcp://localhost', array(
                    'prefix' => 'bernard:',
                ));
                $this->driver = new PredisDriver($predis);

                break;
        }
    }

    public function factory()
    {
        if($this->driver) {
            return new PersistentFactory(
                $this->driver,
                new Serializer(
                    new AggregateNormalizer([
                        new EnvelopeNormalizer(),
                        new \Symfony\Component\Serializer\Serializer(
                            [new ObjectNormalizer()],
                            [new JsonEncoder()]
                        ),
                    ])
                )
            );
        }

        return null;
    }
}