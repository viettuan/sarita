<?php

namespace EliChild\Modules\Base\Traits;

trait LoadAndPublishDataTrait
{
    /**
     * @var bool
     */
    protected $is_in_console = false;

    /**
     * @var string
     */
    protected $namespace = null;

    /**
     * @param $namespace
     * @return $this
     */
    public function setNamespace($namespace): self
    {
        $this->namespace = ucwords(trim(rtrim($namespace, '/'), '/'));

        return $this;
    }

    /**
     * @param $is_in_console
     * @return $this
     */
    public function setIsInConsole($is_in_console): self
    {
        $this->is_in_console = $is_in_console;

        return $this;
    }

    public function runningInConsole()
    {
        if (isset($_ENV['APP_RUNNING_IN_CONSOLE'])) {
            return $_ENV['APP_RUNNING_IN_CONSOLE'] === 'true';
        }

        return php_sapi_name() === 'cli' || php_sapi_name() === 'phpdbg';
    }

    public function loadEvents()
    {
        global $globalData;

        foreach ($this->listens() as $event => $listener) {
            $globalData['events'][$event] = $listener;
        }

        return $this;
    }

    /**
     * Publish the given configuration file name (without extension) and the given module
     * @param $file_names
     * @return $this
     */
    public function loadRoutes($file_names = ['web']): self
    {
        if (!is_array($file_names)) {
            $file_names = [$file_names];
        }

        if (!isset($this->app['config.routes'])) {
            $this->app['config.routes'] = [];
        }

        foreach ($file_names as $file_name) {
            $data = include $this->getRouteFilePath($file_name);

            $this->app['config.routes'] += $data;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function loadMigrations(): self
    {
        global $globalData;

        if ($this->is_in_console) {
            $globalData['migrations'][] = __DIR__ . '/../../../../..' . $this->getDashedNamespace() . '/db/migrations/';
            $globalData['seeds'][] = __DIR__ . '/../../../../..' . $this->getDashedNamespace() . '/db/seeds/';
        }

        return $this;
    }

    /**
     * Publish the given configuration file name (without extension) and the given module
     * @param $file_names
     * @return $this
     */
    public function loadAndPublishConfigurations($file_names): self
    {
        if (!is_array($file_names)) {
            $file_names = [$file_names];
        }

        foreach ($file_names as $file_name) {
            include $this->getConfigFilePath($file_name);
        }

        return $this;
    }

    /**
     * @param $file
     * @return string
     */
    protected function getRouteFilePath($file)
    {
        return __DIR__ . '/../../../../..' . $this->getDashedNamespace() . '/routes/' . $file . '.php';
        //return get_theme_file_path() . $this->getDashedNamespace() . '/routes/' . $file . '.php';
    }

    /**
     * @return mixed
     */
    protected function getDotedNamespace()
    {
        return str_replace('/', '.', $this->namespace);
    }

    /**
     * @return mixed
     */
    protected function getDashedNamespace()
    {
        return '/core/' . str_replace('.', '/', $this->namespace);
    }

    /**
     * @param $file
     * @return string
     */
    protected function getConfigFilePath($file)
    {
        return __DIR__ . '/../../../../..' . $this->getDashedNamespace() . '/config/' . $file . '.php';
        //return get_theme_file_path() . $this->getDashedNamespace() . '/routes/' . $file . '.php';
    }


    /**
     * Get the events and handlers.
     *
     * @return array
     */
    public function listens()
    {
        return $this->listen ?? [];
    }
}