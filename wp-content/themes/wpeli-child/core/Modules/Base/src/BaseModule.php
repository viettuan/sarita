<?php

namespace EliChild\Modules\Base;

class BaseModule
{
    public static function initialize()
    {
        return new self();
    }

    public function __construct()
    {

    }
}