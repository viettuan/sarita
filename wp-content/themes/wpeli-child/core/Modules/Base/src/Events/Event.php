<?php

namespace EliChild\Modules\Base\Events;

use Bernard\Message;

abstract class Event implements Message
{
    public $queue = 'default';

    public function getName()
    {
        return get_class($this);
    }
}