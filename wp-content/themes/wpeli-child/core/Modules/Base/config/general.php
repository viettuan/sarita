<?php

const ERRORS_CODE = [
    'SR_1000' => 'User isn\'t existed from Sarita Lighthouse',
    'SR_1001' => 'Creating new user has failed',
    'SR_1002' => 'Updating user data has failed',
    'SR_1003' => 'Deleting user has failed',
    'SR_1004' => 'Subscription doesn\'t exist',
    'SR_1005' => 'Creating new subscription has failed',
    'SR_1006' => 'Cancellation the subscription has failed',
    'SR_1007' => 'Updating new password has failed',
];
