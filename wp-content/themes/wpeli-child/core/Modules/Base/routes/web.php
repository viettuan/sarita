<?php

return [
    'admin' => [
        'name' => 'admin',
        'pattern' => '/hello/admin',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\StoreController::index',
        'method' => array('get', 'post')
    ],
];
