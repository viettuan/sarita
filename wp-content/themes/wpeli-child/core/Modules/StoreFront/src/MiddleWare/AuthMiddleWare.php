<?php

namespace EliChild\Modules\StoreFront\MiddleWare;

use EliChild\Modules\Authentication\Services\Authentication;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class AuthMiddleWare
{
    public function __invoke(Request $request, Application $app)
    {
        if (!Authentication::check()) {
            return redirect($app["url_generator"]->generate('home'));
        }
    }
}