<?php

namespace EliChild\Modules\StoreFront\Listeners;

use EliChild\Modules\Customer\Models\CustomerModel;
use EliChild\Modules\StoreFront\Events\UpdatedCustomerDetailEvent;
use EliChild\Modules\StoreFront\Mails\UpdateCustomerDetailMail;

class UpdatedCustomerDetailHandler
{
    public function __invoke(UpdatedCustomerDetailEvent $event)
    {
        $args = $event->args;
        $customerId = $args['customer_id'];
        $customerModel = new CustomerModel($customerId);

        (new UpdateCustomerDetailMail())->sendMail([
            'email' => $customerModel->getEmail(),
            'name' => $customerModel->getFullName(),
        ]);
    }
}