<?php

namespace EliChild\Modules\StoreFront\Listeners;

use EliChild\Modules\ApiClient\Events\LogCallApiEvent;
use EliChild\Modules\ApiClient\Services\SubscriptionCurl;
use EliChild\Modules\Base\Supports\EliMailChimpSubscribe;
use EliChild\Modules\StoreFront\Mails\PurchaseEmailWthExistCustomerMail;
use EliChild\Modules\StoreFront\Mails\PurchaseEmailWthPasswordMail;
use EliChild\Modules\Customer\Models\CustomerModel;
use EliChild\Modules\Order\Models\OrderModel;
use EliChild\Modules\StoreFront\Events\OrderPurchaseEvent;
use QuickPay\API\Exception;

class OrderPurchaseHandler
{
    public function __invoke(OrderPurchaseEvent $event)
    {
        $args = $event->args;
        $orderId = $args['order_id'];
        $isGuest = $args['customer_guest'];
        $order = new OrderModel($orderId);

        $customerId = $order->getCustomerId();
        $customerModel = new CustomerModel($customerId);
        $saritaId = $customerModel->getSaritaId();

        //update to lighthouse
        if ($isGuest && empty($saritaId)) {
            $customerModel->createUserToLighthouse($customerId);
            $customerModel = new CustomerModel($customerId);
            $saritaId = $customerModel->getSaritaId();
        }

        if (!empty($saritaId)) {
            //TODO process for prepayment
            $subData = (new SubscriptionCurl())->createSubscription($saritaId, [
                'subscription_type' => $order->getSubscriptionType(),
                'pre_payment' => $order->getPrePayment(),
                'monthly_payment' => $order->getMonthlyPayment(),
                'currency' => $order->getCurrency(),
                'valid_until' => !empty($order->getValidUntil()) ? $order->getValidUntil() : null,
                'status' => 'active',
                'pearl_design_name' => $order->getPearlName(),
                'pearl_design_code' => $order->getPearlDesignCode(),
            ]);

            if ($subData['status'] == 'success') {
                update_post_meta($orderId, 'sarita_subscription_id', $subData['data']['data']);
            }

            $state = !empty($order->getState()) ? $order->getState() . ',' : '';

            $subscriptionType = $order->getSubscriptionType();

            try {
                $subscriptionLabel = get_field($subscriptionType . '_subscription_name', 'options', false);
                $subscriptionLabel = explode("[:da]", $subscriptionLabel);
                if (isset($subscriptionLabel[1])) {
                    $subscriptionType = str_replace("[:]", "", $subscriptionLabel[1]);
                } else if (isset($subscriptionLabel[0])) {
                    $subscriptionType = $subscriptionLabel[0];
                }

            } catch (Exception $e) {}

            $dataForSendEmail = [
                'email' => $order->getEmail(),
                'name' => $order->getFullName(),
                'password' => base64_decode($customerModel->getPasswordHash()),
                'address' => $order->getAddress() . ', ' .
                    $order->getCity() . ', '
                    . $state .
                    $order->getZipcode() . ', ' .
                    $order->getCountry(),
                'order_id' => $orderId,
                'order_date' => $order->getDate(),
                'order_pearl' => $order->getPearlName(),
                'order_price' => $order->getPrePayment(),
                'subscription_type' => $subscriptionType,
                'payment_method' => $order->getPaymentMethod(),
            ];

            if ($isGuest) {
                event(new LogCallApiEvent([
                    'type' => "send email",
                    'request' => "send purchase email for guest",
                    'response' => ['status' => 'success'],
                ]));

                (new PurchaseEmailWthPasswordMail())->sendMail($dataForSendEmail);
            } else {
                event(new LogCallApiEvent([
                    'type' => "send email",
                    'request' => "send email purchase for exist customer",
                    'response' => ['status' => 'success'],
                ]));

                (new PurchaseEmailWthExistCustomerMail())->sendMail($dataForSendEmail);
            }

            //subscribe newsletter
            if (!empty($customerModel->getisReceiveNewsletter())) {
                (new EliMailChimpSubscribe())->subscribe($customerModel->getEmail());
            }
        }
    }
}