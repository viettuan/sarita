<?php

namespace EliChild\Modules\StoreFront\Listeners;

use EliChild\Modules\Customer\Models\CustomerModel;
use EliChild\Modules\StoreFront\Events\UpdatedPaymentMethodEvent;
use EliChild\Modules\StoreFront\Mails\UpdatePaymentMethodInfoMail;

class UpdatedPaymentMethodHandler
{
    public function __invoke(UpdatedPaymentMethodEvent $event)
    {
        $args = $event->args;
        $customerId = $args['customer_id'];
        $customerModel = new CustomerModel($customerId);

        (new UpdatePaymentMethodInfoMail())->sendMail([
            'email' => $customerModel->getEmail(),
            'name' => $customerModel->getFullName(),
        ]);
    }
}