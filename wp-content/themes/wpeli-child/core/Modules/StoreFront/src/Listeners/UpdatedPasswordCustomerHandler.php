<?php

namespace EliChild\Modules\StoreFront\Listeners;

use EliChild\Modules\Customer\Models\CustomerModel;
use EliChild\Modules\StoreFront\Events\UpdatedPasswordCustomerEvent;
use EliChild\Modules\StoreFront\Mails\UpdatePasswordMail;

class UpdatedPasswordCustomerHandler
{
    public function __invoke(UpdatedPasswordCustomerEvent $event)
    {
        $args = $event->args;
        $customerId = $args['customer_id'];
        $customerModel = new CustomerModel($customerId);

        (new UpdatePasswordMail())->sendMail([
            'email' => $customerModel->getEmail(),
            'name' => $customerModel->getFullName(),
        ]);
    }
}