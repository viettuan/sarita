<?php

namespace EliChild\Modules\StoreFront\Listeners;

use EliChild\Helpers\Helper;
use EliChild\Modules\ApiClient\Services\SubscriptionCurl;
use EliChild\Modules\Order\Models\OrderModel;
use EliChild\Modules\StoreFront\Events\CancelledSubscriptionEvent;
use EliChild\Modules\StoreFront\Mails\CancelledSubscriptionMail;

class CancelledSubscriptionHandler
{
    public function __invoke(CancelledSubscriptionEvent $event)
    {
        $args = $event->args;
        $orderId = $args['order_id'];

        $order = new OrderModel($orderId);

        //request cancel subscription to lighthouse
        if (!empty($order->getSaritaSubscriptionId())) {
            $subData = (new SubscriptionCurl())->cancelSubscription($order->getSaritaSubscriptionId());
        }

        //TODO
        (new CancelledSubscriptionMail())->sendMail([
            'email' => $order->getEmail(),
            'name' => $order->getFullName(),
            'pearl_number' => $order->getPearlDesignCode(),
            'cancelled_date' => Helper::getToday(),
            'validity_date' => Helper::getToday(),
        ]);
    }
}