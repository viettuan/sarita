<?php

namespace EliChild\Modules\StoreFront\Listeners;

use EliChild\Modules\StoreFront\Mails\ForgotPasswordMail;
use EliChild\Modules\StoreFront\Events\ResetPasswordEvent;

class ResetPasswordHandler
{
    public function __invoke(ResetPasswordEvent $event)
    {
        $args = $event->args;

        (new ForgotPasswordMail())->sendMail([
            'email' => $args['email'] ?? '',
            'name' => $args['name'] ?? '',
            'new_password' => $args['password'] ?? '',
        ]);
    }
}