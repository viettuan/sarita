<?php

namespace EliChild\Modules\StoreFront\Events;

use EliChild\Modules\Base\Events\Event;

class OrderPurchaseEvent extends Event
{
    public $args;

    public function __construct($args)
    {
        $this->args = $args;
    }
}