<?php

namespace EliChild\Modules\Sample\Models;

use Eli\Modules\Model;

class StoreFrontModel extends Model
{
    /**
     * StoreFrontModel constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}