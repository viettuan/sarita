<?php

namespace EliChild\Modules\StoreFront\Models;

use Eli\Modules\Model;
use EliChild\Modules\Authentication\Services\Authentication;
use EliChild\Modules\StoreFront\Services\PageService;

class ThemeModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    private static function getFieldOption($options, $field, $default = '')
    {
        return $options[$field] ?? get_field($field, 'option') ?? $default;
    }

    private static function getListFooterMenu()
    {
        $menuLocations = get_nav_menu_locations();
        $result = [];

        $menus = wp_get_nav_menu_items($menuLocations['footer_menu_' . qtrans_getLanguage()] ?? 0);
        if (!empty($menus)) {
            foreach ($menus as $menu) {
                if(isset($menu->url) && !empty($menu->url)){
                    $menuUrl = rtrim($menu->url,'/');
                    $menuUrl = str_replace('/?','?',$menuUrl);
                } else {
                    $menuUrl = '';
                }
                $result[] = [
                    'title' => $menu->title ?? '',
                    'href' => $menuUrl,
                ];
            }
        }

        return $result;
    }

    public static function getData()
    {
        $optionsData = get_fields('options');
        $userLogin = Authentication::user();
        $enabledLanguages = get_option('qtranslate_enabled_languages');
        $headerLanguages = [];

        foreach ($enabledLanguages as $lang) {
            $headerLanguages[] = [
                'title' => $lang,
                'lang' => $lang,
            ];
        }
        $data = [
            "isAdapter" => false,
            "siteDescription" => $optionsData['title'] ?? '',
            "user" => $userLogin,
            "feAssetsUrl" => get_stylesheet_directory_uri() . '/assets',
            "ajaxUrl" => get_site_url() . "/ajax",
            "siteConfig" => [
                "favicon" => PageService::getLinkImageFromId(self::getFieldOption($optionsData, 'favicon', feAssets('assets/icons/favicon.ico?v=1.0'))),
                "headerLogos" => [
                    'logoDark' => PageService::getLinkImageFromId(self::getFieldOption($optionsData, 'logo_dark', feAssets('images/logo.svg'))),
                    'logoWhite' => PageService::getLinkImageFromId(self::getFieldOption($optionsData, 'logo_light', feAssets('images/logo-w.svg'))),
                ],
                "termsAndConditionsUrl" => self::getFieldOption($optionsData, 'terms_and_conditions_link'),
                "headerLanguages" => $headerLanguages,
                "footerPages" => self::getListFooterMenu(),
                "footerContact" => [
                    "phones" => self::getFieldOption($optionsData, 'phone_contact'),
                    "email" => self::getFieldOption($optionsData, 'email_contact'),
                    "address" => self::getFieldOption($optionsData, 'address'),
                    "dayOpen" => self::getFieldOption($optionsData, 'day_open'),
                    "timeOpen" => self::getFieldOption($optionsData, 'time_open'),
                    "timeClose" => self::getFieldOption($optionsData, 'time_close'),
                    "socials" => [
                        [
                            "title" => "Facebook",
                            "icon" => "icon-facebook",
                            "link" => self::getFieldOption($optionsData, 'facebook')
                        ],
                        [
                            "title" => "Twitter",
                            "icon" => "icon-twitter",
                            "link" => self::getFieldOption($optionsData, 'twitter')
                        ],
                        [
                            "title" => "Instagram",
                            "icon" => "icon-instagram",
                            "link" => self::getFieldOption($optionsData, 'instagram')
                        ]
                    ]
                ],
                "footerCopyRight" => [
                    "copyRight" => self::getFieldOption($optionsData, 'copyright'),
                    "footerLogos" => [
                        [
                            "title" => "Trustpilot",
                            "logoSrc" => feAssets('images/trustpilot.png'),
                            "link" => self::getFieldOption($optionsData, 'trustpilot_link','#')
                        ],
                        [
                            "title" => "Guiden65",
                            "logoSrc" => feAssets('images/guiden65.png'),
                            "link" => self::getFieldOption($optionsData, 'guiden65_link','#')
                        ]
                    ]
                ]
            ]
        ];

        return $data;
    }
}