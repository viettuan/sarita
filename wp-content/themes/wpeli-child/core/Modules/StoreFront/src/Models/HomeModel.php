<?php

namespace EliChild\Modules\StoreFront\Models;

use Eli\Modules\Model;
use EliChild\Modules\StoreFront\Services\PageService;

class HomeModel extends Model
{
    /**
     * HomeModel constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getPageData()
    {
        $post = get_page_by_path(PageService::getAliasPageByKey('HOME_PAGE'));
        $pageData = [];

        if (!empty($post)) {
            $post = get_field_objects($post->ID);
            $pageData = [
                "pageHero" => $this->getDataPageHero($post),
                "sectionAbout" => $this->getDataSectionAbout($post),
                "sectionAccessory" => $this->getDataSectionAccessory($post),
                "sectionMobileApp" => $this->getDataSectionMobileApp($post),
                "sectionTestimonials" => $this->getDataSectionTestimonials($post),
                "sectionSubscription" => $this->getDataSectionSubscription($post)
            ];
        }

        return $pageData;
    }

    private function getDataPageHero($post)
    {
        $postIdLink = $post['home_banner_link_view_more']['value'];
        $linkViewMore = '';
        if (!empty($postIdLink)) {
            $postLink = get_post($postIdLink);
            if (!empty($postLink)) {
                $linkViewMore = $postLink->post_name;
            }
        }

        return [
            "imgSrc" => PageService::getLinkImageFromId($post['home_banner_background_image']['value'] ?? ''),
            "imgSrcTablet" => PageService::getLinkImageFromId($post['home_banner_background_image_tablet']['value'] ?? ''),
            "imgSrcMobile" => PageService::getLinkImageFromId($post['home_banner_background_image_mobile']['value'] ?? ''),
            "title" => $post['home_banner_title']['value'],
            "subTitle" => $post['home_banner_subtitle']['value'] ?? '',
            "description" => PageService::getTextArea($post['home_banner_description']['value']),
            "btnText" => $post['home_banner_text_view_more']['value'] ?? 'Learn More',
            "btnLink" => $linkViewMore
        ];
    }

    private function getDataSectionAbout($post)
    {
        $logo = PageService::getLinkImageFromId($post['about_pearl_logo']['value']);
        if (empty($logo)) {
            $logo = feAssets('images/logo-pearl.svg');
        }

        $postIdLink = $post['about_pearl_link_view_more']['value'];
        $linkViewMore = '';
        if (!empty($postIdLink)) {
            $postLink = get_post($postIdLink);
            if (!empty($postLink)) {
                $linkViewMore = $postLink->post_name;
            }
        }

        return [
            "logo" => $logo,
            "title" => $post['about_pearl_title']['value'],
            "text" => PageService::getTextArea($post['about_pearl_description']['value']),
            "btnText" => $post['about_pearl_text_view_more']['value'] ?? 'Buy Sarita Pearl',
            "btnLink" => $linkViewMore,
            "sliderImages" => $this->getRelationshipAfc($post['about_pearl_pearl_list']['value'] ?? '')
        ];
    }

    private function getDataSectionAccessory($post)
    {
        $banner = PageService::getLinkImageFromId($post['home_accessory_background_image']['value']);
        if (empty($banner)) {
            $banner = feAssets('images/bg-home-accessory.jpg');
        }

        $postIdLink = $post['home_accessory_link_viewmore']['value'];
        $linkViewMore = '';
        if (!empty($postIdLink)) {
            $postLink = get_post($postIdLink);
            if (!empty($postLink)) {
                $linkViewMore = $postLink->post_name;
            }
        }

        return [
            "title" => PageService::getTextArea($post['home_accessory_title']['value']),
            "backgroundImage" => $banner,
            "btnText" => $post['home_accessory_text_viewmore']['value'] ?? 'Learn More',
            "btnLink" => $linkViewMore,
            "accessoryList" => $this->getAccessoryList($post['accessory-items']['value'])
        ];
    }

    private function getDataSectionMobileApp($post)
    {
        $banner = PageService::getLinkImageFromId($post['apps_store_image']['value']);
        if (empty($banner)) {
            $banner = feAssets('images/mobile.png');
        }

        return [
            "title" => $post['apps_stores_title']['value'],
            "description" => PageService::getTextArea($post['apps_stores_description']['value']),
            "imgSrc" => $banner,
            "downloadLink" => [
                [
                    "title" => $post['android_download_link']['label'],
                    "link" => $post['android_download_link']['value'],
                    "imgSrc" => feAssets("images/google-play.png")
                ],
                [
                    "title" => $post['ios_download_link']['label'],
                    "link" => $post['ios_download_link']['value'],
                    "imgSrc" => feAssets("images/app-store.png")
                ]
            ]
        ];
    }

    private function getDataSectionTestimonials($post)
    {
        $banner = PageService::getLinkImageFromId($post['home_page_testimonial_background_image']['value']);
        if (empty($banner)) {
            $banner = feAssets('images/bg-home-testimonial.jpg');
        }

        $postIdLink = $post['home_page_testimonial_link_view_more']['value'];
        $linkViewMore = '';
        if (!empty($postIdLink)) {
            $postLink = get_post($postIdLink);
            if (!empty($postLink)) {
                $linkViewMore = $postLink->post_name;
            }
        }

        return [
            "backgroundImage" => $banner,
            "title" => PageService::getTextArea($post['home_page_testimonial_title']['value']),
            "btnText" => $post['home_page_testimonial_text_view_more']['value'] ?? 'Buy Sarita Pearl',
            "btnLink" => $linkViewMore,
            "testimonialList" => $this->testimonialList($post['home_page_testimonial_list']['value'])
        ];
    }

    private function getDataSectionSubscription($post)
    {
        return [
            "title" => $post['homepage_subscription_title']['value'],
            "description" => PageService::getTextArea($post['homepage_subscription_description']['value']),
            "subscriptionTypes" => $this->subscriptionList()
        ];
    }

    public function getRelationshipAfc($list)
    {
        $result = [];
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $result[] = [
                    'imgsrc' => get_the_post_thumbnail_url($value),
                    'alt' => get_the_post_thumbnail_caption($value),
                ];
            }
        }

        return $result;
    }

    public function getAccessoryList($list)
    {
        $result = [];
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $post = PageService::translatePost($value);
                if (!empty($post)) {
                    $icon = get_post_meta($value, 'icon_class');
                    $result[] = [
                        "title" => $post->post_title,
                        "iconClass" => $icon[0] ?? '',
                        "description" => PageService::getTextArea(get_field('accessories_description', $value))
                    ];
                }
            }
        }

        return $result;
    }

    public function testimonialList($list)
    {
        $result = [];
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $post = PageService::translatePost($value);
                if(!empty($post)){
                    $fields = get_field_objects($value);
                    $result[] = [
                        "name" => $post->post_title,
                        "imgSrc" => PageService::getLinkImageFromId($fields['testimonial_avatar']['value'] ?? ''),
                        "position" => $fields['testimonial_position']['value'] ?? '',
                        "description" => $fields['testimonial_description']['value']
                    ];
                }
            }
        }

        return $result;
    }

    public function subscriptionList()
    {
        $optionsData = get_fields('options');
        $result = [];

        $subscriptionModel = new SubscriptionModel();
        if (!empty($optionsData['monthly_subscription_enable'])) {
            $result[] = $subscriptionModel->getMonthlySubscription();
        }

        if (!empty($optionsData['once_off_subscription_enable'])) {
            $result[] = $subscriptionModel->getOnceOffSubscription();
        }

        if (!empty($optionsData['annual_subscription_enable'])) {
            $result[] = $subscriptionModel->getAnnualSubscription();
        }

        return $result;
    }
}