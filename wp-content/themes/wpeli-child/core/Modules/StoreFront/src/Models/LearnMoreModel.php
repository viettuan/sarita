<?php

namespace EliChild\Modules\StoreFront\Models;

use Eli\Modules\Model;
use EliChild\Modules\StoreFront\Services\PageService;

class LearnMoreModel extends Model
{
    /**
     * LearnMoreModel constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getPageData()
    {
        $post = get_page_by_path(PageService::getAliasPageByKey('LEARN_MORE_PAGE'));
        $pageData = [];
        if (!empty($post)) {
            $post = get_field_objects($post->ID);

            $pageData = [
                'sectionFeatures' => [
                    'logoSrc' => PageService::getLinkImageFromId($post['learn_more_feature_logo']['value']),
                    'imgSrc' => PageService::getLinkImageFromId($post['learn_more_feature_image']['value']),
                    'title' => $post['learn_more_feature_title']['value'] ?? '',
                    'text' => PageService::getTextArea($post['learn_more_feature_description']['value']),
                    'btnText' => $post['learn_more_feature_app_button_label']['value'],
                    'btnLink' => $this->getLinkViewMore($post['learn_more_feature_app_button_link']['value']),
                    'features' => $this->getAccessoryList($post['learn_more_accessories_list']['value']),
                ],
                'learnMoreItems' => [
                    [
                        'imgsrc' => PageService::getLinkImageFromId($post['learn_more_mobile_app_image_featured']['value']),
                        'title' => $post['learn_more_mobile_app_title']['value'],
                        'description' => PageService::getTextArea($post['learn_more_mobile_app_description']['value']),
                        'btnText' => $post['learn_more_mobile_app_button_label']['value'],
                        'btnLink' => $this->getLinkViewMore($post['learn_more_mobile_app_button_link']['value'])
                    ], [
                        'imgsrc' => PageService::getLinkImageFromId($post['learn_more_manual_call_button_image_featured']['value']),
                        'title' => $post['learn_more_manual_call_button_title']['value'],
                        'description' => PageService::getTextArea($post['learn_more_manual_call_button_description']['value']),
                        'btnText' => $post['learn_more_manual_call_button_button_label']['value'],
                        'btnLink' => $this->getLinkViewMore($post['learn_more_manual_call_button_button_link']['value']),
                    ], [
                        'icon' => $post['learn_more_auto_detection_icon_featured']['value'],
                        'title' => $post['learn_more_auto_detection_button_title']['value'],
                        'description' => PageService::getTextArea($post['learn_more_auto_detection_description']['value']),
                        'btnText' => $post['learn_more_auto_detection_button_label']['value'],
                        'btnLink' => $this->getLinkViewMore($post['learn_more_auto_detection_button_link']['value']),
                    ], [
                        'slider' => $this->getPearlDesign($post['learn_more_customisable_casings_pearls']['value']),
                        'title' => $post['learn_more_customisable_casings_title']['value'],
                        'description' => PageService::getTextArea($post['learn_more_customisable_casings_description']['value']),
                        'btnText' => $post['learn_more_customisable_casings_button_label']['value'],
                        'btnLink' => $this->getLinkViewMore($post['learn_more_customisable_casings_button_link']['value']),
                    ], [
                        'icon' => $post['learn_more_gps_icon_featured']['value'],
                        'title' => $post['learn_more_gps_title']['value'],
                        'description' => PageService::getTextArea($post['learn_more_gps_description']['value']),
                        'btnText' => $post['learn_more_gps_button_label']['value'],
                        'btnLink' => $this->getLinkViewMore($post['learn_more_gps_button_link']['value']),
                    ], [
                        'imgsrc' => PageService::getLinkImageFromId($post['learn_more_magnetic_lock_image_featured']['value']),
                        'title' => $post['learn_more_magnetic_lock_title']['value'],
                        'description' => PageService::getTextArea($post['learn_more_magnetic_lock_description']['value']),
                        'btnText' => $post['learn_more_magnetic_lock_button_label']['value'],
                        'btnLink' => $this->getLinkViewMore($post['learn_more_magnetic_lock_button_link']['value']),
                    ],
                ]
            ];
        }

        return $pageData;
    }

    private function getLinkViewMore($postId)
    {
        $linkViewMore = '';
        if (!empty($postId)) {
            $postLink = get_post($postId);
            if (!empty($postLink)) {
                $linkViewMore = $postLink->post_name;
            }
        }

        return $linkViewMore;
    }

    public function getAccessoryList($list)
    {
        $result = [];
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $post = PageService::translatePost($value);
                if(!empty($post)){
                    $result[] = [
                        "title" => $post->post_title,
                        "iconClass" => get_field_objects($value)['icon_class']['value'],
                        "description" => $post->post_content
                    ];
                }
            }
        }

        return $result;
    }

    public function getPearlDesign($list)
    {
        $result = [];
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $result[] = [
                    'imgsrc' => get_the_post_thumbnail_url($value),
                    'alt' => get_the_post_thumbnail_caption($value),
                ];
            }
        }

        return $result;
    }
}