<?php

namespace EliChild\Modules\StoreFront\Models;

use Eli\Modules\Model;
use EliChild\Modules\Order\Helpers\OrderConstant;

class SubscriptionModel
{
    protected $name;
    protected $title;
    protected $total;
    protected $currency;
    protected $prePayment;
    protected $monthlyPayment;
    protected $priceInfos;
    protected $btnText;
    protected $btnLink;
    protected $infoTexts;
    protected $notes;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total): void
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getPrePayment()
    {
        return $this->prePayment;
    }

    /**
     * @param mixed $prePayment
     */
    public function setPrePayment($prePayment): void
    {
        $this->prePayment = $prePayment;
    }

    /**
     * @return mixed
     */
    public function getMonthlyPayment()
    {
        return $this->monthlyPayment;
    }

    /**
     * @param mixed $monthlyPayment
     */
    public function setMonthlyPayment($monthlyPayment): void
    {
        $this->monthlyPayment = $monthlyPayment;
    }

    /**
     * @return mixed
     */
    public function getPriceInfos()
    {
        return $this->priceInfos;
    }

    /**
     * @param mixed $priceInfos
     */
    public function setPriceInfos($priceInfos): void
    {
        $this->priceInfos = $priceInfos;
    }

    /**
     * @return mixed
     */
    public function getBtnText()
    {
        return $this->btnText;
    }

    /**
     * @param mixed $btnText
     */
    public function setBtnText($btnText): void
    {
        $this->btnText = $btnText;
    }

    /**
     * @return mixed
     */
    public function getBtnLink()
    {
        return $this->btnLink;
    }

    /**
     * @param mixed $btnLink
     */
    public function setBtnLink($btnLink): void
    {
        $this->btnLink = $btnLink;
    }

    /**
     * @return mixed
     */
    public function getInfoTexts()
    {
        return $this->infoTexts;
    }

    /**
     * @param mixed $infoTexts
     */
    public function setInfoTexts($infoTexts): void
    {
        $this->infoTexts = $infoTexts;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes): void
    {
        $this->notes = $notes;
    }

    /**
     * StoreFrontModel constructor.
     */
    public function __construct($type = null)
    {
        if (!empty($type)) {
            $this->setSubscriptionData($type);
        }
    }

    public function setSubscriptionData($type)
    {
        $subscription = $this->getSubscriptionData($type);

        $this->setName($subscription['name'] ?? '');
        $this->setTitle($subscription['title'] ?? '');
        $this->setTotal($subscription['total'] ?? 0);
        $this->setCurrency($subscription['currency'] ?? 'DKK');
        $this->setPrePayment($subscription['pre_payment'] ?? 0);
        $this->setMonthlyPayment($subscription['monthly_payment'] ?? 0);
        $this->setPriceInfos($subscription['priceInfos'] ?? []);
        $this->setBtnText($subscription['btnText'] ?? []);
        $this->setBtnLink($subscription['btnLink'] ?? []);
        $this->setInfoTexts($subscription['infoTexts'] ?? []);
        $this->setNotes($subscription['notes'] ?? []);
    }

    public function getSubscriptionData($type)
    {
        switch ($type) {
            case OrderConstant::SUBSCRIPTION_MONTHLY:
                return $this->getMonthlySubscription();
                break;

            case OrderConstant::SUBSCRIPTION_ONE_OFF:
                return $this->getOnceOffSubscription();
                break;

            case OrderConstant::SUBSCRIPTION_ANNUAL:
                return $this->getAnnualSubscription();
                break;

            default:
                return null;
        }
    }

    public function getMonthlySubscription()
    {
        $options = get_fields('options');
        $prePaymentAmount = $this->getFieldOption($options, 'monthly_subscription_prepayment_amount', 0);
        $amount = $this->getFieldOption($options, 'monthly_subscription_amount', 0);
        $currency = 'DKK';

        return [
            "name" => OrderConstant::SUBSCRIPTION_MONTHLY,
            "title" => $this->getFieldOption($options, 'monthly_subscription_name'),
            "total" => $prePaymentAmount + $amount,
            "currency" => $currency,
            'pre_payment' => $prePaymentAmount,
            'monthly_payment' => $amount,
            "priceInfos" => [
                [
                    "title" => trans("Prepayment"),
                    "price" => $prePaymentAmount
                ],
                [
                    "title" => trans("Monthly Payment"),
                    "price" => $amount
                ],
            ],
            "btnText" => $this->getFieldOption($options, 'monthly_subscription_button_text', 'Order Now'),
            "btnLink" => $this->getFieldOption($options, 'monthly_subscription_button_link', '#'),
            'infoTexts' => [
                trans('The prepayment will be withdrawn at the time of purchase'),
                trans('The monthly payment will be billed at the start of each month*'),
                trans("The first months' payment will be billed at the time of purchase**"),
                trans('No binding')
            ],
            'notes' => [
                trans('* That means you will be billed at the start of each month'),
                trans('** That means you will be billed for the first time once will make a purchase. This line is just to show how 2-lines sentence could look like')
            ],
        ];
    }

    public function getOnceOffSubscription()
    {
        $options = get_fields('options');
        $amount = $this->getFieldOption($options, 'once_off_subscription_amount', 0);
        $currency = 'DKK';

        return [
            "name" => OrderConstant::SUBSCRIPTION_ONE_OFF,
            "title" => $this->getFieldOption($options, 'once_off_subscription_name'),
            "total" => $amount,
            'pre_payment' => $amount,
            'monthly_payment' => 0,
            "currency" => $currency,
            "priceInfos" => [
                [
                    "title" => trans("Cost"),
                    "price" => $amount
                ]
            ],
            "btnText" => $this->getFieldOption($options, 'once_off_subscription_button_text', 'Order Now'),
            "btnLink" => $this->getFieldOption($options, 'once_off_subscription_button_link', '#'),
            'infoTexts' => [
                trans('The whole Pearl cost will be withdrawn at the time of purchase'),
                trans('No monthly payment'),
                trans('The monthly SIM card fee is included in the price'),
                trans('No binding')
            ],
            'notes' => [
                trans('* That means you will be billed at the start of each month'),
            ]
        ];
    }

    public function getAnnualSubscription()
    {
        $options = get_fields('options');
        $amount = $this->getFieldOption($options, 'annual_subscription_amount', 0);
        $currency = 'DKK';

        return [
            "name" => OrderConstant::SUBSCRIPTION_ANNUAL,
            "title" => $this->getFieldOption($options, 'annual_subscription_name'),
            "total" => $amount,
            'pre_payment' => 0,
            'monthly_payment' => $amount,
            "currency" => $currency,
            "priceInfos" => [
                [
                    "title" => trans("Prepayment"),
                    "price" => 0
                ],
                [
                    "title" => trans("Monthly Payment"),
                    "price" => $amount
                ]
            ],
            "btnText" => $this->getFieldOption($options, 'annual_subscription_button_text', 'Order Now'),
            "btnLink" => $this->getFieldOption($options, 'annual_subscription_button_link', '#'),
            'infoTexts' => [
                trans('No prepayment*'),
                trans('The monthly payment will be billed at the start of each month*'),
                trans("The first months' payment will be billed at the time of purchase**"),
                trans('12 months binding')
            ],
            'notes' => [
                trans('* That means you will be billed at the start of each month'),
            ]
        ];
    }

    private function getFieldOption($options, $field, $default = '')
    {
        return $options[$field] ?? get_field($field) ?? $default;
    }
}