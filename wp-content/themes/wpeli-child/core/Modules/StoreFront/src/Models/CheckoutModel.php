<?php

namespace EliChild\Modules\StoreFront\Models;

use Eli\Modules\Model;
use EliChild\Modules\Authentication\Services\Authentication;
use EliChild\Modules\Base\TransferAddress;
use EliChild\Modules\Customer\CustomerModule;
use EliChild\Modules\Pearl\Models\PearlModel;

class CheckoutModel extends Model
{
    /**
     * CheckoutModel constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getPageData()
    {
        $personalInfo = $this->getPersonalInfo();
        $address = $this->getAddress($personalInfo['country']['value'] ?? '');

        $pageData = [
            "metadata" => [
                "state" => Authentication::check() ? 'logged' : 'guest',
                "optionalFields" => [
                    "purchaseData",
                    "personalInfo",
                    "myCards"
                ]
            ],
            'pageData' => [
                "personalInfo" => $this->getPersonalInfo(),
                "productDesign" => $this->getProductDesign(),
                "subscriptionTypes" => $this->getSubscriptionTypes(),
                "myCards" => $this->getMyCards(),
                "countries" => $address['countries'],
                "states" => $address['states'] ?? '',
                "cities" => $address['cities'],
                "phoneCodes" => TransferAddress::getListPhoneCode(),
            ]
        ];

        return $pageData;
    }

    private function getAddress($countryId)
    {
        $countries = TransferAddress::getListCountries();
        $data = [
            'states' => [],
            'cities' => [],
            'countries' => [],
        ];

        foreach ($countries as $country) {
            $data['countries'][] = [
                'value' => $country['id'],
                'label' => $country['name'],
            ];
        }

        if (!empty($countryId)) {
//            $data['states'] = TransferAddress::getStatesByCountry($countryId);
            $data['cities'] = TransferAddress::getCitiesByCountry($countryId);
        }

        return $data;
    }

    private function getPersonalInfo()
    {
        $data = [];

        if (Authentication::check()) {
            $userDetail = Authentication::user();

            $data = [
                "name" => $userDetail['name'],
                "surname" => $userDetail['surname'],
                "address" => $userDetail['address'],
                "country" => [
                    "value" => $userDetail['country'],
                    "label" => 'Country'
                ],
                "state" => [
                    "value" => $userDetail['state'] ?? '',
                    "label" => "State"
                ],
                "zipcode" => $userDetail['zipcode'],
                "city" => [
                    "value" => $userDetail['city'],
                    "label" => "City"
                ],
                "language" => [
                    "value" => "DK",
                    "label" => "+45"
                ],
                "phone" => $userDetail['phone'],
                "email" => $userDetail['email'],
                'phoneCode' => [
                    "value" => $userDetail['phoneCode'],
                    "label" => $userDetail['phoneCode'],
                    "iso2" => $userDetail['country'],
                ]
            ];
        }

        return $data;
    }

    private function getProductDesign()
    {
        $pearlModel = new PearlModel();

        $data['collectionTypes'] = $pearlModel->getCategories();

        return $data;
    }

    private function getMyCards()
    {
        $data = [];

        if (Authentication::check()) {
            $data = (new CustomerModule())->getMyCards(Authentication::user());
        }

        return $data;
    }

    public function getSubscriptionTypes()
    {
        $optionsData = get_fields('options');
        $result = [];

        $subscriptionModel = new SubscriptionModel();
        if (!empty($optionsData['monthly_subscription_enable'])) {
            $result[] = $subscriptionModel->getMonthlySubscription();
        }

        if (!empty($optionsData['once_off_subscription_enable'])) {
            $result[] = $subscriptionModel->getOnceOffSubscription();
        }

        if (!empty($optionsData['annual_subscription_enable'])) {
            $result[] = $subscriptionModel->getAnnualSubscription();
        }

        return $result;
    }
}