<?php

namespace EliChild\Modules\StoreFront;

use Silex\Provider\TwigServiceProvider;
use Twig\TwigFunction;
use Twig_SimpleFilter;

class StoreFrontModule
{
    private $templatePath = '/core/Modules/StoreFront/views';

    public static function initialize()
    {
        return new self();
    }

    /**
     * @codeCoverageIgnore
     */
    public function __construct()
    {
        add_action('wp_head', array($this, 'addHead'));
        add_action('twig_filters', array($this, 'add_filters'));
        add_action('twig_functions', array($this, 'add_functions'));
    }

    public function addHead()
    {
        $googleAnalytics = get_field('google_analytics_code', 'option');
        echo $googleAnalytics;
    }

    public static function render($template, $data = [])
    {
        $template .= '.twig';
        $twig = (new self)->getTwig();

        return $twig->render($template, $data);
    }

    public function getTwig()
    {
        global $app;
        $themePath = get_theme_file_path() . $this->templatePath;
        //$loader = new FilesystemLoader($themePath);
        //$twig = new Environment($loader);
        if (!isset($app['twig'])) {
            $app->register(new TwigServiceProvider(), array(
                'twig.path' => [
                    $themePath => 'views',
                ],
                'twig.options' => [
                    'strict_variables' => false,
                ]
            ));
        }

        $twig = $app['twig'];
        $twig = apply_filters('twig_functions', $twig);
        $twig = apply_filters('twig_filters', $twig);

        return $twig;
    }

    public function clearCacheTwig()
    {
        $twig = $this->getTwig();
        if (method_exists($twig, 'clearCacheFiles')) {
            $twig->clearCacheFiles();
        }
        $cache = $twig->getCache();
        if ($cache) {
            self::rrmdir($twig->getCache());
            return true;
        }

        return false;
    }

    public function add_functions($twig)
    {
        /* actions and filters */
        $twig->addFunction(new TwigFunction('action', function ($context) {
            $args = func_get_args();
            array_shift($args);
            $args[] = $context;
            call_user_func_array('do_action', $args);
        }, array('needs_context' => true)));

        $twig->addFunction(new TwigFunction('function', array(&$this, 'exec_function')));
        $twig->addFunction(new TwigFunction('fn', array(&$this, 'exec_function')));
        $twig->addFunction(new TwigFunction('shortcode', 'do_shortcode'));
        $twig->addFunction(new TwigFunction('include', function ($filename) {
            if (!empty($filename)) {
                include $filename;
            }
        }));

        $twig->addFunction(new TwigFunction('body_class', 'body_class'));
        $twig->addFunction(new TwigFunction('fe_assets', array(&$this, 'fe_assets')));

        /* bloginfo and translate */
        $twig->addFunction(new TwigFunction('blog_info', 'bloginfo'));
        $twig->addFunction(new TwigFunction('__', '__'));
        $twig->addFunction(new TwigFunction('translate', 'translate'));
        $twig->addFunction(new TwigFunction('_e', '_e'));

        $twig->addFunction(new TwigFunction('log', array(&$this, 'log_function')));

        return $twig;
    }


    public function add_filters($twig)
    {
        /* actions and filters */
        $twig->addFilter(new Twig_SimpleFilter('apply_filters', function () {
            $args = func_get_args();
            $tag = current(array_splice($args, 1, 1));

            return apply_filters_ref_array($tag, $args);
        }));

        /* other filters */
        $twig->addFilter(new Twig_SimpleFilter('stripshortcodes', 'strip_shortcodes'));
        $twig->addFilter(new Twig_SimpleFilter('excerpt', 'wp_trim_words'));
        $twig->addFilter(new Twig_SimpleFilter('shortcodes', 'do_shortcode'));

        $twig->addFilter(new Twig_SimpleFilter('relative', function ($link) {
            return URLHelper::get_rel_url($link, true);
        }));

        $twig->addFilter(new Twig_SimpleFilter('truncate', function ($text, $len) {
            return TextHelper::trim_words($text, $len);
        }));

        $twig->addFilter(new Twig_SimpleFilter('trans', 'trans'));


        return $twig;
    }

    public function exec_function($functionName)
    {
        $args = func_get_args();
        array_shift($args);
        if (is_string($functionName)) {
            $functionName = trim($functionName);
        }

        return call_user_func_array($functionName, ($args));
    }

    public function path($routeName)
    {
        return $routeName;
    }

    public function fe_assets($link)
    {
        return get_stylesheet_directory_uri() . '/assets/' . $link;
    }

    public function log_function($param)
    {
        $value = "<script> console.log('%c SERVER LOG:', 'color:red;font-weight:bold;'," . json_encode($param) . ");</script>";

        print_r($value);
    }
}