<?php

namespace EliChild\Modules\StoreFront\Providers;

use EliChild\Modules\Base\Traits\LoadAndPublishDataTrait;
use EliChild\Modules\StoreFront\Events\OrderPurchaseEvent;
use EliChild\Modules\StoreFront\Events\CancelledSubscriptionEvent;
use EliChild\Modules\StoreFront\Events\ResetPasswordEvent;
use EliChild\Modules\StoreFront\Events\UpdatedCustomerDetailEvent;
use EliChild\Modules\StoreFront\Events\UpdatedPasswordCustomerEvent;
use EliChild\Modules\StoreFront\Events\UpdatedPaymentMethodEvent;
use EliChild\Modules\StoreFront\Listeners\OrderPurchaseHandler;
use EliChild\Modules\StoreFront\Listeners\CancelledSubscriptionHandler;
use EliChild\Modules\StoreFront\Listeners\ResetPasswordHandler;
use EliChild\Modules\StoreFront\Listeners\UpdatedCustomerDetailHandler;
use EliChild\Modules\StoreFront\Listeners\UpdatedPasswordCustomerHandler;
use EliChild\Modules\StoreFront\Listeners\UpdatedPaymentMethodHandler;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class StoreFrontServiceProvider implements ServiceProviderInterface, BootableProviderInterface
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Silex\Application
     */
    protected $app;

    protected $listen = [
        OrderPurchaseEvent::class => OrderPurchaseHandler::class,
        UpdatedCustomerDetailEvent::class => UpdatedCustomerDetailHandler::class,
        UpdatedPasswordCustomerEvent::class => UpdatedPasswordCustomerHandler::class,
        CancelledSubscriptionEvent::class => CancelledSubscriptionHandler::class,
        ResetPasswordEvent::class => ResetPasswordHandler::class,
        UpdatedPaymentMethodEvent::class => UpdatedPaymentMethodHandler::class,
    ];

    /**
     * Bootstrap the application events.
     * @param Application $app
     */
    public function boot(Application $app)
    {
        $this->app = $app;

        $this->setIsInConsole($this->runningInConsole())
            ->setNamespace('modules.StoreFront')
            ->loadRoutes(['web', 'auth', 'checkout', 'singlePage', 'api'])
            ->loadEvents()
            ->loadMigrations();
    }

    /**
     * Register the service provider.
     *
     * @param Container $container
     * @return void
     */
    public function register(Container $container)
    {

    }
}
