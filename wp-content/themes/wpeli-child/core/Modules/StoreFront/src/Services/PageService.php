<?php

namespace EliChild\Modules\StoreFront\Services;

use Eli\Deployment\Deploy;

class PageService
{

    /**
     * Get link page by Id page
     * @param $id
     * @return string
     */
    public static function getLinkPageById($id)
    {
        $link = '#';
        if (!empty($id)) {
            $link = get_page_link($id);
        }
        return $link;
    }

    /**
     * Get link image from Id image
     * @param $id
     * @return mixed
     */
    public static function getLinkImageFromId($id)
    {
        if(!empty($id['url'])) {
            return $id['url'];
        }
        if(!empty(wp_get_attachment_image_src($id))){
            return wp_get_attachment_image_src($id)[0];
        }
        return $id;
    }

    /**
     * Get Info page in deployment by key
     * @param $key
     * @return array
     */
    public static function getInfoPageByKey($key)
    {
        if (!file_exists(locate_template(Deploy::BASE_PATH . '/data/page.json'))) {
            return [];
        }
        $pages = json_decode(@file_get_contents(locate_template(Deploy::BASE_PATH . '/data/page.json')), true);
        $result = [];
        foreach ($pages as $page) {
            if (!empty($page['meta_data']['post_key']) && $page['meta_data']['post_key'] == $key) {
                $result = $page;
                break;
            }
        }
        return $result;
    }

    /**
     * Get alias page by key in deployment
     * @param $key
     * @return string
     */
    public static function getAliasPageByKey($key)
    {
        $postTitle = self::getInfoPageByKey($key)['post_data']['post_title'] ?? '';
        return sanitize_title($postTitle);
    }

    public static function getTextArea($string)
    {
        if ($string != strip_tags($string)) {
            return $string;
        }
        return nl2br($string);
    }

    public static function translatePost($id)
    {
        $post = get_post($id);
        if (!empty($post)){
            $post->post_title = isset($post->post_title) && !empty($post->post_title) ? qtranxf_use( qtranxf_getLanguage(), $post->post_title, false) : "";
            $post->post_content = isset($post->post_content) && !empty($post->post_content) ? qtranxf_use( qtranxf_getLanguage(), $post->post_content, false) : "";
        } else {
            $post = null;
        }
        return $post;
    }
}