<?php

namespace EliChild\Modules\StoreFront\Services;

use EliChild\Modules\Authentication\Services\Authentication;
use EliChild\Modules\Base\TransferAddress;
use EliChild\Modules\Customer\Models\CustomerModel;
use EliChild\Modules\Order\Helpers\OrderConstant;
use EliChild\Modules\Order\Models\OrderModel;
use EliChild\Modules\Payment\GateWay\Paypal\PaypalGateWay;
use EliChild\Modules\Payment\Supports\Payment;
use EliChild\Modules\Pearl\Models\PearlModel;
use EliChild\Modules\StoreFront\Models\SubscriptionModel;
use EliValidForm;

class CheckoutService
{
    public static function checkValidShippingData($request)
    {
        $response = [
            'error' => false,
            'message' => '',
            'data' => [],
        ];

        $validForm = new EliValidForm();
        $required = [
            'email' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'country' => 'required',
//            'state' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
        ];

        $validation = $validForm->validate($request, $required);

        if (!empty($validation)) {
            $response['error'] = true;
            $response['message'] = trans('Some fields on your form are invalid. Please check your information!');
            $response['data'] = $validation;
        } else {
            $customerModel = new CustomerModel();
            $userInput = [
                'email' => $request->get('email', ''),
                'name' => $request->get('name', ''),
                'surname' => $request->get('surname', ''),
                'address' => $request->get('address', ''),
                'country' => $request->get('country', ''),
                'state' => $request->get('state', ''),
                'city' => $request->get('city', ''),
                'zipcode' => $request->get('zipcode', ''),
                'phoneCode' => $request->get('phoneCode', ''),
                'phone' => $request->get('phone', ''),
                'isReceiveNewsletter' => $request->get('isReceiveNewsletter', ''),
            ];

            if (Authentication::check()) {
                $userLogin = Authentication::user();

                //update personal info in webshop and lighthouse
                $customerData = $customerModel->updateCustomer($userLogin['sarita_id'], $userInput, $userLogin['id']);
                Authentication::reGetUser();

                $response['data']['user_id'] = $customerData['user_id'];
            } else {
                $customerModel = new CustomerModel();

                //store customer trash
                $customerData = $customerModel->updateDraftCustomer($userInput);

                if ($customerData['error']) {
                    $response = $customerData;
                } else {
                    $response['data']['user_id'] = $customerData['user_id'];
                }
            }

            if (isset($response['data']['user_id'])) {
                Authentication::getGuestId($response['data']['user_id']);
            }
        }

        return $response;
    }

    public static function checkValidProductDesign($productId)
    {
        $error = false;
        $message = '';

        if (empty($productId)) {
            $error = true;
            $message = trans('Product design invalid!');
        }

        return [
            'error' => $error,
            'message' => $message
        ];
    }

    public static function checkValidSubscription($subscriptionType)
    {
        $error = false;
        $message = '';

        if (empty($subscriptionType)) {
            $error = true;
            $message = trans('Subscription type invalid!');
        }

        return [
            'error' => $error,
            'message' => $message
        ];
    }

    public static function getDetailOrderInfo($args)
    {
        $subscriptionType = $args['subscription_type'];
        $pearlId = $args['product_id'];
        $guestId = Authentication::getGuestId();

        $subscriptionModel = new SubscriptionModel($subscriptionType);
        $pearlModel = new PearlModel($pearlId);
        $customerModel = new CustomerModel($guestId);
        $countryDetail = TransferAddress::getCountryDetail($customerModel->getCountry());

        return [
            'purchaseData' => [
                'deliveryFee' => OrderConstant::DELIVERY_FEE,
                'shippingFee' => OrderConstant::SHIPPING_FEE,
                'shippingLabel' => trans('Free Shipping'),
                'monthlyPrice' => $subscriptionModel->getMonthlyPayment(),
                'prepaymentPrice' => $subscriptionModel->getPrePayment(),
                'productImage' => $pearlModel->getImageUrl(),
                'productName' => $pearlModel->getName(),
                'productCode' => $pearlModel->getDesignCode(),
                'productId' => $pearlId,
                'subscription' => $subscriptionModel->getTitle(),
                'subscriptionType' => $subscriptionModel->getName(),
                'currency' => $subscriptionModel->getCurrency(),
                'total' => $subscriptionModel->getTotal(),
            ],
            'shippingData' => [
                'address' => $customerModel->getAddress(),
                'city' => $customerModel->getCity(),
                'state' => $customerModel->getState(),
                'zipcode' => $customerModel->getZipcode(),
                'country' => $countryDetail['iso2'],
                'country_iso3' => $countryDetail['iso3'],
                'name' => $customerModel->getName(),
                'surname' => $customerModel->getSurname(),
                'email' => $customerModel->getEmail(),
                'phoneCode' => $customerModel->getPhoneCode(),
                'phone' => $customerModel->getPhone(),
            ]
        ];
    }

    public static function paymentProcess($request)
    {
        $paymentMethod = $request->get('paymentMethod');
        $orderId = self::getOrderCurrentWaiting();
        $orderModel = new OrderModel();
        $orderCheckoutInfo = self::getDetailOrderInfo([
            'subscription_type' => $request->get('subscriptionType'),
            'product_id' => $request->get('productDesignId'),
        ]);
        $guestId = Authentication::getGuestId();

        $dataOrderInput = [
            'customer' => [
                'sarita_id' => '',
                'id' => $guestId,
                'name' => $orderCheckoutInfo['shippingData']['name'],
                'surname' => $orderCheckoutInfo['shippingData']['surname'],
                'email' => $orderCheckoutInfo['shippingData']['email'],
                'phoneCode' => $orderCheckoutInfo['shippingData']['phoneCode'],
                'phone' => $orderCheckoutInfo['shippingData']['phone'],
            ],
            'shipping' => [
                'address' => $orderCheckoutInfo['shippingData']['address'],
                'city' => $orderCheckoutInfo['shippingData']['city'],
                'state' => $orderCheckoutInfo['shippingData']['state'] ?? '',
                'zipcode' => $orderCheckoutInfo['shippingData']['zipcode'],
                'country' => $orderCheckoutInfo['shippingData']['country'],
                'country_code' => $orderCheckoutInfo['shippingData']['country'],
                'country_iso3' => $orderCheckoutInfo['shippingData']['country_iso3'],
            ],
            'pre_payment' => $orderCheckoutInfo['purchaseData']['prepaymentPrice'] ?? 0,
            'monthly_payment' => $orderCheckoutInfo['purchaseData']['monthlyPrice'] ?? 0,
            'first_payment_amount' => ($orderCheckoutInfo['purchaseData']['monthlyPrice'] ?? 0) + ($orderCheckoutInfo['purchaseData']['prepaymentPrice'] ?? 0),
            'currency' => $orderCheckoutInfo['purchaseData']['currency'],
            'payment_method' => $paymentMethod,
            'pearl_id' => $orderCheckoutInfo['purchaseData']['productId'],
            'pearl_name' => $orderCheckoutInfo['purchaseData']['productName'],
            'pearl_code' => $orderCheckoutInfo['purchaseData']['productCode'],
            'subscription_type' => $orderCheckoutInfo['purchaseData']['subscriptionType'],
            'subscription_description' => $orderCheckoutInfo['purchaseData']['subscription'],
        ];

        $dataOrderInput['billing-address'] = $dataOrderInput['shipping'];

        $cardInfo = $request->get('cardInfo', []);
        $useShippingAddress = $cardInfo['useShippingAddress'] ?? false;

        // check use shipping address
        if (!empty($cardInfo['billingCountry']) && !$useShippingAddress) {
            $countryDetail = TransferAddress::getCountryDetail($cardInfo['billingCountry']);
            $dataOrderInput['billing-address'] = [
                'address' => $cardInfo['billingAddress'],
                'city' => $cardInfo['billingCity'],
                'state' => $cardInfo['billingState'] ?? '',
                'zipcode' => $cardInfo['billingZipcode'],
                'country' => $countryDetail['iso2'],
                'country_iso3' => $countryDetail['iso3'],
                'country_code' => $countryDetail['iso2']
            ];
        }

        if (Authentication::check()) {
            $customerModel = new CustomerModel($guestId);
            $dataOrderInput['customer']['sarita_id'] = $customerModel->getSaritaId();
        }

        if (!$orderId) {
            //create order into web-shop
            $orderId = $orderModel->createOrder($dataOrderInput);

            //save session order
            self::orderIdCurrently($orderId);

            //update order status to waiting to payment
            $orderModel->awaitingToPaymentOrder($orderId);
        } else {
            //update order
            $orderModel->updateOrder($orderId, $dataOrderInput);
        }

        //process payment
        $dataOrderInput['order_id'] = $orderId;
        switch ($paymentMethod) {
            case 'paypal':
                return self::processPaymentForPaypal($orderId, $dataOrderInput);
                break;

            case 'credit-debit':
                //for save card
                $saveCardId = $request->get('savedCardId');

                if ($saveCardId) {
                    $dataOrderInput['card-info']['card-id'] = $saveCardId;
                    $addressSaved = self::getAddressByCardSaved($saveCardId);
                    if ($addressSaved) {
                        $countryDetail = TransferAddress::getCountryDetail($addressSaved['country']);
                        $dataOrderInput['billing-address'] = [
                            'address' => $addressSaved['address'],
                            'city' => $addressSaved['city'],
                            'state' => $addressSaved['state'] ?? '',
                            'zipcode' => $addressSaved['zipcode'],
                            'country' => $countryDetail['iso2'],
                            'country_iso3' => $countryDetail['iso3'],
                            'country_code' => $countryDetail['iso2']
                        ];
                        $orderModel->updateOrder($orderId, $dataOrderInput);
                    }
                } else {
                    $card = $request->get('cardInfo');
                    $dataOrderInput['card-info'] = [
                        'number' => $card['cardNumber'] ?? '',
                        'expiration' => ($card['expiryYear'] ?? '') . ($card['expiryMonth'] ?? ''),
                        'cvd' => $card['cvv'] ?? '',
                        'name' => $card['cardName'] ?? '',
                    ];
                }

                if (empty($card['useShippingAddress'])) {
                    if (!empty($card['billingCountry'])) {
                        $countryDetail = TransferAddress::getCountryDetail($card['billingCountry']);
                        $dataOrderInput['billing-address'] = [
                            'address' => $card['billingAddress'] ?? $orderCheckoutInfo['shippingData']['address'],
                            'city' => $card['billingCity'] ?? $orderCheckoutInfo['shippingData']['city'],
                            'state' => $card['billingState'] ?? $orderCheckoutInfo['shippingData']['state'] ?? '',
                            'zipcode' => $card['billingZipcode'] ?? $orderCheckoutInfo['shippingData']['zipcode'],
                            'country_code' => $countryDetail['iso3'] ?? $orderCheckoutInfo['shippingData']['country_iso3'],
                        ];
                    }
                }

                return self::processPaymentForQuickpay($orderId, $dataOrderInput);
                break;

            default:
                return [
                    'error' => true,
                    'message' => trans('Payment gateway invalid')
                ];
        }
    }

    private static function getAddressByCardSaved($cardId)
    {
        $customer = Authentication::user();
        $addresses = json_decode($customer['billing_addresses'], true);
        if (empty($addresses[$cardId])) {
            return false;
        }
        return $addresses[$cardId];
    }

    private static function processPaymentForQuickpay($orderId, $dataOrderInput)
    {
        $gateWayPayment = Payment::create('credit_card');
        $orderSubscriptionQP = (new OrderModel())->getSubscriptionQuickPay($orderId);

        if (empty($orderSubscriptionQP)) {
            //Create subscription quick-pay
            $subscription = $gateWayPayment->createSubscription($orderId, [
                'currency' => $dataOrderInput['currency'],
                'description' => $dataOrderInput['subscription_type'] ?? 'Subscription',
                'billing-address' => $dataOrderInput['billing-address'],
            ]);

            if ($subscription['error']) {
                return $subscription;
            }

            update_post_meta($orderId, 'subscription_quickpay', json_encode($subscription['data']));
            $dataOrderInput['subscriptionQPId'] = $subscription['data']['id'];
        } else {
            $dataOrderInput['subscriptionQPId'] = $orderSubscriptionQP;
        }

        $responsePaymentProcess = $gateWayPayment->process($dataOrderInput);

        if (!$responsePaymentProcess['error']) {
            //update status paid for order and request subscription to lighthouse
            (new OrderModel())->paidOrder($orderId, $responsePaymentProcess['data']);

            return [
                'error' => false,
                'data' => [
                    'link_redirect' => site_url("/checkout/success?order-id=" . $orderId)
                ]
            ];
        } else {
            $errorData = $responsePaymentProcess['data'] ?? null;
            $message = $responsePaymentProcess['message'] ?? '';
            if (!empty($errorData)) {
                $message .= '. ' . self::parseErrorPayment($errorData);
            }

            return [
                'error' => true,
                'message' => $message,
                'data' => $errorData,
            ];
        }
    }

    private static function processPaymentForPaypal($orderId, $dataOrderInput)
    {
        $gateWayPayment = Payment::create('paypal');
        $dataOrderInput['success-link'] = site_url("/checkout/execute-agreement?order_id={$orderId}&success=true");
        $dataOrderInput['cancel-link'] = site_url("/checkout/execute-agreement?order_id={$orderId}&success=false");

        $responsePaymentProcess = $gateWayPayment->process($dataOrderInput);

        if (!$responsePaymentProcess['error']) {
            return [
                'error' => false,
                'data' => [
                    'link_redirect' => $responsePaymentProcess['approval_url']
                ]
            ];
        } else {
            return [
                'error' => true,
                'message' => $responsePaymentProcess['message'] ?? '',
                'data' => $responsePaymentProcess['data'] ?? null,
            ];
        }
    }

    public static function paypalExecuteAgreementProcess($args)
    {
        $orderId = $args['order_id'] ?? '';
        $paypalGateWay = new PaypalGateWay();

        //execute paypal payment
        $responseExecute = $paypalGateWay->execute([
            'status' => $args['status'] ?? false,
            'token' => $args['token'] ?? '',
            'order_id' => $orderId,
            'paymentId' => $args['paymentId'] ?? null,
            'payerID' => $args['payerID'] ?? null,
        ]);

        if (!$responseExecute['error']) {
            //update status paid for order and request subscription to lighthouse
            (new OrderModel())->paidOrder($orderId, $responseExecute['data']);

            return [
                'error' => false,
            ];
        }

        return [
            'error' => true,
            'message' => $responseExecute['message'] ?? '',
            'data' => $responsePaymentProcess['data'] ?? null,
        ];
    }

    public static function orderIdCurrently($orderId = null)
    {
        if (!empty($orderId)) {
            $_SESSION[SESSION_ORDER_ID] = $orderId;
        }

        return $_SESSION[SESSION_ORDER_ID] ?? null;
    }

    public static function clearSession()
    {
        if (isset($_SESSION[SESSION_ORDER_ID])) {
            unset($_SESSION[SESSION_ORDER_ID]);
        }
    }

    public static function parseErrorPayment($errorData)
    {
        $output = "";
        foreach ($errorData as $key => $errorValue) {
            $output .= $key . ' ' . ($errorValue[0] ?? '') . ' . ';
        }

        return $output;
    }

    private static function getOrderCurrentWaiting()
    {
        $orderId = self::orderIdCurrently();
        if (!empty($orderId)) {
            $order = new OrderModel($orderId);

            if ($order->getStatus() == OrderConstant::STATUS_WAITING) {
                return $orderId;
            }
            self::clearSession();
        }
        return null;
    }
}