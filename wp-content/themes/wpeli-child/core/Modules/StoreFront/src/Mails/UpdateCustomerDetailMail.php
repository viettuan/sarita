<?php

namespace EliChild\Modules\StoreFront\Mails;

use EliChild\Email\Mandrill;

class UpdateCustomerDetailMail extends Mandrill
{
    public function sendMail($args = [])
    {
        if(!empty($args['email'])) {
            $data = [
                'template_name' => 'updated-customer-details',
                'subject' => 'Sarita - Updated customer details',
                'email' => $args['email'],
                'name' => $args['name'] ?? '',
                'vars' => [
                    [
                        'name' => 'subject',
                        'content' => 'Sarita - Updated customer details',
                    ]
                ]
            ];

            $this->send($data);
        }
    }
}