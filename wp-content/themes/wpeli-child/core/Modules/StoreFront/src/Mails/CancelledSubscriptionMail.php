<?php

namespace EliChild\Modules\StoreFront\Mails;

use EliChild\Email\Mandrill;

class CancelledSubscriptionMail extends Mandrill
{
    public function sendMail($args = [])
    {
        if (!empty($args['email'])) {
            $data = [
                'template_name' => 'cancelled-subscription',
                'subject' => 'Sarita - Cancelled subscription',
                'email' => $args['email'],
                'name' => $args['name'] ?? '',
                'vars' => [
                    [
                        'name' => 'subject',
                        'content' => 'Sarita - Cancelled subscription',
                    ], [
                        'name' => 'pearl_number',
                        'content' => $args['pearl_number'] ?? '',
                    ], [
                        'name' => 'cancelled_date',
                        'content' => $args['cancelled_date'] ?? '',
                    ], [
                        'name' => 'validity_date',
                        'content' => $args['validity_date'] ?? '',
                    ]
                ]
            ];

            $this->send($data);
        }
    }
}