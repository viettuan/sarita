<?php

namespace EliChild\Modules\StoreFront\Mails;

use EliChild\Email\Mandrill;

class PurchaseEmailWthPasswordMail extends Mandrill
{
    public function sendMail($args = [])
    {
        if (!empty($args['email'])) {
            $data = [
                'template_name' => 'purchase-email-with-password',
                'subject' => 'Sarita - Purchase Email with password',
                'email' => $args['email'],
                'name' => $args['name'] ?? '',
                'vars' => [
                    [
                        'name' => 'subject',
                        'content' => 'Sarita - Purchase Email with password',
                    ], [
                        'name' => 'customer_name',
                        'content' => $args['name'] ?? '',
                    ], [
                        'name' => 'password',
                        'content' => $args['password'] ?? '',
                    ], [
                        'name' => 'order_date',
                        'content' => $args['order_date'] ?? '',
                    ], [
                        'name' => 'customer_address',
                        'content' => $args['address'] ?? '',
                    ], [
                        'name' => 'order_pearl',
                        'content' => $args['order_pearl'] ?? '',
                    ], [
                        'name' => 'subscription_type',
                        'content' => $args['subscription_type'] ?? '',
                    ], [
                        'name' => 'payment_method',
                        'content' => $args['payment_method'] ?? '',
                    ], [
                        'name' => 'order_id',
                        'content' => $args['order_id'] ?? '',
                    ], [
                        'name' => 'order_price',
                        'content' => $args['order_price'] ?? '',
                    ]
                ]
            ];

            $this->send($data);
        }
    }
}