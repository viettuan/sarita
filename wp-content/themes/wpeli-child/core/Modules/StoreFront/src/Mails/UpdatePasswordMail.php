<?php

namespace EliChild\Modules\StoreFront\Mails;

use EliChild\Email\Mandrill;

class UpdatePasswordMail extends Mandrill
{
    public function sendMail($args = [])
    {
        if(!empty($args['email'])) {
            $data = [
                'template_name' => 'updated-password',
                'subject' => 'Sarita - Updated password',
                'email' => $args['email'],
                'name' => $args['name'] ?? '',
                'vars' => [
                    [
                        'name' => 'subject',
                        'content' => 'Sarita - Updated password',
                    ],
                    [
                        'name' => 'new_password',
                        'content' => $args['new_password'] ?? '',
                    ]
                ]
            ];

            $this->send($data);
        }
    }
}