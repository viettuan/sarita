<?php

namespace EliChild\Modules\StoreFront\Mails;

use EliChild\Email\Mandrill;

class UpdatePaymentMethodInfoMail extends Mandrill
{
    public function sendMail($args = [])
    {
        if(!empty($args['email'])) {
            $data = [
                'template_name' => 'updated-payment-method-information',
                'subject' => 'Sarita - Updated payment method information',
                'email' => $args['email'],
                'name' => $args['name'] ?? '',
                'vars' => [
                    [
                        'name' => 'subject',
                        'content' => 'Sarita - Updated payment method information',
                    ]
                ]
            ];

            $this->send($data);
        }
    }
}