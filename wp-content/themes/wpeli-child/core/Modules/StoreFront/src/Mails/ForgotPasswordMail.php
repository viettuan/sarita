<?php

namespace EliChild\Modules\StoreFront\Mails;

use EliChild\Email\Mandrill;

class ForgotPasswordMail extends Mandrill
{
    public function sendMail($args = [])
    {
        if(!empty($args['email'])) {
            $data = [
                'template_name' => 'forgot-password',
                'subject' => 'Sarita - Forgot password',
                'email' => $args['email'],
                'name' => $args['name'] ?? '',
                'vars' => [
                    [
                        'name' => 'subject',
                        'content' => 'Sarita - Forgot password',
                    ],
                    [
                        'name' => 'new_password',
                        'content' => $args['new_password'] ?? '',
                    ]
                ]
            ];

            $this->send($data);
        }
    }
}