<?php

namespace EliChild\Modules\StoreFront\Controllers;

use EliChild\Modules\Authentication\Services\Authentication;
use EliChild\Modules\Base\Controllers\BaseController;
use EliValidForm;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use EliChild\Modules\Customer\Models\CustomerModel;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class AuthController extends BaseController
{
    public function login(Request $request)
    {
        $validForm = new EliValidForm();

        $required = [
            'username' => 'required',
            'password' => 'required'
        ];

        $valid = $validForm->validate($request, $required);

        if (!empty($valid)) {
            return response([
                'error' => false,
                'data' => $valid
            ]);
        }

        $username = $request->get('username', '');
        $password = $request->get('password', '');

        $authLogin = Authentication::login($username, $password);

        if (!$authLogin) {
            return response([
                'error' => true,
                'message' => trans("User not found")
            ]);
        }

        return response([
            'error' => false,
            'message' => trans('Login successfully!'),
        ]);
    }

    public function logout(Application $app)
    {
        Authentication::logout();

        return redirect($app["url_generator"]->generate('home'));
    }

    public function forgotPassword(Request $request)
    {
        $email = $request->get('email', '');
        $response = Authentication::forgetPassword($email);

        if (!empty($response)) {
            return response([
                'error' => false,
                'message' => trans('An email reset password send to your email!')
            ]);
        }

        return response([
            'error' => true,
            'message' => trans('Email not exist!')
        ]);
    }

//    public function getPageSendMailResetPassword()
//    {
//        return view('reset-password');
//    }
//
//    /***
//     * Get link to send mail reset password
//     * @param Request $request
//     * @param Application $app
//     */
//    public function sendMailResetPassword(Request $request, Application $app)
//    {
//        $customerModel = new CustomerModel();
//        $email = $request->request->get('email', '');
//        $user_exist = $customerModel->getFirst(['email' => $email]);
//
//        if ($user_exist) {
//            $time = time();
//            $_SESSION['time-reset-password'] = $time;
//            $_SESSION['email-reset-password'] = $email;
//            $url = get_site_url() . '/call-back-reset-password?time=' . $time . '&' . 'email=' . $email;
//
//            // send mail
//            $subject = 'Link reset password in Sarita';
//            $body = renderView('email/reset-password', ['link' => $url]);
//            $header = array('Content-Type: text/html; charset=UTF-8');
//            $send = wp_mail($email, $subject, $body, $header);
//        }
//
//        return redirect($app["url_generator"]->generate('customer.resetPass.pageSendMail'));
//    }
//
//    /**
//     * Handle callback to reset password
//     * @param Request $request
//     * @param Application $app
//     */
//    public function handleCallbackResetPassword(Request $request, Application $app)
//    {
//        $email = $request->query->get('email', null);
//        $time = $request->query->get('time', null);
//        if ($email && $time && isset($_SESSION['time-reset-password']) && isset($_SESSION['email-reset-password'])) {
//            if ($email == $_SESSION['email-reset-password'] && $time == $_SESSION['time-reset-password']) {
//                $_SESSION['status-reset-password'] = true;
//
//                return redirect($app["url_generator"]->generate('customer.resetPass.pageReset'));
//            }
//        }
//
//        return redirect($app["url_generator"]->generate('customer.resetPass.pageSendMail'));
//    }
//
//    /**
//     * get to page reset password
//     * @param Application $app
//     * @return string
//     */
//    public function getPageResetPassword(Application $app)
//    {
//        if (isset($_SESSION['status-reset-password'])) {
//            return view('change-password');
//        }
//
//        return redirect($app["url_generator"]->generate('customer.resetPass.pageSendMail'));
//    }
//
//    /**
//     * Post change password
//     * @param Request $request
//     * @param Application $app
//     */
//    public function postChangePasswordReset(Request $request, Application $app)
//    {
//        $customerModule = new CustomerModule();
//        $email = $_SESSION['email-reset-password'] ? $_SESSION['email-reset-password'] : null;
//        $password = $request->request->get('password', null);
//        $password_confirmation = $request->request->get('password_confirmation', null);
//
//        if (!empty($password) && !empty($password_confirmation) && !empty($_SESSION['status-reset-password']) && !empty($email)) {
//            if ($password == $password_confirmation) {
//                $update = $customerModule->update(
//                    [
//                        'password' => wp_hash_password($password)
//                    ],
//                    [
//                        'email' => $email
//                    ]
//                );
//                $customerModule->addLogin($email);
//                unset($_SESSION['status-reset-password']);
//                unset($_SESSION['time-reset-password']);
//                unset($_SESSION['email-reset-password']);
//
//                return redirect($app["url_generator"]->generate('homepage'));
//            }
//        }
//    }
//
//    /**
//     * Handle call back login facebook
//     * @param Application $app
//     * @throws FacebookSDKException
//     */
//    public function handleCallBackLoginFacebook(Application $app)
//    {
//        $fb = new Facebook(array(
//            'app_id' => FB_APP_ID,
//            'app_secret' => FB_APP_SECRET_LOGIN,
//            'default_graph_version' => 'v4.0',
//        ));
//
//        $helper = $fb->getRedirectLoginHelper();
//        try {
//            if (isset($_SESSION['facebook_access_token'])) {
//                $accessToken = $_SESSION['facebook_access_token'];
//            } else {
//                $accessToken = $helper->getAccessToken();
//            }
//        } catch (FacebookResponseException $e) {
//            echo 'Graph returned an error: ' . $e->getMessage();
//            exit;
//        } catch (FacebookSDKException $e) {
//            echo 'Facebook SDK returned an error: ' . $e->getMessage();
//            exit;
//        }
//
//        if (!isset($accessToken)) {
//            if ($helper->getError()) {
//                header('HTTP/1.0 401 Unauthorized');
//                echo "Error: " . $helper->getError() . "\n";
//                echo "Error Code: " . $helper->getErrorCode() . "\n";
//                echo "Error Reason: " . $helper->getErrorReason() . "\n";
//                echo "Error Description: " . $helper->getErrorDescription() . "\n";
//            } else {
//                header('HTTP/1.0 400 Bad Request');
//                echo 'Bad request';
//            }
//            exit;
//        }
//
//        $response = $fb->get('/me?fields=id,name,email,picture, first_name, last_name', $accessToken);
//        $user = $response->getGraphUser();
//        $this->checkLoginFacebook($user, $app);
//    }
//
//    /**
//     * Check login in facebook
//     * @param $user
//     * @param $app
//     */
//    private function checkLoginFacebook($user, $app)
//    {
//        $customerModel = new CustomerModel();
//        $customerModule = new CustomerModule();
//        $provide_id = $user->getId() ?? null;
//        $email = $user->getEmail() ?? null;
//        if ($user) {
//            $customer = $customerModel->getFirst(['provide_id' => $provide_id]);
//            if (empty($customer)) {
//                $data = [
//                    'email' => $email,
//                    'provide_id' => $provide_id,
//                    'avatar' => $user->getPicture()['url'] ?? null,
//                    'first_name' => $user->getFirstName() ?? '',
//                    'last_name' => $user->getLastName() ?? '',
//                    'username' => $customerModule->generateUsername($email),
//                    'status' => STATUS_CUSTOMER_ACTIVE
//                ];
//                $data = addTimestamps($data);
//                $customerModule->updateOrCreateCustomer($data, ['email' => $email]);
//            }
//        }
//
//        $email = $email ?? $provide_id; // if facebook account haven't email
//        $customerModule->addLogin($email);
//
//        return redirect($app["url_generator"]->generate('homepage'));
//    }
}