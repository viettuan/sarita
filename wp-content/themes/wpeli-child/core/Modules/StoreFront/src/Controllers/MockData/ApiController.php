<?php

namespace EliChild\Modules\StoreFront\Controllers\MockData;

use EliChild\Modules\Base\Controllers\BaseController;

class ApiController extends BaseController
{
    public function response($response, $statusCode = 200)
    {
        return wp_send_json([
            'data' => $response['data'] ?? null,
        ], $statusCode);
    }

    public function createUser()
    {
        return $this->response([
            'data' => [
                'sarita_user_id' => $this->generateUuid(),
                'generated_password' => $this->generateRandomString(10)
            ]
        ]);
    }

    public function getUserDetail()
    {
        return $this->response([
            'data' => [
                'sarita_user_id' => $this->generateUuid(),
                'generated_password' => $this->generateRandomString(10)
            ]
        ]);
    }

    public function updateUser()
    {
        return $this->response([
            'data' => null
        ]);
    }

    public function deleteUser()
    {
        return $this->response([
            'data' => null
        ]);
    }

    public function changePassword()
    {
        return $this->response([
            'data' => null
        ]);
    }

    public function createSubscription()
    {
        return $this->response([
            'data' => $this->generateUuid(),
        ]);
    }

    public function getSubscriptions()
    {
        return $this->response([
            'data' => null
        ]);
    }

    public function cancelSubscription()
    {
        return $this->response([
            'data' => null
        ]);
    }

    private function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function generateUuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}