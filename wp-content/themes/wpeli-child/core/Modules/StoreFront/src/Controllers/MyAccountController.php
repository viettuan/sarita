<?php

namespace EliChild\Modules\StoreFront\Controllers;

use EliChild\Modules\Base\Controllers\BaseController;
use EliChild\Modules\Customer\CustomerModule;
use Symfony\Component\HttpFoundation\Request;

class MyAccountController extends BaseController
{
    private $customerModule;

    public function __construct()
    {
        parent::__construct();

        $this->customerModule = new CustomerModule();
    }

    public function index()
    {
        $pageData = $this->customerModule->getMyAccount();

        return renderView("@views/pages/my-account/my-account.model", ['pageData' => $pageData]);
    }

    public function updatePersonalInfo(Request $request)
    {
        $response = $this->customerModule->updatePersonalInfo($request->request);

        return response($response);
    }

    public function deleteCreditCard(Request $request)
    {
        $response = $this->customerModule->deleteCreditCard($request->request->get('cardId'));

        return response($response);
    }

    public function savePaymentData(Request $request)
    {
        $response = $this->customerModule->savePaymentData($request->request->all());

        return response($response);
    }

    public function registerSubscribe()
    {
        return response([
            'error' => false,
            'message' => trans('Subscribe successfully!')
        ]);
    }

    public function ajaxLoadAddress(Request $request)
    {
        $response = $this->customerModule->ajaxLoadAddress($request);

        return response([
            'data' => $response
        ]);
    }

    public function updatePassword(Request $request)
    {
        $response = $this->customerModule->updatePassword($request->request);

        return response($response);
    }

    public function cancelSubscription(Request $request)
    {
        $response = $this->customerModule->cancelSubscription($request->request);

        return response($response);
    }

    public function ajaxLoadPhoneCodes(Request $request)
    {
        $name = $request->get('name');
        $phoneCodesAvailable = get_field($name, 'option');
        return response([
            'error' => false,
            'data' => $phoneCodesAvailable
        ]);
    }
}