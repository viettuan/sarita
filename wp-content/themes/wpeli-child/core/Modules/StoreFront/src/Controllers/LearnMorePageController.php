<?php

namespace EliChild\Modules\StoreFront\Controllers;

use EliChild\Modules\Base\Controllers\BaseController;
use EliChild\Modules\StoreFront\Models\LearnMoreModel;

class LearnMorePageController extends BaseController
{
    public function index()
    {
        $homeModal = new LearnMoreModel();
        $pageData = $homeModal->getPageData();

        return renderView("@views/pages/learn-more/learn-more.model", ['pageData' => $pageData]);
    }
}