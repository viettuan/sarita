<?php

namespace EliChild\Modules\StoreFront\Controllers;

use EliChild\Modules\Base\Controllers\BaseController;

class AdminController extends BaseController
{
    public function getFileTranslate($lang)
    {
        $langPathMirror = [
            'en' => 'en_US',
            'da' => 'da_DK',
        ];
        $nameFile = ($langPathMirror[$lang] ?? '') . '.json';
        $path = wp_upload_dir()['basedir'] . '/lang/' . $nameFile;
        if (file_exists($path)) {
            $file = file_get_contents($path);
            $json = json_decode($file, true);
            ksort($json);
            header('Content-type: application/json');
            $json = json_encode($json, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            echo($json);
            die();

        }
        echo 'File json is not exist';
        die();
    }
}