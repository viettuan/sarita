<?php

namespace EliChild\Modules\StoreFront\Controllers;

use EliChild\Modules\Base\Controllers\BaseController;
use EliChild\Modules\Base\Supports\EliMailChimpSubscribe;
use EliChild\Modules\StoreFront\Models\HomeModel;
use Symfony\Component\HttpFoundation\Request;

class HomePageController extends BaseController
{
    public function index()
    {
        $homeModal = new HomeModel();
        $pageData = $homeModal->getPageData();

        return renderView("@views/pages/home/home.model", ['pageData' => $pageData]);
    }

    public function registerSubscribe(Request $request) {
        $email = $request->get('email');
        $subscribeResponse = (new EliMailChimpSubscribe())->subscribe($email);

        if($subscribeResponse['error']) {
            return response([
                'error' => true,
                'message' => $subscribeResponse['message']
            ]);
        }

        return response([
            'error' => false,
            'message' => trans('You have successfully subscribed to the newsletter')
        ]);
    }
}