<?php

namespace EliChild\Modules\StoreFront\Controllers;

use EliChild\Modules\Base\Controllers\BaseController;
use Silex\Application;

class SinglePageController extends BaseController
{
    public function index($slug, Application $app)
    {
        $page = get_page_by_path($slug);
        if (empty($page)) {
            return redirect($app["url_generator"]->generate('home'));
        }

        $pageData = [
            'pageTitle' => apply_filters('translate_text', $page->post_title),
            'pageContent' => apply_filters('translate_text', $page->post_content)
        ];

        return renderView("@views/pages/single/single.model", ['pageData' => $pageData]);
    }
}