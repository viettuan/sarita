<?php

namespace EliChild\Modules\StoreFront\Controllers;

use EliChild\Modules\Authentication\Services\Authentication;
use EliChild\Modules\Base\TransferAddress;
use EliChild\Modules\Pearl\Models\PearlModel;
use EliChild\Modules\Base\Controllers\BaseController;
use EliChild\Modules\StoreFront\Models\CheckoutModel;
use EliChild\Modules\StoreFront\Services\CheckoutService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class CheckoutController extends BaseController
{
    public function index()
    {
        $checkoutModal = new CheckoutModel();
        $data = $checkoutModal->getPageData();

        return renderView("@views/pages/checkout/checkout", $data);
    }

    //step 1
    public function verifyPersonalInfo(Request $request)
    {
        $valid = CheckoutService::checkValidShippingData($request);

        return response([
            'error' => $valid['error'],
            'message' => $valid['message'],
            'data' => Authentication::getGuestId()
        ]);
    }

    //step 2: get states/cities
    public function getStateCities(Request $request)
    {
        $value = $request->get('value', '');
        $data = [];
//        $data['states'] = TransferAddress::getStatesByCountry($value);
        $data['cities'] = TransferAddress::getCitiesByCountry($value);

        return response([
            'error' => false,
            'data' => $data
        ]);
    }

    //step 2
    public function getPearlsDesign(Request $request)
    {
        $category = $request->get('type', '');
        $limit = $request->get('limit', 6);
        $page = $request->get('page', 1);

        $pearls = (new PearlModel())->getList([
            'limit' => $limit,
            'page' => $page,
            'category' => $category
        ]);

        return response([
            'error' => false,
            'data' => $pearls
        ]);
    }

    //step 3,4
    public function orderInfo(Request $request)
    {
        $paymentMethod = $request->get('paymentMethod');

        if (!empty($paymentMethod)) {
            return response([
                'error' => false,
                'data' => CheckoutService::getDetailOrderInfo([
                    'subscription_type' => $request->get('subscriptionType'),
                    'product_id' => $request->get('productDesignId'),
                ])
            ]);
        }

        return response([
            'error' => false,
        ]);
    }

    //step 5
    public function payment(Request $request)
    {
        //check valid personal info again
        //TODO

        //check valid product design
        $productId = $request->get('productDesignId');
        $valid = CheckoutService::checkValidProductDesign($productId);

        if ($valid['error']) {
            return response([
                'error' => $valid['error'],
                'message' => $valid['message']
            ]);
        }

        //check valid subscription
        $subscriptionType = $request->get('subscriptionType');
        $valid = CheckoutService::checkValidSubscription($subscriptionType);

        if ($valid['error']) {
            return response([
                'error' => $valid['error'],
                'message' => $valid['message']
            ]);
        }

        //process for payment method
        $data = CheckoutService::paymentProcess($request);

        return response($data);
    }

    public function paypalExecuteAgreement(Request $request, Application $app)
    {
        $data = CheckoutService::paypalExecuteAgreementProcess([
            'status' => $request->get('success'),
            'token' => $request->get('token'),
            'order_id' => $request->get('order_id'),
            'paymentId' => $request->get('paymentId', null),
            'payerID' => $request->get('PayerID', null),
        ]);

        if (!$data['error']) {
            return redirect($app["url_generator"]->generate('checkout_success', ['order-id' => $request->get('order_id')]));
        }

        return redirect($app["url_generator"]->generate('checkout_error'));
    }

    public function success(Request $request, Application $app)
    {
        $orderId = $request->get('order-id');
        if (CheckoutService::orderIdCurrently() != $orderId) {
            return redirect($app["url_generator"]->generate('checkout_error'));
        }

        CheckoutService::clearSession();
        $data = [
            'pageData' => [
                'orderCode' => $orderId,
                'message' => trans('You will soon receive your Sarita Family solution!')
                    . '<br>' .
                    trans('We will send you the shipment details and customer login details over the email.') . '<br>' .
                    trans("If you don't receive an email within one day, please contact us at:") . "<br><a href=\"mailto:family@sarita.dk\">family@sarita.dk</a><br><a href=\"tel:+4560704977\">+4560704977</a>"
            ]
        ];

        return renderView("@views/pages/checkout-success/checkout-success", $data);
    }

    public function error(Request $request)
    {
        CheckoutService::clearSession();

        $data = [
            'pageData' => [
                'title' => trans('Sorry, can not order. It looks something wrong.'),
                'message' => trans('Error occurred during order. Please contact your system administrator.'),
            ]
        ];

        return renderView("@views/pages/checkout-error/checkout-error", $data);
    }
}