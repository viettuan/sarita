<?php

return [
    'api-create-user' => [
        'name' => 'api-create-user',
        'pattern' => '/api/webshop/users',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MockData\ApiController::createUser',
        'method' => ['post']
    ],
    'api-get-user-detail' => [
        'name' => 'api-get-user-detail',
        'pattern' => '/api/webshop/users',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MockData\ApiController::getUserDetail',
        'method' => ['get']
    ],
    'api-update-user' => [
        'name' => 'api-update-user',
        'pattern' => '/api/webshop/users',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MockData\ApiController::updateUser',
        'method' => ['put']
    ],
    'api-delete-user' => [
        'name' => 'api-delete-user',
        'pattern' => '/api/webshop/users',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MockData\ApiController::deleteUser',
        'method' => ['delete']
    ],
    'api-change-password' => [
        'name' => 'api-change-password',
        'pattern' => '/api/webshop/password',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MockData\ApiController::changePassword',
        'method' => ['put']
    ],
    'api-create-subscription' => [
        'name' => 'api-create-subscription',
        'pattern' => '/api/webshop/subscriptions',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MockData\ApiController::createSubscription',
        'method' => ['post']
    ],
    'api-get-subscription' => [
        'name' => 'api-get-subscriptions',
        'pattern' => '/api/webshop/subscriptions',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MockData\ApiController::getSubscriptions',
        'method' => ['get']
    ],
    'api-cancel-subscription' => [
        'name' => 'api-cancle-subscriptions',
        'pattern' => '/api/webshop/subscriptions',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MockData\ApiController::cancelSubscription',
        'method' => ['put']
    ],
];