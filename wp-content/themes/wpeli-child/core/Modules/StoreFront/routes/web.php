<?php

use EliChild\Modules\StoreFront\MiddleWare\AuthMiddleWare;

return [
    'home' => [
        'name' => 'home',
        'pattern' => '/',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\HomePageController::index',
        'method' => ['get', 'post']
    ],
    'test' => [
        'name' => 'test',
        'pattern' => '/test',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\HomePageController::test',
        'method' => array('get', 'post', 'put', 'delete', 'options', 'head'),
    ],
    'subscribe' => [
        'name' => 'subscribe',
        'pattern' => '/ajax/subscribe-newsletter',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\HomePageController::registerSubscribe',
        'method' => ['post'],
    ],
    'myAccount' => [
        'name' => 'myAccount',
        'pattern' => '/my-account',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MyAccountController::index',
        'method' => ['get'],
        'before' => [new AuthMiddleWare()]
    ],
    'updatePersonalInfo' => [
        'name' => 'updatePersonalInfo',
        'pattern' => '/ajax/my-account/update-personal-info',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MyAccountController::updatePersonalInfo',
        'method' => ['post'],
        'before' => [new AuthMiddleWare()]
    ],
    'savePaymentData' => [
        'name' => 'savePaymentData',
        'pattern' => '/ajax/my-account/add-payment-data',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MyAccountController::savePaymentData',
        'method' => ['post'],
        'before' => [new AuthMiddleWare()]
    ],
    'deleteCreditCard' => [
        'name' => 'deleteCreditCard',
        'pattern' => ' ajax/my-account/delete-credit-card',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MyAccountController::deleteCreditCard',
        'method' => ['post'],
        'before' => [new AuthMiddleWare()]
    ],
    'cancel-subscription' => [
        'name' => 'cancel-subscription',
        'pattern' => ' ajax/my-account/delete-subcription',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MyAccountController::cancelSubscription',
        'method' => ['post'],
        'before' => [new AuthMiddleWare()]
    ],
    'learnMore' => [
        'name' => 'learnMore',
        'pattern' => '/learn-more',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\LearnMorePageController::index',
        'method' => array('get', 'post')
    ],

    'ajaxLoadAddress' => [
        'name' => 'ajaxLoadAddress',
        'pattern' => '/ajax/address/load-address',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MyAccountController::ajaxLoadAddress',
        'method' => array('post')
    ],

    'ajaxLoadPhoneCode' => [
        'name' => 'ajaxLoadPhoneCode',
        'pattern' => '/ajax/theme-option/load-phone-codes',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MyAccountController::ajaxLoadPhoneCodes',
        'method' => array('get')
    ],

    'readFileTranslate' => [
        'name' => 'readFileTranslate',
        'pattern' => '/ajax/theme-option/read-file-translate/{lang}',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\AdminController::getFileTranslate',
        'method' => array('get')
    ]
];