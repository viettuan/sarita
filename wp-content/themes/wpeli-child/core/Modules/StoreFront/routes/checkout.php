<?php

return [
    'checkout' => [
        'name' => 'checkout',
        'pattern' => 'checkout',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::index',
        'method' => ['get', 'post']
    ],
    'get-state-cities' => [
        'name' => 'get-state-cities',
        'pattern' => '/ajax/get-state-cities',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::getStateCities',
        'method' => ['get']
    ],
    'verify-personal-info' => [
        'name' => 'verify-personal-info',
        'pattern' => '/ajax/checkout/verify-personal-info',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::verifyPersonalInfo',
        'method' => ['post']
    ],
    'get-pearls-design' => [
        'name' => 'get-pearls-design',
        'pattern' => '/ajax/checkout/product-designs',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::getPearlsDesign',
        'method' => ['get']
    ],
    'order-info' => [
        'name' => 'order-info',
        'pattern' => '/ajax/checkout/order-info',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::orderInfo',
        'method' => ['get', 'post']
    ],
    'place-order' => [
        'name' => 'place-order',
        'pattern' => '/ajax/checkout/place-order',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::payment',
        'method' => ['post']
    ],
    'execute-agreement' => [
        'name' => 'execute-agreement',
        'pattern' => '/checkout/execute-agreement',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::paypalExecuteAgreement',
        'method' => ['get']
    ],
    'checkout-success' => [
        'name' => 'checkout-success',
        'pattern' => '/checkout/success',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::success',
        'method' => ['get']
    ],
    'checkout-error' => [
        'name' => 'checkout-error',
        'pattern' => '/checkout/error',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\CheckoutController::error',
        'method' => ['get']
    ],
];