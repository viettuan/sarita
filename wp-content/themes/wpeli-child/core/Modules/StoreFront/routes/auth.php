<?php

return [
    'login' => [
        'name' => 'login',
        'pattern' => '/ajax/login',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\AuthController::login',
        'method' => array('post')
    ],
    'logout' => [
        'name' => 'logout',
        'pattern' => 'logout',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\AuthController::logout',
        'method' => array('get')
    ],
    'forgot-password' => [
        'name' => 'forgot-password',
        'pattern' => 'ajax/forgot-password',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\AuthController::forgotPassword',
        'method' => array('post')
    ],
    'update-password' => [
        'name' => 'update-password',
        'pattern' => 'ajax/my-account/update-password',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\MyAccountController::updatePassword',
        'method' => array('post')
    ]
//    'get-signup' => [
//        'name' => 'customer.getSignUp',
//        'pattern' => '/signup',
//        'controller' => $authenticationController . 'getSignup',
//        'method' => array('get')
//    ],
//
//    'post-signup' => [
//        'name' => 'customer.postSignUp',
//        'pattern' => '/customer/post-signup',
//        'controller' => $authenticationController . 'postSignup',
//        'method' => array('post', 'get')
//    ],
//
////    reset password
//    'get-page-send-mail-reset-password' => [
//        'name' => 'customer.resetPass.pageSendMail',
//        'pattern' => '/page-send-mail-reset-password',
//        'controller' => $authenticationController . 'getPageSendMailResetPassword',
//        'method' => array('get')
//    ],
//
//    'get-page-reset-password' => [
//        'name' => 'customer.resetPass.pageReset',
//        'pattern' => '/get-page-reset-password',
//        'controller' => $authenticationController . 'getPageResetPassword',
//        'method' => array('get')
//    ],
//
//    'post-change-password-reset' => [
//        'name' => 'customer.resetPass.postChange',
//        'pattern' => '/post-change-password-reset',
//        'controller' => $authenticationController . 'postChangePasswordReset',
//        'method' => array('post')
//    ],
//
//    'call-back-reset-password' => [
//        'name' => 'customer.resetPass.callback',
//        'pattern' => '/call-back-reset-password',
//        'controller' => $authenticationController . 'handleCallbackResetPassword',
//        'method' => array('get')
//    ],
//
//    'send-mail-reset-password' => [
//        'name' => 'customer.resetPass.sendMail',
//        'pattern' => '/send-mail-reset-password',
//        'controller' => $authenticationController . 'sendMailResetPassword',
//        'method' => array('post')
//    ],
//
//    'call-back-login-facebook' => [
//        'name' => 'call-back-login-facebook',
//        'pattern' => '/oauth/facebook',
//        'controller' => $authenticationController . 'handleCallBackLoginFacebook',
//        'method' => array('get')
//    ],
];
