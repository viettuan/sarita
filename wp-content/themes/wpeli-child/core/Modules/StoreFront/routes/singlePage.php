<?php

return [
    'single-page' => [
        'name' => 'single-page',
        'pattern' => '{slug}',
        'controller' => 'EliChild\Modules\StoreFront\Controllers\SinglePageController::index',
        'method' => ['get'],
    ],
];