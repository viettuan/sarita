<?php

namespace EliChild\Modules\Admin;

use EliChild\Modules\Admin\Supports\SariraCreator;
use EliChild\Modules\Base\TransferAddress;

class AdminModule
{
    public static function initialize()
    {
        return new self();
    }

    public function __construct()
    {
        add_action('wp_eli_re_active_theme', array($this, 'migrationData'), 5);

        AccessoryAdminModule::initialize();
        TestimonialAdminModule::initialize();

        add_action('admin_menu', array($this, 'initFunction'));
        add_filter('acf/load_field/name=icon_class', array($this, 'acfLoadIconChoices'));
        add_filter('acf/load_field/name=learn_more_auto_detection_icon_featured', array($this, 'acfLoadIconChoices'));
        add_filter('acf/load_field/name=learn_more_gps_icon_featured', array($this, 'acfLoadIconChoices'));
        add_filter('acf/load_field/name=setting_countries_available', array($this, 'acfLoadAllCountriesChoices'));
        add_filter('acf/load_field/name=setting_phone_codes_available', array($this, 'acfLoadAllPhoneCodesChoices'));

        add_filter('get_sample_permalink_html', array($this, 'hidePermalinks'), 10, 5);
    }

    public function initFunction()
    {
        UploadTranslationPage::initialize();
    }

    public function migrationData()
    {
        SariraCreator::migrateData();
    }

    public function hidePermalinks($return, $post_id, $new_title, $new_slug, $post)
    {
        if ($post->post_type == 'page') {
            $isPageDefault = get_post_meta($post->ID, 'post_default');
            if (!empty($isPageDefault[0])) {
                return '';
            }
        }

        return $return;
    }

    /**
     * Load list icon to Icon class select
     * @param $field
     * @return mixed
     */
    public function acfLoadIconChoices($field)
    {
        $file = file_get_contents(ROOT_CHILD_WPELI . '/assets/icons.json');
        $icons = json_decode($file, true);
        if (is_array($icons)) {
            foreach ($icons as $choice) {
                $field['choices'][$choice['class']] = $choice['title'];
            }
        }
        return $field;
    }

    public function acfLoadAllCountriesChoices($field)
    {
        $choices = TransferAddress::getAllCountries();

        // loop through array and add to field 'choices'
        if (is_array($choices)) {
            foreach ($choices as $choice) {
                $field['choices'][$choice['id']] = $choice['name'];
            }
        }
        return $field;
    }

    public function acfLoadAllPhoneCodesChoices($field)
    {
        $choices = TransferAddress::getAllPhoneCode();

        // loop through array and add to field 'choices'
        if (is_array($choices)) {
            foreach ($choices as $choice) {
                $phoneCode = $choice['phonecode'];
                if (!empty($phoneCode)) {
                    $field['choices'][$choice['id']] = '+'.$choice['phonecode']. '('.$choice['iso2'].')';
                }
            }
        }
        return $field;
    }
}