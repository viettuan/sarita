<?php

namespace EliChild\Modules\Admin;

use Eli\ModuleAbstract\ModuleAdminAbstract;
use EliChild\Helpers\ChildConstant;

class AccessoryAdminModule extends ModuleAdminAbstract
{
    public static function initialize()
    {
        return new self();
    }
    
    public function __construct($tax_slug = '', $post_types = array())
    {
        parent::__construct([
            'menu_icon' => 'dashicons-image-filter',
            'support_fields' => ['title']
        ]);

        $this->enable = ChildConstant::ACCESSORY_PT_ENABLE;
        $this->moduleName = ChildConstant::ACCESSORY_PT_NAME;
        $this->postType = ChildConstant::ACCESSORY_PT_KEY;
    }
}
