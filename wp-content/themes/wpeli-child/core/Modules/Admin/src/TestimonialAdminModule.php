<?php

namespace EliChild\Modules\Admin;

use Eli\ModuleAbstract\ModuleAdminAbstract;
use EliChild\Helpers\ChildConstant;

class TestimonialAdminModule extends ModuleAdminAbstract
{
    public static function initialize()
    {
        return new self();
    }
    
    public function __construct($tax_slug = '', $post_types = array())
    {
        parent::__construct([
            'menu_icon' => 'dashicons-admin-comments',
            'support_fields' => ['title']
        ]);

        $this->enable = ChildConstant::TESTIMONIAL_PT_ENABLE;
        $this->moduleName = ChildConstant::TESTIMONIAL_PT_NAME;
        $this->postType = ChildConstant::TESTIMONIAL_PT_KEY;
    }
}
