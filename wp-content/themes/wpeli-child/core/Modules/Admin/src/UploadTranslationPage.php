<?php

namespace EliChild\Modules\Admin;

class UploadTranslationPage
{
    private $langPathMirror = [
        'en' => 'en_US',
        'da' => 'da_DK',
    ];
    private $uploadPath = '';

    public static function initialize()
    {
        return new self();
    }

    public function __construct($tax_slug = '', $post_types = array())
    {
        $this->registerTaxonomy();
        $this->uploadPath = wp_upload_dir()['basedir'] . "/lang/";
    }

    public function registerTaxonomy()
    {
        add_menu_page("Manage Translation",
            'Manage Translation',
            'manage_options',
            'manage-translation',
            array($this, 'pageRender'));
    }

    private function getLinkFileDownLoadLastest($lang)
    {
        $nameFile = ($this->langPathMirror[$lang] ?? '') . '.json';
        $path = $this->uploadPath . $nameFile;

        if (file_exists($path)) {
            $link = '/ajax/theme-option/read-file-translate/' . $lang;
            return '<a target="_blank" href="' . $link . '">last updated</a> |';
        }

        return '';
    }

    public function pageRender()
    {
        $errorMessage = '';
        $successMessage = '';

        if ('POST' == $_SERVER['REQUEST_METHOD'] && $_FILES) {
            $file = $_FILES["file"];
            $lang = $_POST['language'] ?? '';

            if (empty($lang)) {
                $errorMessage = 'Language is required';
            } elseif (empty($file['tmp_name'])) {
                $errorMessage = 'File upload is required';
            } else {
                $jsonDataContent = json_decode(file_get_contents($file['tmp_name']), true);
                if ($jsonDataContent == NULL) {
                    $errorMessage = 'Imported content invalid format JSON';
                } else {
                    if (!file_exists($this->uploadPath)) {
                        wp_mkdir_p($this->uploadPath);
                    };
                    $langFileName = $this->uploadPath . $this->langPathMirror[$lang] . '.json';
                    @file_put_contents($langFileName, json_encode($jsonDataContent, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));

                    $successMessage = 'Successfully';
                }
            }
        }

        $enabledLanguages = get_option('qtranslate_enabled_languages');
        ?>
        <div class="wrap icon-manager event-icon-setting-box">
            <form action="" method="post" enctype="multipart/form-data" name="front_end_upload">
                <div class="postbox">
                    <div class="submitbox inside">
                        <table class="wp-list-table widefat">
                            <tr>
                                <th> File upload</th>
                                <td>
                                    <input type="file" name="file">
                                </td>
                            </tr>
                            <tr>
                                <th>Language</th>
                                <td>
                                    <table>
                                        <?php foreach ($enabledLanguages as $lang): ?>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input type="radio" name="language"
                                                               value="<?php echo $lang; ?>"><?php echo $lang ?>
                                                        -
                                                        Download <?php echo $this->getLinkFileDownLoadLastest($lang) ?>
                                                        <a target="_blank"
                                                           href="<?php echo get_theme_file_uri("admin/files/$lang.json") ?>">
                                                            example</a>
                                                    </label>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <?php if (!empty($errorMessage)) {
                            echo '<p class="error danger text-danger" style="color: red">' . $errorMessage . '</p>';
                        } ?>

                        <?php if (!empty($successMessage)) {
                            echo '<p class="success" style="color: green">' . $successMessage . '</p>';
                        } ?>
                        <div class="clearfix"></div>
                        <br>
                        <input type="submit" class="button button-primary button-large" value="Save">
                    </div>
                    <br/><br/>
                </div>
            </form>
        </div>
        <?php
    }
}