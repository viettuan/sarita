<?php

namespace EliChild\Modules\Admin\Supports;


use Eli\Deployment\DummyData;

class SariraCreator
{
    public static function migrateData()
    {
        $dummyDataObject = new DummyData();
        $dummyDataObject->dummyDataPage();
        $dummyDataObject->dummyDataPost();
        $dummyDataObject->dummyDataMenu();
    }
}
