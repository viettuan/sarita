<tr>
    <td>
        <p>Dear {fullname},</p>
        <p>Your new account is set up.</p>
        <p>You can log in with the following information:</p>
        <ul>
            <li>Username: {username}</li>
            <li>Password: {password}</li>
        </ul>
        <p>Sign in: <a href="{login_link}" style="color:{primary_color};text-decoration:underline;">{login_link}</a></p>
        <p>Thanks! </p>
        <p>The Team <a href="" style="color:{primary_color};text-decoration:none;">@ {site_name}</a></p>
    </td>
</tr>