<tr>
    <td>
        <p>Dear {username},</p>
        <p>You`ve been invited to join {site_name} at {site_url}</p>
        <p>If you do not want to join this site please ignore
            this email. This invitation will expire in a few days.</p>
        <p>Please click the following link to activate your user account: {activate_link}</p>
    </td>
</tr>