<tr>
    <td>
        <p>Hi {username},</p>
        <p>This notice confirms that your password was changed on apollo.</p>
        <p>If you did not change your password, please contact the Site Administrator at {admin_email}</p>
        <p>This email has been sent to {admin_email}</p>
        <p>Regards,</p>
        <p>All at {site_name}</p>
        <p><a href="{site_url}">{site_url}</a></p>
    </td>
</tr>