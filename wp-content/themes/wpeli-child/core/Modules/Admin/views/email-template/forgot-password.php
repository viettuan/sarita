<tr>
    <td>
        <p>Somebody recently asked to reset your password. </p>
        <p>Username: {username}</p>
        <p>If this was a mistake, just ignore this email and nothing will happen.</p>
        <p>To reset your password, visit the following address: <a href="{reset_link}" style="color:{primary_color};text-decoration:underline;">{reset_link}</a></p>
        <br />
    </td>
</tr>