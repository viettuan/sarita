<?php

namespace EliChild\Modules\Customer\Admin;

use Eli\ModuleAbstract\ModuleAdminAbstract;
use EliChild\Helpers\ChildConstant;
use EliChild\Modules\Base\TransferAddress;
use EliChild\Modules\Customer\Models\CustomerModel;

class CustomerAdminModule extends ModuleAdminAbstract
{
    public static function initialize()
    {
        return new self();
    }

    public function __construct()
    {
        parent::__construct([
            'menu_icon' => 'dashicons-buddicons-buddypress-logo',
            'support_fields' => ['revisions'],
            'enable_create_post' => 'do_not_allow'
        ]);
        $this->enable = ChildConstant::CUSTOMER_MODULE_ENABLE;
        $this->moduleName = ChildConstant::CUSTOMER_MODULE_NAME;
        $this->postType = ChildConstant::CUSTOMER_MODULE_POST_TYPE;

        //Admin column
        add_filter('manage_edit-' . $this->postType . '_columns', array($this, 'editColumns'));
        add_action('manage_' . $this->postType . '_posts_custom_column', array($this, 'customColumns'), 2);
        add_filter('acf/load_field/name=country', array($this, 'acfLoadFieldChoices'));
        add_filter('acf/load_field/name=phoneCode', array($this, 'acfLoadFieldChoices'));
        add_filter('acf/load_field/name=city', array($this, 'acfLoadFieldChoices'));
        add_filter('acf/load_field/name=state', array($this, 'acfLoadFieldChoices'));
        add_filter('acf/load_field/name=sarita_id', array($this, 'disableAcfLoadField'));
        add_filter('acf/load_field/name=password_hash', array($this, 'disableAcfLoadField'));
        add_action('admin_enqueue_scripts', array($this, 'enqueueAdminScripts'));
        add_action('pre_post_update', array($this, 'actionCustomer'));
        add_action('admin_notices', array($this, 'addError'));
    }

    public function editColumns($existing_columns)
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            $columns = [];
            $columns["cb"] = "<input type=\"checkbox\" />";
            $columns["name"] = __('Full name', 'sarita');
            $columns["status"] = __('Status', 'sarita');
            $columns["date"] = __('Date', 'sarita');

            return $columns;
        }
    }

    public function customColumns($column)
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            $customerModel = new CustomerModel($post->ID);
            switch ($column) {
                case "name" :
                    $edit_link = get_edit_post_link($post->ID);
                    $title = $customerModel->getFullName();
                    echo '<strong><a class="row-title" href="' . esc_url($edit_link) . '">' . $title . '</a>';
                    echo '</strong>';
                    break;

                case "status" :
                    echo $customerModel->getStatus();
                    break;
            }
        }
    }

    public function acfLoadFieldChoices($field)
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            $field['choices'] = [];

            switch ($field['name']) {
                case 'country' :
                    $choices = TransferAddress::getListCountries();

                    // loop through array and add to field 'choices'
                    if (is_array($choices)) {
                        foreach ($choices as $choice) {
                            $field['choices'][$choice['id']] = $choice['name'];
                        }
                    }
                    break;

                case 'phoneCode' :
                    $choices = TransferAddress::getListPhoneCode();

                    if (is_array($choices)) {
                        foreach ($choices as $choice) {
                            $field['choices'][$choice['value']] = $choice['label'] . '(' . $choice['iso2'] . ')';
                        }
                    }
                    break;

                case 'state' :
                    $countryID = get_field('country') ?? '';
                    $choices = TransferAddress::getStatesByCountry($countryID);

                    if (is_array($choices)) {
                        foreach ($choices as $choice) {
                            $field['choices'][$choice['value']] = $choice['label'];
                        }
                    }
                    break;

                case 'city' :
                    $countryID = get_field('country') ?? '';
                    $choices = TransferAddress::getCitiesByCountry($countryID);

                    if (is_array($choices)) {
                        foreach ($choices as $choice) {
                            $field['choices'][$choice['value']] = $choice['label'];
                        }
                    }
                    break;
            }
        }

        return $field;
    }

    public function disableAcfLoadField($field)
    {
        $field['disabled'] = 1;

        return $field;
    }

    public function enqueueAdminScripts($hook)
    {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            wp_enqueue_script('admin-my-script', get_theme_file_uri('core/Modules/Customer/assets/customer.js'));
        }
    }

    public function actionCustomer()
    {
        global $post;


        if ($post && $post->post_type == $this->postType) {
            $customerId = $post->ID;
            $request = $_POST['acf'] ?? null;

            if ($request) {
                $this->updateUser($request, $customerId);
            } else if ($_GET) {

                if ($_GET['action'] == 'trash') {
                    $this->deleteUser($customerId);
                }
            }
        }
    }

    private function updateUser($request, $customerId)
    {
        $customerModel = new CustomerModel();
        $saritaId = get_field('sarita_id', $customerId);
        $customer = [];

        foreach ($request as $key => $value) {
            $field = get_field_object($key);
            $customer[$field['name']] = $value;
        }
        $update = $customerModel->updateCustomer($saritaId, $customer, $customerId);

        if (!empty($update['error'])) {
            update_option('addError', $update['message']);
            header('Location: ' . get_edit_post_link($customerId, 'redirect'));
            exit;
        }
    }

    private function deleteUser($customerId)
    {
        $customerModel = new CustomerModel();

        $delete = $customerModel->deleteUser($customerId);
        if (!$delete) {
            update_option('addError', 'Can not delete customer, please check subscriptions!');
            header('Location: ' . get_edit_post_link($customerId, 'redirect'));
            exit;
        }
    }

    public function addError()
    {
        $notifications = get_option('addError');
        if (!empty($notifications)) {
            echo '<div class="error">' . $notifications . '</div>';
            update_option('addError', false);
        }
    }
}