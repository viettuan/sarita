<?php

namespace EliChild\Modules\Customer;

use EliChild\Modules\Authentication\Services\Authentication;
use EliChild\Modules\Base\TransferAddress;
use EliChild\Modules\Customer\Models\CustomerModel;
use EliChild\Modules\Order\Helpers\OrderConstant;
use EliChild\Modules\Order\Models\OrderModel;
use EliChild\Modules\Payment\GateWay\Quickpay\QuickpayGateWay;
use EliChild\Modules\StoreFront\Events\UpdatedPaymentMethodEvent;
use EliValidForm;

class CustomerModule
{
    public $customerModel;

    public function __construct()
    {
        $this->customerModel = new CustomerModel();
    }

    public function getMyAccount()
    {
        $personalInfo = $this->getPersonalInfo();
        $countryID = $personalInfo['country']['value'] ?? 0;
        $selectAddress = 'id AS value, name AS label';
        $pageData = [
            'personalInfo' => $personalInfo,
            'subscriptions' => $this->getSubscriptions(),
            'myCards' => $this->getMyCards(),
            'countries' => TransferAddress::getListCountries($selectAddress),
//            'states' => TransferAddress::getStatesByCountry($countryID),
            'cities' => TransferAddress::getCitiesByCountry($countryID),
            'phoneCodes' => TransferAddress::getListPhoneCode(),
        ];

        return $pageData;
    }

    public function updatePersonalInfo($request)
    {
        $customer = Authentication::user();
        $validForm = new EliValidForm();

        $required = [
            'email' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'country' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
        ];
        $valid = $validForm->validate($request, $required);
        if (!empty($valid)) {
            return response([
                'error' => true,
                'data' => $valid
            ]);
        }
        $request = $request->all();
        $request['status'] = STATUS_POST_PUBLISH;
        $response = $this->customerModel->updateCustomer($customer['sarita_id'] ?? '', $request, $customer['id']);

        if (!$response['error']) {
            Authentication::reGetUser();
        }

        return $response;
    }

    private function getPersonalInfo()
    {
        $customer = Authentication::user();
        if ($customer) {
            return [
                'name' => $customer['name'] ?? '',
                'surname' => $customer['surname'] ?? '',
                'address' => $customer['address'] ?? '',
                'country' =>
                    [
                        'label' => $customer['country'] ?? '',
                        'value' => $customer['country'] ?? '',
                    ],
                'state' =>
                    [
                        'label' => $customer['state'] ?? '',
                        'value' => $customer['state'] ?? '',
                    ],
                'zipcode' => $customer['zipcode'] ?? '',
                'city' =>
                    [
                        'label' => $customer['city'] ?? '',
                        'value' => $customer['city'] ?? '',
                    ],
                'phoneCode' =>
                    [
                        'label' => $customer['phoneCode'] ?? '',
                        'value' => $customer['phoneCode'] ?? '',
                    ],
                'phone' => $customer['phone'] ?? '',
                'email' => $customer['email'] ?? '',
            ];
        }

        return null;
    }

    public function getMyCards()
    {
        $customer = Authentication::user();
        $cards = json_decode($customer['credit_card'] ?? '[]', true);
        $result = [];
        if (!empty($cards) && count($cards) > 0) {
            foreach ($cards as $key => $card) {
                $result[$key] = [
                    'id' => $key,
                    'token' => $card['token'] ?? '',
                    'cardName' => $card['cardName'] ?? '',
                    'last4Digits' => $card['last4Digits'] ?? '',
                ];
            }
        }

        return $result;
    }

    private function getSubscriptions()
    {
        $orderModel = new OrderModel();
        $customer = Authentication::user();
        $subscriptionIDs = $orderModel->getSubscriptionIDs($customer['id']);
        $results = [];

        if (isset($subscriptionIDs)) {
            foreach ($subscriptionIDs as $subscriptionID) {
                $orderModel->setDetail($subscriptionID);
                $validUntil = $orderModel->getValidUntil();
                $status = $orderModel->getStatus();

                if ($this->isShowSubscriptionCancel($orderModel)) {
                    $results[] = [
                        'id' => $orderModel->getId(),
                        'headerLabel' => $this->isExpired($orderModel)['message'],
                        'orderDate' => $validUntil,
                        'info' => $this->getInfoSubscription($orderModel),
                        'productImage' => get_the_post_thumbnail_url($orderModel->getPearlId()),
                        'productName' => $orderModel->getPearlName(),
                        'isExpired' => $this->isExpired($orderModel)['status'],
                        'status' => $status,
                        'type' => $orderModel->getSubscriptionType(),
                        'isShowCancel' => $orderModel->checkConditionCancel($orderModel->getId())
                    ];
                }
            }
        }

        return $results;
    }

    private function getInfoSubscription($orderModel)
    {
        $currency = $orderModel->getCurrency();
        $prePayment = $orderModel->getPrePayment();
        $monthlyPayment = $orderModel->getMonthlyPayment();
        $type = $orderModel->getSubscriptionType();

        if ($type == OrderConstant::SUBSCRIPTION_ONE_OFF) {
            return $prePayment . $currency;
        }

        return $prePayment . ' - ' . $monthlyPayment . $currency;
    }

    private function isShowSubscriptionCancel($orderModel)
    {
        $status = $orderModel->getStatus();
        $validDay = $orderModel->getValidUntil() > date('Y-m-d');
        $type = $orderModel->getSubscriptionType();

        if (($type != OrderConstant::SUBSCRIPTION_ONE_OFF && $validDay) || $status == OrderConstant::STATUS_COMPLETED) {
            return true;
        }
        return false;
    }

    private function isExpired($orderModel)
    {
        $status = $orderModel->getStatus();
        $today = date('Y-m-d');
        $type = $orderModel->getSubscriptionType();
        $validDay = $orderModel->getValidUntil();

        if ($type == OrderConstant::SUBSCRIPTION_ONE_OFF || $validDay > $today) {
            $message = $status;

            if ($status == OrderConstant::STATUS_COMPLETED) {
                $message = ($type == OrderConstant::SUBSCRIPTION_ONE_OFF) ? trans('One-off') : trans('Next monthly payment on');

            } else if ($status == OrderConstant::STATUS_CANCELED) {
                $message = trans('Valid Until');
            }

            return [
                'status' => false,
                'message' => $message
            ];
        }

        return [
            'status' => true,
            'message' => trans('Expired')
        ];
    }

    public function ajaxLoadAddress($request)
    {
        $countryID = $request->request->get('countryId');
//        $result['states'] = TransferAddress::getStatesByCountry($countryID);
        $result['cities'] = TransferAddress::getCitiesByCountry($countryID);

        return $result;
    }

    public function savePaymentData($request)
    {
        $customer = Authentication::user();
        $result = $this->saveCreditCard($request, $customer);

        if (empty($request['useShippingAddress']) && !$result['error'] && !empty($result['data']['id'])) {
            $this->saveBillingAddress($request, $customer, $result['data']['id']);
        }

        if (!$result['error']) {
            Authentication::reGetUser();
            event(new UpdatedPaymentMethodEvent([
                'customer_id' => $customer['id']
            ]));
        }

        return $result;
    }

    private function checkSavedCard($cardNumber)
    {
        $customer = Authentication::user();
        $creditCart = json_decode($customer['credit_card'] ?? '[]', true);

        if (!empty($creditCart)) {
            foreach ($creditCart as $id => $item) {
                if (md5($cardNumber) == $item['cardExists']) {
                    return true;
                }
            }
        }
    }

    /**
     * save billing address for credit card
     * @param $request
     * @param $customer
     */
    private function saveBillingAddress($request, $customer, $creditCardId)
    {
        $addresses = json_decode($customer['billing_addresses'] ?? '[]', true);

        $addresses[$creditCardId] = [
            'address' => $request['billingAddress'],
            'country' => $request['billingCountry'],
            'state' => $request['billingState'] ?? null,
            'city' => $request['billingCity'],
            'zipcode' => $request['billingZipcode'],
        ];
        update_post_meta($customer['id'] ?? '', 'billing_addresses', json_encode($addresses));
    }

    private function saveCreditCard($request, $customer)
    {
        $payment = new QuickpayGateWay();
        $checkSavedCard = $this->checkSavedCard($request['cardNumber']);

        if ($checkSavedCard) {
            return [
                'error' => true,
                'message' => trans('Card number already exists')
            ];
        }

        $cardInfo = [
            'number' => $request['cardNumber'],
            'expiration' => $request['expiryYear'] . $request['expiryMonth'],
            'cvd' => $request['cvv'],
            'name' => $request['cardName'],
        ];
        $result = $payment->processCreateNewCard($cardInfo);

        if (!$result['error']) {
            //Merge credit card from database
            $creditCart = json_decode($customer['credit_card'] ?? '[]', true);
            $creditCartId = $result['data']['id'];
            $creditCart[$creditCartId] = [
                'cardName' => $request['cardName'] ?? '',
                'last4Digits' => $this->takeLast4Digits($request['cardNumber'] ?? ''),
                'cardExists' => md5($request['cardNumber'])
            ];
            //Update customer credit card
            $update = update_post_meta($customer['id'] ?? '', 'credit_card', json_encode($creditCart));

            if ($update) {
                $data = [
                    'error' => false,
                    'message' => trans('Success'),
                    'data' => ['id' => $creditCartId]
                ];
            }
        } else {
            $data = ['message' => $result['message']];
        }

        return $this->response($data ?? []);
    }

    /**
     * Delete credit card and billing address of credit card
     * @param $cardId
     * @return array
     */
    public function deleteCreditCard($cardId)
    {
        $customer = Authentication::user();
        //Merge credit card from database
        $creditCart = json_decode($customer['credit_card'] ?? '[]', true);
        $billingAddress = json_decode($customer['billing_addresses'] ?? '[]', true);

        //Delete credit in array
        unset($creditCart[$cardId]);
        $update = update_post_meta($customer['id'] ?? '', 'credit_card', json_encode($creditCart));

        if ($update) {

            //Delete billing of credit card address in array
            if (!empty($billingAddress[$cardId])) {
                unset($billingAddress[$cardId]);
                update_post_meta($customer['id'] ?? '', 'billing_addresses', json_encode($billingAddress));
            }
            Authentication::reGetUser();
            $data = [
                'error' => false,
                'message' => trans('Delete success')
            ];
        }

        return $this->response($data ?? []);
    }

    public function updatePassword($request)
    {
        $validForm = new EliValidForm();
        $required = [
            'oldPassword' => 'required',
            'newPassword' => 'required',
            'confirmPassword' => 'required|same:newPassword',
        ];
        $valid = $validForm->validate($request, $required);
        if (!empty($valid)) {
            return [
                'error' => true,
                'data' => $valid
            ];
        }

        $newPassword = $request->get('newPassword') ?? '';
        $oldPassword = $request->get('oldPassword') ?? '';
        $customer = Authentication::user();
        $customerUpdate = $this->customerModel->updatePassword($customer['id'], $newPassword, $oldPassword);

        if ($customerUpdate) {
            return [
                'error' => false,
                'message' => trans('Update password success')
            ];
        }

        return [
            'error' => true,
            'message' => trans('Update password failed!')
        ];
    }

    public function cancelSubscription($request)
    {
        $orderId = $request->get('subcriptionId');
        $orderModel = new OrderModel();
        $result = $orderModel->cancelledOrder($orderId);
        if ($result) {
            return [
                'error' => false,
                'message' => trans('Cancel success!')
            ];
        }

        return [
            'error' => true,
            'message' => trans('Cancel failed')
        ];
    }

    private function takeLast4Digits($string)
    {
        $length = strlen($string);
        if ($length > 4) {
            $string = substr($string, $length - 5, $length - 1);
        }

        return $string;
    }

    /**
     * Return response - default error: true
     * @param $data
     * @return array
     */
    private function response($data)
    {
        return [
            'error' => $data['error'] ?? true,
            'message' => $data['message'] ?? trans('Error'),
            'data' => $data['data'] ?? null
        ];
    }
}