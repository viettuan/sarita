<?php

namespace EliChild\Modules\Customer\Models;

use Eli\Modules\Model;
use EliChild\Helpers\ChildConstant;
use EliChild\Helpers\Helper;
use EliChild\Modules\ApiClient\Services\CustomerCurl;
use EliChild\Modules\Authentication\Services\Authentication;
use EliChild\Modules\Base\TransferAddress;
use EliChild\Modules\Order\Helpers\OrderConstant;
use EliChild\Modules\Order\Models\OrderModel;
use EliChild\Modules\StoreFront\Events\UpdatedCustomerDetailEvent;
use EliChild\Modules\StoreFront\Events\UpdatedPasswordCustomerEvent;
use WP_Query;

class CustomerModel extends Model
{
    protected $id;
    protected $saritaId;
    protected $status;
    protected $name;
    protected $email;
    protected $surname;
    protected $address;
    protected $country;
    protected $state;
    protected $city;
    protected $zipcode;
    protected $phoneCode;
    protected $phone;
    protected $passwordHash;
    protected $creditCard;
    protected $billingAddresses;
    protected $isReceiveNewsletter;

    protected $postType = ChildConstant::CUSTOMER_MODULE_POST_TYPE;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSaritaId()
    {
        return $this->saritaId;
    }

    /**
     * @param mixed $saritaId
     */
    public function setSaritaId($saritaId): void
    {
        $this->saritaId = $saritaId;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->getName() . ' ' . $this->getSurname();
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode): void
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    /**
     * @param mixed $phoneCode
     */
    public function setPhoneCode($phoneCode): void
    {
        $this->phoneCode = $phoneCode;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $passwordHash
     */
    public function setPasswordHash($passwordHash): void
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @return mixed
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }

    /**
     * @param mixed $creditCard
     */
    public function setCreditCard($creditCard): void
    {
        $this->creditCard = $creditCard;
    }

    /**
     * @return mixed
     */
    public function getBillingAddresses()
    {
        return $this->billingAddresses;
    }

    /**
     * @param mixed $billingAddresses
     */
    public function setBillingAddresses($billingAddresses): void
    {
        $this->billingAddresses = $billingAddresses;
    }

    /**
     * @return mixed
     */
    public function getisReceiveNewsletter()
    {
        return $this->isReceiveNewsletter;
    }

    /**
     * @param mixed $isReceiveNewsletter
     */
    public function setIsReceiveNewsletter($isReceiveNewsletter): void
    {
        $this->isReceiveNewsletter = $isReceiveNewsletter;
    }

    public function __construct($customerId = null)
    {
        parent::__construct();

        if (!empty($customerId)) {
            $this->setDetail($customerId);
        }
    }

    public function setDetail($id)
    {
        $post = get_post($id);
        $metas = get_post_meta($id, null, true);

        $this->setId($post->ID ?? '');
        $this->setStatus($post->post_status ?? '');
        $this->setName(Helper::getValueByMeta($metas, 'name'));
        $this->setEmail(Helper::getValueByMeta($metas, 'email'));
        $this->setSaritaId(Helper::getValueByMeta($metas, 'sarita_id'));
        $this->setSurname(Helper::getValueByMeta($metas, 'surname'));
        $this->setAddress(Helper::getValueByMeta($metas, 'address'));
        $this->setCountry(Helper::getValueByMeta($metas, 'country'));
        $this->setState(Helper::getValueByMeta($metas, 'state'));
        $this->setCity(Helper::getValueByMeta($metas, 'city'));
        $this->setZipcode(Helper::getValueByMeta($metas, 'zipcode'));
        $this->setPhoneCode(Helper::getValueByMeta($metas, 'phoneCode'));
        $this->setPhone(Helper::getValueByMeta($metas, 'phone'));
        $this->setPasswordHash(Helper::getValueByMeta($metas, 'password_hash'));
        $this->setCreditCard(Helper::getValueByMeta($metas, 'credit_card'));
        $this->setBillingAddresses(Helper::getValueByMeta($metas, 'billing_addresses'));
        $this->setIsReceiveNewsletter(Helper::getValueByMeta($metas, 'isReceiveNewsletter'));
    }

    public function getDetail()
    {
        return [
            'id' => $this->getId(),
            'sarita_id' => $this->getSaritaId(),
            'status' => $this->getStatus(),
            'name' => $this->getName(),
            'surname' => $this->getSurname(),
            'email' => $this->getEmail(),
            'address' => $this->getAddress(),
            'country' => $this->getCountry(),
            'state' => $this->getState(),
            'city' => $this->getCity(),
            'zipcode' => $this->getZipcode(),
            'phoneCode' => $this->getPhoneCode(),
            'phone' => $this->getPhone(),
            'password_hash' => $this->getPasswordHash(),
            'credit_card' => $this->getCreditCard(),
            'billing_addresses' => $this->getBillingAddresses(),
            'isReceiveNewsletter' => $this->getisReceiveNewsletter(),
        ];
    }

    public function getDetailByEmailPassword($email, $password)
    {
        $posts = get_posts([
            'post_type' => $this->postType,
            'meta_query' => [
                [
                    'key' => 'email',
                    'value' => $email,
                ],
                [
                    'key' => 'password_hash',
                    'value' => base64_encode($password),
                ],
            ],
            'posts_per_page' => -1,
        ]);

        if (!empty($posts)) {
            $this->setDetail($posts[0]->ID);

            return $this->getDetail();
        }

        return false;
    }

    public function getDetailBySaritaId($saritaId)
    {
        $args = [
            'posts_per_page' => -1,
            'post_type' => $this->postType,
            'meta_key' => 'sarita_id',
            'meta_value' => $saritaId
        ];

        $posts = (new WP_Query($args))->get_posts();

        if (isset($posts[0])) {
            $this->setDetail($posts[0]->ID);
            return $this->getDetail();
        }

        return null;
    }

    public function createCustomerFromDraft($id)
    {
        wp_publish_post($id);

        return false;
    }

    public function updateCustomer($saritaUserId, $data, $customerId)
    {
        if ($this->checkChangedData($data, $customerId)) {
            $response = $this->createOrUpdate($data, $customerId);

            if (!$response['error']) {
                $countryDetail = TransferAddress::getCountryDetail($data['country']);
                //update to lighthouse
                $customerCurl = new CustomerCurl();
                $customerCurl->updateUser($saritaUserId, [
                    'status' => 'publish',
                    'name' => $data['name'],
                    'surname' => $data['surname'],
                    'email' => $data['email'],
                    'address' => $data['address'],
                    'city' => $data['city'],
                    'country' => $countryDetail['iso3'],
                    'zipcode' => $data['zipcode'],
                    'state' => $data['state'] ?? '',
                    'phone_number_code' => $countryDetail['iso2'],
                    'phone_number' => $data['phone'],
                ]);
                event(new UpdatedCustomerDetailEvent([
                    'customer_id' => $customerId
                ]));
            }
            return $response;
        }
        return [
            'error' => false,
            'user_id' => $customerId
        ];
    }

    /**
     * Check already change data when update customer info
     * @param $data
     * @param $customerId
     * @return bool
     */
    protected function checkChangedData($data, $customerId)
    {
        $this->setDetail($customerId);
        $customer = $this->getDetail();
        $change = false;
        foreach ($data as $field => $value) {
            if ($value != ($customer[$field] ?? '')) {
                return true;
            }
        }
        return $change;
    }

    public function updateDraftCustomer($data)
    {
        $customerExist = $this->checkCustomerActiveExistByEmail($data['email']);
        if ($customerExist) {
            return [
                'error' => true,
                'message' => trans('An account is already registered with your email address. Please log in.')
            ];
        }

        $data['status'] = 'draft';

        return $this->createOrUpdate($data);
    }

    private function createOrUpdate($data, $customerId = null)
    {
        $customerExist = $this->checkCustomerExistByEmail($data['email'], $customerId);

        if ($customerId) {
            if ($customerExist) {
                return [
                    'error' => true,
                    'message' => trans('Email existed')
                ];
            }
        }
        if (!$customerId) {
            if (!$customerExist) {
                $customerId = wp_insert_post([
                    'post_title' => $data['name'] . $data['surname'],
                    'post_content' => '',
                    'post_status' => $data['status'] ?? 'draft',
                    'post_type' => $this->postType
                ]);
            } else {
                $customerId = $customerExist->ID;
                $dataUpdate = [
                    'ID' => $customerId,
                    'post_title' => $data['name'] . $data['surname'],
                ];

                if (isset($dataUpdate['status'])) {
                    $dataUpdate['post_status'] = $data['status'];
                }

                wp_update_post($dataUpdate);
            }
        }

        if ($customerId) {
            $this->updateFields($customerId, $data);

            return [
                'error' => false,
                'user_id' => $customerId
            ];
        }

        return [
            'error' => true,
            'message' => 'Error'
        ];
    }

    private function updateFields($customerId, $data)
    {
        update_post_meta($customerId, 'name', $data['name'] ?? '');
        update_post_meta($customerId, 'surname', $data['surname'] ?? '');
        update_post_meta($customerId, 'email', $data['email'] ?? '');
        update_post_meta($customerId, 'address', $data['address'] ?? '');
        update_post_meta($customerId, 'country', $data['country'] ?? '');
        update_post_meta($customerId, 'state', $data['state'] ?? '');
        update_post_meta($customerId, 'city', $data['city'] ?? '');
        update_post_meta($customerId, 'zipcode', $data['zipcode'] ?? '');
        update_post_meta($customerId, 'phoneCode', $data['phoneCode'] ?? '');
        update_post_meta($customerId, 'phone', $data['phone'] ?? '');
        update_post_meta($customerId, 'isReceiveNewsletter', $data['isReceiveNewsletter'] ?? 0);
    }

    public function updateCreditCard($customerId, $cardInfo)
    {
        update_post_meta($customerId, 'credit_card', $cardInfo);
    }

    public function checkCustomerActiveExistByEmail($email)
    {
        $posts = get_posts([
            'post_type' => $this->postType,
            'post_status' => 'publish',
            'meta_key' => 'email',
            'meta_value' => $email,
            'posts_per_page' => -1,
        ]);

        if (!empty($posts)) {
            return $posts[0];
        }

        return false;
    }

    public function checkCustomerExistByEmail($email, $customerId = null)
    {
        $posts = get_posts([
            'post_type' => $this->postType,
            'meta_key' => 'email',
            'meta_value' => $email,
            'post_status' => get_post_types('', 'names'),
            'posts_per_page' => -1,
        ]);

        if (empty($posts)) {
            return false;
        } elseif ($customerId) {

            foreach ($posts as $post) {
                if ($post->ID != $customerId) {
                    return $post;
                }
            }
            return false;
        }

        return $posts[0];
    }

    public function updatePassword($userId, $newPassword, $oldPassword = null)
    {
        $customerCurl = new CustomerCurl();
        $this->setDetail($userId);

        if (!empty($this->getSaritaId())) {
            $response = $customerCurl->changePassword($this->getSaritaId(), [
                    'old_password' => $oldPassword ?? base64_decode($this->getPasswordHash()),
                    'new_password' => $newPassword
                ]
            );

            if ($response['status'] == 'success') {
                update_post_meta($userId, 'password_hash', base64_encode($newPassword));

                Authentication::reGetUser();
                event(new UpdatedPasswordCustomerEvent([
                    'customer_id' => $userId
                ]));
                return true;
            }
        }

        return false;
    }

    public function createUserToLighthouse($customerId)
    {
        $customerCurl = new CustomerCurl();
        $this->setDetail($customerId);
        $countryDetail = TransferAddress::getCountryDetail($this->getCountry());

        $customerSaritaDetail = $customerCurl->createUser([
            'name' => $this->getName(),
            'surname' => $this->getSurname(),
            'email' => $this->getEmail(),
            'address' => $this->getAddress(),
            'city' => $this->getCity(),
            'country' => $countryDetail['iso3'], //ISO code 3
            'zipcode' => $this->getZipcode(),
            'state' => $this->getState(),
            'phone_number_code' => $countryDetail['iso2'],
            'phone_number' => $this->getPhone(),
        ]);

        if ($customerSaritaDetail['status'] == 'success') {
            update_post_meta($customerId, 'sarita_id', $customerSaritaDetail['data']['data']['sarita_user_id']);
            update_post_meta($customerId, 'password_hash', base64_encode($customerSaritaDetail['data']['data']['generated_password']));

            return $customerSaritaDetail;
        }

        return false;
    }

    public function deleteUser($customerId)
    {
        $saritaId = $saritaId = get_field('sarita_id', $customerId);
        $customerUrl = new CustomerCurl();

        if ($this->checkAvailableDelete($customerId)) {
            $customerUrl->deleteUser($saritaId);
            return true;
        }

        return false;
    }

    private function checkAvailableDelete($customerId)
    {
        $orderModel = new OrderModel();
        $subscriptionIDs = $orderModel->getSubscriptionIDs($customerId);

        if (!empty($subscriptionIDs)) {
            foreach ($subscriptionIDs as $subscriptionID) {
                $orderModel->setDetail($subscriptionID);
                if ($orderModel->getStatus() != OrderConstant::STATUS_CANCELED) {
                    return false;
                }
            }
        }

        return true;
    }

    private function getAllFields()
    {
        $fields = [];
        $groups = acf_get_field_groups([
            'post_type' => $this->postType
        ]);

        if (isset($groups[0]['ID'])) {
            $fields = acf_get_fields($groups[0]['ID']);
        }

        return $fields;
    }


    /**
     * Get fields by condition
     * @param $conditions : array conditions
     * @param $limit : number post
     * @return array|mixed
     */
    public function getFieldsByCondition($conditions, $limit)
    {
        $where = [];
        if (!empty($conditions)) {
            foreach ($conditions as $key => $arg) {
                $where[] = [
                    'key' => $key,
                    'value' => $arg
                ];
            }
            if (count($conditions) > 1) {
                $where['relation'] = 'AND';
            }
        }

        $query = [
            'numberposts' => $limit,
            'post_type' => $this->postType,
            'meta_query' => $where
        ];
        $result = new \WP_Query($query);
        $result = $result->get_posts();

        $fields = [];
        foreach ($result as $key => $item) {
            $fields[$key] = get_fields($item->ID);
            $fields[$key]['id'] = $item->ID;
        }
        return count($fields) == 1 ? $fields[0] : $fields;
    }
}
