(function ($) {
    'use strict';
    var Customer = {
        init: function () {
            $ = jQuery.noConflict();
            $(document).ready(function () {
                $('body')
                    .on('click', '#country-select select', changeCountry);
                $('.input-hidden').hide();
            });
        }
    };

    var changeCountry = function () {
        var country = $(this).val();
        getAddressByCountry(country);
    };

    var getAddressByCountry = function (countryId) {
        $.ajax( {
            url: '/ajax/address/load-address',
            type: 'POST',
            data: {
                countryId: countryId,
            },
            success: function( data ) {
                if ( data.data ) {
                    // var optionsState = '';
                    var optionsCity = '';

                    // $.each(data.data['states'], function (key, value) {
                    //     optionsState += '<option value="' + value['value'] + '">' + value['label'] + '</option>';
                    // });

                    $.each(data.data['cities'], function (key, value) {
                        optionsCity += '<option value="' + value['value'] + '">' + value['label'] + '</option>';
                    });

                    // $('#state select').html(optionsState);
                    $('#city select').html(optionsCity);
                }
            },
            complete: function() {
            }
        } );
    };


    Customer.init();
})(jQuery);