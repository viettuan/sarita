(function ($) {
    'use strict';
    var Custom = {
        init: function () {
            $ = jQuery.noConflict();
            $(document).ready(function () {
                $('#postimagediv .inside').prepend('<p>We recommend 360 x 300 pixel (1:1)</p>');
            });
        }
    };

    Custom.init();
})(jQuery);