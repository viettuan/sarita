<?php

namespace EliChild\Modules\Pearl;

use Eli\ModuleAbstract\ModuleAdminAbstract;
use EliChild\Helpers\ChildConstant;
use WP_Query;

class PearlAdminModule extends ModuleAdminAbstract
{
    public static function initialize()
    {
        return new self();
    }

    public function __construct($tax_slug = '', $post_types = array())
    {
        $this->enable = ChildConstant::PEARL_MODULE_ENABLE;
        $this->moduleName = ChildConstant::PEARL_MODULE_NAME;
        $this->postType = ChildConstant::PEARL_MODULE_POST_TYPE;

        parent::__construct([
            'menu_icon' => 'dashicons-art',
            'support_fields' => ['title', 'editor', 'thumbnail'],
        ]);

        //add_action('init', array($this, 'registerTaxonomy'), 5);
        $this->registerTaxonomy();
        add_action('save_post', array($this, 'updateDesignCode'));
        add_action('admin_enqueue_scripts', array($this, 'enqueueAdminScripts'));
    }

    public function registerTaxonomy()
    {
        $this->createTaxonomy([
            'tax_name' => __('Categories', 'wpeli'),
            'search_items' => __("Search categories", 'wpeli'),
            'single_name' => __('Categories', 'wpeli'),
            'edit_item' => __('Edit Pearl category', 'wpeli'),
            'all_items' => __('All Pearl categories', 'wpeli'),
            'menu_name' => __('Categories', 'wpeli'),
            'add_new_item' => __('Add new category', 'wpeli'),
            'parent_items' => __('Parent category', 'wpeli'),
            'parent_item_colon' => __('Parent category', 'wpeli'),
            'update_item' => __('Update category', 'wpeli'),
            'new_item_name' => __('New category', 'wpeli'),
        ]);
    }

    public function updateDesignCode($post_id)
    {
        global $post;

        if ($post && $post->post_type == $this->postType && !get_post_meta($post_id, 'pearl_design_code', true)) {
            $designCode = strtoupper(substr(md5(uniqid(rand())), 0, 16));
            update_post_meta($post_id, 'pearl_design_code', $designCode);
        }
    }

    public function enqueueAdminScripts() {
        global $post;

        if ($post && $post->post_type == $this->postType) {
            wp_enqueue_script('admin-my-script', get_theme_file_uri('core/Modules/Pearl/assets/custom.js'));
        }
    }
}
