<?php

namespace EliChild\Modules\Pearl\Models;

use EliChild\Helpers\ChildConstant;
use EliChild\Helpers\Helper;

class PearlModel
{
    protected $id;
    protected $name;
    protected $imageUrl;
    protected $designCode;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param mixed $imageUrl
     */
    public function setImageUrl($imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return mixed
     */
    public function getDesignCode()
    {
        return $this->designCode;
    }

    /**
     * @param mixed $designCode
     */
    public function setDesignCode($designCode): void
    {
        $this->designCode = $designCode;
    }

    protected $postType;

    public function __construct($pearlId = null)
    {
        $this->postType = ChildConstant::PEARL_MODULE_POST_TYPE;

        if (isset($pearlId)) {
            $this->setDetail($pearlId);
        }
    }

    public function setDetail($id)
    {
        $post = get_post($id);
        $metas = get_post_meta($id, null, true);

        $imageFeature = get_the_post_thumbnail_url($post->ID);
        if (!$imageFeature) {
            $imageFeature = feAssets('images/manual-call-button.png');
        }

        $this->setId($post->ID);
        $this->setName(isset($post->post_title) && !empty($post->post_title) ? apply_filters('translate_text', $post->post_title) : "");
        $this->setImageUrl($imageFeature);
        $this->setDesignCode(Helper::getValueByMeta($metas, 'pearl_design_code'));
    }

    public function getList($args = [])
    {
        global $wp_query;
        $limit = $args['limit'] ?? 6;
        $page = $args['page'] ?? 1;
        $searchCat = $args['category'] ?? null;
        $postsData = [];

        $queryData = [
            'posts_per_page' => $limit,
            'paged' => $page,
            'post_type' => $this->postType
        ];

        if ($searchCat > -1) {
            $queryData['tax_query'][] = [
                'taxonomy' => 'pearl',
                'field' => 'term_id',
                'terms' => $searchCat,
            ];
        }

        $posts = query_posts($queryData);

        foreach ($posts as $post) {
            $imageFeature = get_the_post_thumbnail_url($post->ID);
            if (!$imageFeature) {
                $imageFeature = feAssets('images/manual-call-button.png');
            }

            $postsData[] = [
                'id' => $post->ID,
                'label' => $post->post_title,
                'description' => $post->post_content,
                'imgsrc' => $imageFeature,
            ];
        }

        $totalItems = $wp_query->found_posts;

        return [
            'list' => $postsData,
            'totalItem' => $totalItems,
            'totalPage' => ceil($totalItems / $limit),
            'currentPage' => $page,
        ];
    }

    public function getCategories()
    {
        $terms = get_terms($this->postType);

        $data[] = [
            'value' => '-1',
            'label' => '--' . trans('Select collection') . '--'
        ];

        if (is_array($terms)) {
            foreach ($terms as $term) {
                $data[] = [
                    'value' => $term->term_id ?? '',
                    'label' => $term->name ?? ''
                ];
            }
        }

        return $data;
    }
}
