<?php

namespace EliChild\Modules\Pearl\Providers;

use EliChild\Modules\Base\Traits\LoadAndPublishDataTrait;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class PearlServiceProvider implements ServiceProviderInterface, BootableProviderInterface
{
    use LoadAndPublishDataTrait;

    /**
     * @var \Silex\Application
     */
    protected $app;

    public function boot(Application $app)
    {
        $this->app = $app;

        $this->setIsInConsole($this->runningInConsole())
            ->setNamespace('modules.Pearl')
            ->loadMigrations();
    }

    public function register(Container $container)
    {

    }
}
