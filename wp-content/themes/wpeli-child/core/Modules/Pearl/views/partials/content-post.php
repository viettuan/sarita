<li>
    <div class="blog-content no-margin">
        <div class="blog-pic-detail allow-max-height">
            <a href="<?php the_permalink() ?>">
                <img src="<?php echo $model->getImageUrl($model->postId) ?>">
            </a>
        </div>
    </div>

    <div class="blog-tt blog-category-list">
        <div class="color-category"
             style="background-color: <?php echo $model->getColorByCategory($model->postId) ?>; width: 50px; height: 20px;">
        </div>
        <div class="blog-name">

            <a href="<?php echo get_the_permalink(); ?>">
                <?php echo get_the_title(); ?>
            </a>

        </div>
    </div>

</li>
