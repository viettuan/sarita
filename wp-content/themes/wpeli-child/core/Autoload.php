<?php

namespace EliChild;

use EliChild\Email\SampleEmail;
use EliChild\Modules\Admin\AdminModule;
use EliChild\Modules\ApiClient\ApiClientModule;
use EliChild\Modules\ApiLogs\Admin\ApiLogsAdminModule;
use EliChild\Modules\Customer\Admin\CustomerAdminModule;
use EliChild\Modules\Order\OrderAdminModule;
use EliChild\Modules\Pearl\PearlAdminModule;
use EliChild\Modules\Sample\Admin\SampleAdminModule;
use EliChild\Modules\Subscription\SubscriptionModule;

class Autoload
{
    /**
     * Auto new class in core
     */
    public static function init()
    {
        if(!in_array(getFirstSegment(), ['admin', 'wp-admin', 'wp-login.php', 'wp-content'])) {
//            require_once get_theme_file_path('app.php');
            $disableRunApp = true;
        }

        SampleEmail::initialize();
        SampleAdminModule::initialize();
        SubscriptionModule::initialize();
        ApiClientModule::initialize();
        PearlAdminModule::initialize();

        if (current_user_can('sarita_admin') || current_user_can('administrator')) {
            AdminModule::initialize();
            CustomerAdminModule::initialize();
            OrderAdminModule::initialize();
            ApiLogsAdminModule::initialize();
        }

        require_once get_theme_file_path('app.php');
    }
}