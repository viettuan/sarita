<?php

$wp_eli_child_includes = [
    'vendor/autoload.php',  // Scripts and stylesheets
    'lib/constant.php',      // contants
    'lib/extras.php',      // Custom functions
    'lib/setup.php',      // Theme setup,
    'lib/utils.php', // Utility functions,
    'lib/roles.php', // Roles and capabilities
    'lib/helpers.php'
];

foreach ($wp_eli_child_includes as $file) {
    $file = __DIR__ . DIRECTORY_SEPARATOR . $file;
    if (!file_exists($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'wp-eli-child'), $file), E_USER_ERROR);
    }

    require_once $file;
}

function blog_info()
{
    return bloginfo();
}

if(!function_exists('dump')){
    function dump($data)
    {
        $args = func_get_args();
        var_dump($data);
    }
}

add_action('init', 'create_nav_menu_locations');

function create_nav_menu_locations($items = [])
{
    static $once;
    global $_wp_registered_nav_menus;

    if (function_exists('qtrans_getSortedLanguages') && isset($_wp_registered_nav_menus) && !$once) {
        foreach ($_wp_registered_nav_menus as $loc => $name) {
            foreach (qtrans_getSortedLanguages() as $lang) {
                $arr[$loc . '_' . $lang] = $name . ' ' . strtoupper($lang);
            }
        }
        if (!empty($arr)) {
            $_wp_registered_nav_menus = $arr;
            $once = true;
        }
    }

    return $items;
}
