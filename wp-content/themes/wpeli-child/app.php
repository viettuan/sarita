<?php

namespace EliChild;

use EliChild\Modules\ApiClient\Providers\ApiClientServiceProvider;
use EliChild\Modules\Order\Providers\OrderServiceProvider;
use EliChild\Modules\Payment\Providers\PaymentServiceProvider;
use EliChild\Modules\Base\Providers\BaseServiceProvider;
use EliChild\Modules\Base\Providers\RoutingServiceProvider;
use EliChild\Modules\Authentication\Providers\AuthenticationServiceProvider;
use EliChild\Modules\Pearl\Providers\PearlServiceProvider;
use EliChild\Modules\StoreFront\Providers\StoreFrontServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

global $app, $globalData;
$globalData['migrations'] = [];
$globalData['events'] = [];

$dotenv = \Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

// Start session
if(empty($session_un_set) && !session_id() && empty(getenv('RUNNING_COMMAND'))) {
    session_start();
}

$app = new Application();
$request = Request::createFromGlobals();
$app['request'] = $request;

$app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'mysql_read' => array(
            'driver'    => 'pdo_mysql',
            'host'      => getenv('DB_HOST'),
            'dbname'    => getenv('DB_DATABASE'),
            'user'      => getenv('DB_USERNAME'),
            'password'  => getenv('DB_PASSWORD'),
            'charset'   => 'utf8mb4',
        ),
        'mysql_write' => array(
            'driver'    => 'pdo_mysql',
            'host'      => getenv('DB_HOST'),
            'dbname'    => getenv('DB_DATABASE'),
            'user'      => getenv('DB_USERNAME'),
            'password'  => getenv('DB_PASSWORD'),
            'charset'   => 'utf8mb4',
        ),
    ),
));

//Set all routes
$app->register(new BaseServiceProvider());
$app->register(new AuthenticationServiceProvider());
$app->register(new ApiClientServiceProvider());
$app->register(new PearlServiceProvider());
$app->register(new StoreFrontServiceProvider());
$app->register(new PaymentServiceProvider());
$app->register(new OrderServiceProvider());
$app->register(new \Silex\Provider\RoutingServiceProvider());
$app->register(new RoutingServiceProvider());

//dd($app['config.routes']);
//Manual $app->run()

if(!empty($disableRunApp)) {
    if (getenv('APP_DEBUG') == 'true') {
        $app['debug'] = true;
    } else {
        $app['debug'] = false;
    }
    if (empty(getenv('RUNNING_COMMAND'))) {
        $app->run();
    }
} else {
    $app->boot();
}