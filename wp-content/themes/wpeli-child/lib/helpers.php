<?php

function addTimestamps($array)
{
    if (is_array($array)) {
        $array['created'] = $array['updated'] = date('Y-m-d H:i:s');
    }
    return $array;
}

function redirect($nameRoute)
{
    wp_redirect($nameRoute);
    exit();
}

function mergeRequest($request, $array)
{
    $arrayRequest = $request->all();
    $request = new \Symfony\Component\HttpFoundation\Request(array_replace($arrayRequest, $array));
    return $request->query;
}

function wpView($file, $templateArgs = [], $return = true)
{
    extract(wp_parse_args($templateArgs));

    ob_start();
    if (file_exists($file)) {
        require($file);
    }
    $data = ob_get_clean();

    if ($return) {
        return $data;
    } else {
        echo $data;
    }
}

function getFirstSegment()
{
    $uri_segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));

    return $uri_segments[0] ?? '';
}