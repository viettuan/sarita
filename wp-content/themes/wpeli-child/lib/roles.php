<?php

remove_role('subscriber');
remove_role('contributor');
remove_role('author');
remove_role('editor');

$adminRoleCapabilities = get_role('administrator')->capabilities;

add_role('sarita_admin', 'Sarita Admin');
$saritaAdminRole = get_role('sarita_admin');

foreach ($adminRoleCapabilities as $capability => $isEnable) {
    $saritaAdminRole->add_cap($capability);
}
