<?php

define('THEME_PATH', get_theme_file_path());

define('GOOGLE_APP_CLIENT_ID', 'YOUR_APP_CLIENT_ID');
define('GOOGLE_APP_CLIENT_SECRET', 'YOUR_APP_CLIENT_SECRET');
define('GOOGLE_APP_CLIENT_REDIRECT_URL', get_site_url() . '/customer/google');

define('FB_APP_ID', '194247604540657');
define('FB_APP_SECRET_LOGIN', 'f5e0f527d35cc4a43d6c50144e4e9603');
define('FB_REDIRECT_URL', get_site_url() . '/oauth/facebook');

define('THEME_PARENT_DIR', get_theme_root() ?? '');
define('ROOT_CHILD_WPELI', THEME_PARENT_DIR . '/wpeli-child');
define('ROOT_CHILD_MODULE', ROOT_CHILD_WPELI . '/core/Modules');

/**
 * Authentication
 */
define('SESSION_AUTH', 'login_customer');
define('SESSION_GUEST_ID', 'guest_id');
define('SESSION_ORDER_ID', 'order_id');

/**
 * Admin module
 */
const ADMIN_MODULE_PATH = THEME_PATH . '/core/Modules/Admin';

/**
 * Email template module
 */
const EMAIL_MODULE_PATH = THEME_PATH . '/core/Modules/EmailTemplate';
