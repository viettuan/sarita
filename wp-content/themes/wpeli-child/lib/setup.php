<?php

namespace EliChild\Setup;

use EliChild\Autoload;
use EliChild\Helpers\ChildConstant;
use EliChild\Widgets\Sample\SampleWidget;
use function Roots\Sage\Assets\asset_path;
use Roots\Sage\Extras;

add_action('init', [Autoload::class, 'init'], 0);

function add_widgets()
{
    register_widget(SampleWidget::class);

    foreach (ChildConstant::$available_widgets as $widget) {
        $widget_registered = get_option('widget_' . $widget);
        if (empty($widget_registered)) {
            switch ($widget) {
                case ChildConstant::SAMPLE_WIDGET:
                    Extras\add_widget($widget);
                    break;
            }
        }
    }
}

/**
 * Theme assets
 */
function assetsChild()
{
    if (getenv('APP_ENV') == 'production') {
        $assetVersion = env('STYLE_VERSION', '1.0');
        wp_enqueue_style('sage/app-chunk-lib', get_stylesheet_directory_uri() . '/assets/styles/app-chunk-lib.css?v=' . $assetVersion, false, null);
        wp_enqueue_style('sage/app-chunk-icons.css', get_stylesheet_directory_uri() . '/assets/styles/app-chunk-icons.css?v=' . $assetVersion, false, null);
        wp_enqueue_style('sage/app', get_stylesheet_directory_uri() . '/assets/styles/app.css?v=' . $assetVersion, false, null);
        wp_enqueue_style('sage/css', get_stylesheet_directory_uri() . '/assets/styles/style.css?v=' . $assetVersion, false, null);

        wp_enqueue_script('sage/app', get_stylesheet_directory_uri() . '/assets/js/app.js?v=' . $assetVersion, false, null, true);
    } else {
        try {
            $assets = json_decode(file_get_contents(THEME_PATH . '/assets/assets.json'), true);
            foreach ($assets['scripts'] as $key => $script) {
                wp_enqueue_script('sage/app' . $key, get_stylesheet_directory_uri() . '/assets/' . $script, false, null, true);
            }

            foreach ($assets['styles'] as $key => $style) {
                wp_enqueue_style('sage/app' . $key, get_stylesheet_directory_uri() . '/assets/' . $style, false, null);
            }

        } catch (\Exception $exception) {
        }
    }
}

// Update CSS within in Admin
function adminStyle()
{
    wp_enqueue_style('admin-styles', get_stylesheet_directory_uri() . '/admin/assets/admin.css');
    if (!current_user_can('administrator')) {
        wp_enqueue_style('sarita-admin-styles', get_stylesheet_directory_uri() . '/admin/assets/sarita-admin.css');
    }

    wp_enqueue_script('admin-js', get_stylesheet_directory_uri() . '/admin/assets/script.js', ['jquery'], true, true);
}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assetsChild', 100);

add_action('init', __NAMESPACE__ . '\\add_widgets', 0);

add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\adminStyle');

function global_variables($vars)
{
    return array_merge($vars, [
        'sample message' => __('Sample message', 'wp-eli-child'),
    ]);
}

add_filter('wp_eli_global_variables', __NAMESPACE__ . '\\global_variables', 120, 1);

function remove_dashboard_meta($args = []) {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action('admin_init', __NAMESPACE__ . '\\remove_dashboard_meta');