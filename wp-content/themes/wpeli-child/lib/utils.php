<?php

$dependencies = [
    'theme_parent_admin_abstract' => get_parent_theme_file_path('core/Abstract/ModuleAdminAbstract.php'),
    'theme_parent_helper' => get_parent_theme_file_path('core/Helpers/Constant.php'),
];

foreach ($dependencies as $dependency) {
    require_once "$dependency";
}
