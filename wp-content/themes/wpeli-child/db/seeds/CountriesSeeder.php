<?php


use Phinx\Seed\AbstractSeed;

class CountriesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        include_once __DIR__ . "/../../bin/import-countries";
    }
}
