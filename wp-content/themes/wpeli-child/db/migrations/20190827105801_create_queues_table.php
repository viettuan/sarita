<?php

use Phinx\Migration\AbstractMigration;

class CreateQueuesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->createQueueTable();
        $this->createMessagesTable();
    }

    /**
     * Creates queue table on the current schema given.
     *
     */
    protected function createQueueTable()
    {
        $table = $this->table('bernard_queues', [
            'id' => false,
            'primary_key' => [
                'name',
            ]
        ]);

        $table->addColumn('name', 'string')
            ->create();
    }

    /**
     * Creates message table on the current schema given.
     *
     */
    protected function createMessagesTable()
    {
        $table = $this->table('bernard_messages');

        $table->addColumn('queue', 'string')
            ->addColumn('message', 'text')
            ->addColumn('visible', 'boolean', ['default' => true])
            ->addColumn('sentAt', 'datetime')
            ->addIndex(['queue', 'sentAt', 'visible'])
            ->create();
    }
}
