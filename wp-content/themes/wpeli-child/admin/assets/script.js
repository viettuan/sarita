jQuery(function ($) {
    $(document).ready(function () {
        var countriesAvailable = $("body.toplevel_page_theme-options div[data-name=setting_countries_available] select");
        var phoneCodesAvailable = $("body.toplevel_page_theme-options div[data-name=setting_phone_codes_available] select");

        var sortSelect2 = function(input, name)
        {
            input.on("change", function () {
                var oldValue = $(this).attr('data-id-old');
                oldValue = oldValue ? oldValue.split(',') : [];
                var result = generateChangeSelect(oldValue);

                input.attr('data-id-old', result);
            });

            $('#publish').click(function (e) {
                var phoneCode = input.attr('data-id-old');

                phoneCode = phoneCode ? phoneCode.split(',') : [];
                input.html('<option value="[' + phoneCode + ']" selected="selected">');
                var name = input.closest('.acf-field').attr('data-key');
                var ele = input.closest('.acf-input');
                ele.html('');
                if (phoneCode.length > 0) {
                    $.each(phoneCode, function (key, value) {
                        if (value) {
                            ele.append('<input type="hidden" value="' + value + '" name="acf[' + name + '][]">');
                        }
                    });
                } else{
                    ele.html('<input type="hidden" value="[]" name="acf[' + name + '][]">');
                }
            });
            var sortSelected = function (name) {
                $.ajax({
                    url: '/ajax/theme-option/load-phone-codes',
                    type: 'GET',
                    data: {
                        name: name
                    },
                    success: function (data) {
                        if (!data.error) {
                            var phoneCodes = data.data;
                            input.attr('data-id-old', phoneCodes);
                            var li = input.prev().find('.select2-search-choice');
                            var label = [];
                            phoneCodes = removeInArray(phoneCodes, '[]');
                            $.each(phoneCodes, function (key, value) {
                                if (value.length>0 && value !=='[]' && value!=='') {
                                    label.push(input.find('option[value=' + value + ']').html());
                                }
                            });

                            if (li.length > 0) {
                                $.each(li, function (key, index) {
                                    $(index).find('div').html(label[key]);
                                    $(index).attr('data-id', phoneCodes[key]);
                                });
                            }
                        }
                    },
                });
            };
            sortSelected(name);

            var generateChangeSelect = function (oldValue) {
                var li = input.prev().find('.select2-search-choice');
                var newValue = Array.isArray(input.val()) ? input.val() : [];

                oldValue = removeInArray(oldValue, '[]');

                if (li.length < oldValue.length) {

                    newValue = [];
                    $.each(li, function (key, index) {
                        newValue.push($(index).attr('data-id'));
                    });

                    oldValue = removeInArray(oldValue, difference(oldValue, newValue));
                } else {

                    var dff = difference(newValue, oldValue);
                    oldValue.push(dff);
                    $(li[li.length-1]).attr('data-id', dff);
                }
                input.val(oldValue);
                return oldValue;
            }
        };
        if (countriesAvailable.length > 0) {
            countriesAvailable.select2();
            sortSelect2(countriesAvailable, 'setting_countries_available');

        }
        if (phoneCodesAvailable.length > 0) {
            phoneCodesAvailable.select2({
                matcher: function (term, text, option) {
                    return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
                }
            });
            sortSelect2(phoneCodesAvailable, 'setting_phone_codes_available');
        }

        /**
         * Function helper
         */
        var removeInArray = function(array, value)
        {
            var index = array.indexOf(value);
            if (index !== -1){
                array.splice(index, 1);
            }
            return array;
        }

        var difference = function (a1, a2) {
            for (var i = 0; i < a1.length; i++) {
                if (a2.indexOf(a1[i]) === -1) {
                    return a1[i];
                }
            }
            return null;
        };
        var customPixel = function (max) {
            var pixel = '';
            for (i = 7; i <= max; i++) {
                pixel += i + 'px';
                if (i != max) {
                    pixel = pixel + ' ';
                }
            }
            return pixel;
        };
        if ($('.editor-custom textarea') > 0) {
            tinymce.init({
                selector: '.editor-custom textarea',
                plugins: 'textcolor colorpicker',
                menubar: 'file edit view insert format tools table tc help',
                fontsize_formats: customPixel(36),
                toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat',
            });
        }
    });

});