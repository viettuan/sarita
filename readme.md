#BACKEND INSTALLATION GUIDELINES

# Recommendations
    - PHP version 7.3 or higher.
    - MySQL version 5.6 or higher.
	- The mod_rewrite Apache module.
	
# Setup
- Clone source code from GIT
    - Git repo: https://git.elidev.info/thienld/sarita
    - Checkout to dev branch

- Create your virtual host for this project
    - Point local domain to root folder

- Install composer
    ```
        $ cd ./wp-content/themes/wpeli
        $ sudo composer.phar install
        
        $ cd ../wpeli-child
        $ sudo composer.phar install
    ```
                
- Migrations and seeders
    - Move to wpeli-child theme:
       ```
        cd  wp-content/themes/wpeli-child
       ```
    - Create .env and adapt configs environment variables in .env file:
       ```
          cp .env.example .env
       ```
       ```
        - database name
        - database user
        - database pass
        - API_LIGHTHOUSE_URL
        - API_LIGHTHOUSE_KEY
        - MAILCHIMP KEY
        - ...
       ``` 
    - Command line for Migrations
        - Run migrate: ```php bin/db migrate -e development```
        
    - Command line for Seeders
        - Run migrate: ```php bin/db seed:run -e development```
        
- Install Wordpress
    -  Make sure you have written permission for wp-content/uploads folder
    ```
        sudo chmod 777 -R wp-content/uploads
    ```
    - Open ```{{url}}/wp-admin/install.php``` in your browser. It will take you through the process to set up a wp-config.php file with your database connection details
    - Once the configuration file is set up, the installer will set up the tables needed for your site. If there is an error, double check your wp-config.php file, and try again
    - Sign in with the username and password you chose during the installation
    - Appearance > Themes: Activate  **WP Eli Child Theme** from the site theme setting (/wp-admin/themes.php)
    - Sync ACF > Sync Now
    - Theme Activation > Re-active Theme: Re-active theme to migrate data sample

- Setting multi-languages: 

    Access to admin
     + Setting general: Setting > Languages > General tab
        - Default Language / Order: check for ```English``` option
        - URL Modification Mode: check for ```Use Query Mode``` option
        - Untranslated Content: don`t select for all
             
     + Add new language: Setting > Languages > Languages tab:  
        - Language Code: ```da```
        - Flag: ```dk.png```
        - Name: ```Danish```
        - Locale: ```da_DK```
        - Flag: ```da_DK```
        - Not Available Message: ```Sorry, this entry is only available in %LANG:, : and %.```
     
     + Setting Post types enabled for translation:
        - Setting > Languages > Advanced tab: enabled for translation all post type except for customer, orders
     
     + Setting > ACF qTranslate: enable for Enable translation for Standard Field Types

- Setting footer menu
    - Appearance > Menus

- Note:
    - During the installation process, .htaccess file will auto create. If it's not exist, you can create .htaccess from .htaccess.example
    
# Setting/Run queue
    - Conf QUEUE_DRIVER in wp-content/themes/wpeli-child/.env file (sync, database)
    - If you set QUEUE_DRIVER is ```database```, you need to run in command line:
    ```
          php wp-content/themes/wpeli-child/bin/queue
    ``` 
# Write log
    - To enable wirte log when system call to lighthouse, need to set ENABLE_WRITE_API_LOG is true in .env          

# For Development

- Routing
    - Document referer: https://silex.symfony.com
    - View all routes: ```php bin/routes```
    - https://github.com/marcojanssen/silex-routing-service-provider
    - https://github.com/fayway/silex-wordpress
    - https://www.matthewdawkins.co.uk/2015/05/controller-routes-in-silex/?cn-reloaded=1

- Migrations and seeders
    - Document referer: http://docs.phinx.org/en/latest/migrations.html
    - Move to wpeli-child theme:
       ```
        cd  wp-content/themes/wpeli-child
       ```
    - Command line for Migrations
        - To create a new Migration: ```php bin/db create CreateMemberTable```
        - Run migrate: ```php bin/db migrate -e development```
        - Rollback migrate: ```php bin/db rollback -e development -t 0```
        
    - Command line for Seeders
        - To create a new Seed: ```php bin/db seed:create MemberSeeder```
        - Run migrate: ```php bin/db seed:run -e development```
    
    - To register the migrations/seeders path, you can declare it into $migrationsPathRegister variable in phpnix.php file
                      
# Online Resources
    If you have any questions that aren’t addressed in this document, please take advantage of WordPress’ numerous online resources:

- The WordPress Codex(https://codex.wordpress.org/)
	- The Codex is the encyclopedia of all things WordPress. It is the most comprehensive source of information for WordPress available.
- The WordPress Blog(https://wordpress.org/news/)
	- This is where you’ll find the latest updates and news related to WordPress. Recent WordPress news appears in your administrative dashboard by default.
- WordPress Planet(https://planet.wordpress.org/)
	- The WordPress Planet is a news aggregator that brings together posts from WordPress blogs around the web.
- WordPress Support Forums(https://wordpress.org/support/)
	- If you’ve looked everywhere and still can’t find an answer, the support forums are very active and have a large community ready to help. To help them help you be sure to use a descriptive thread title and describe your question in as much detail as possible.
- WordPress IRC Channel(https://codex.wordpress.org/IRC)
	- There is an online chat channel that is used for discussion among people who use WordPress and occasionally support topics. The above wiki page should point you in the right direction. (irc.freenode.net #wordpress)