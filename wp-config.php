<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sarita' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Config Mailer
 */
define( 'SMTP_AUTO_TLS', false );
define( 'SMTP_FROM', 'name@gmail.com' );
define( 'SMTP_PORT', 587 );
define( 'SMTP_SECURE', '' ); // none is:'', 'ssl' or 'tls'
define( 'SMTP_USERNAME', '' );
define( 'SMTP_PASSWORD', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'O0XW(XkS*k:&+9rGHHC%w4=T1Xi9(ZrC,UAfW>mSDNOQF>}!0g!G78H.Wi&9dwjL' );
define( 'SECURE_AUTH_KEY',  'CY;u@yZnyCknFTT(+_]9&HI^y~MZc|a`cP1arDNTLMr<pCl3f) *V~It_i8XzT,(' );
define( 'LOGGED_IN_KEY',    'aIg.U]@XT3Yk6_p#gcHVL;XjXkSn=f+7mM6NU#|C;5?El,9&+4oB4axLxSA7-)~J' );
define( 'NONCE_KEY',        '*wY,-,K wWF.q`/D@Q*^[bZ#Di0a}vQ]zz?{9Sj{js}N,{>sG@St4Tqs4g&ji T%' );
define( 'AUTH_SALT',        '4,K$G=|^T|lC:JPy`:=8Fi?hB>&vn;3(gTotmy2>sjjT<{Cdv8t1fi.Y4sL_YHn)' );
define( 'SECURE_AUTH_SALT', 'g:w7NCwg#i1-L9:oWY?tS# EGPQ^ic{xmz!MKXkt3vTR68;$aKMS&;GH 2b1)B5a' );
define( 'LOGGED_IN_SALT',   'fmYLJ>nV>RA><P~v(JO4@%u*<ymZf$zl>Q08uaU3y|,T:ysXlyQV/a^18zvVG9Ui' );
define( 'NONCE_SALT',       ' 5E06vOp,Sf~vD/#Gl>EPdynyUE[i]zf&p%3+ko(3GA(eILpu)!dP^9i_Oq:*I{g' );


/**#@-*/

/**
 * Mandrill API
 */
define( 'MANDRILL_APIKEY','key mandrill hre' );
define( 'MANDRILL_FROM_EMAIL','email from here' );


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );