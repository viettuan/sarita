<?php


use EliChild\Modules\ApiClient\Curl\CustomerCurl;

class CustomerCurlTest extends WP_UnitTestCase
{
    public function testGetUser()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->getUser('example@email.com', '123456');

        $this->assertFalse($data['error']);
    }

    public function testCanNotGetUser()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->getUser('', '');

        $this->assertTrue($data['error']);
        $this->assertEquals('SR_1000', $data['error_code']);
    }

    public function testCreateUser()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->createUser([
            'name' => "John",
            'surname' => "Doe",
            'email' => "johndoe@gmail.com",
            'address' => "Broadway Street, Apt. 5",
            'city' => "New York",
            'country' => "USA",
            'zipcode' => "10101",
            'state' => "NY",
            'phone_number_code' => "DK",
            'phone_number' => "123456789",
        ]);

        $this->assertFalse($data['error']);
    }

    public function testCanNotCreateUser()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->createUser([
            'name' => "John",
            'surname' => "Doe",
        ]);

        $this->assertTrue($data['error']);
        $this->assertEquals('SR_1001', $data['error_code']);
    }

    public function testUpdateUser()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->updateUser(123, [
            'sarita_user_id' => 123,
            'name' => "John",
            'surname' => "Doe",
            'email' => "johndoe@gmail.com",
            'address' => "Broadway Street, Apt. 5",
            'city' => "New York",
            'country' => "USA",
            'zipcode' => "10101",
            'state' => "NY",
            'phone_number_code' => "DK",
            'phone_number' => "123456789",
        ]);

        $this->assertFalse($data['error']);
    }

    public function testCanNotUpdateUser()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->updateUser(123, [
            'sarita_user_id' => 123,
            'name' => "John",
            'surname' => "Doe",
        ]);

        $this->assertTrue($data['error']);
        $this->assertEquals('SR_1002', $data['error_code']);
    }

    public function testDeleteUser()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->deleteUser(123);

        $this->assertFalse($data['error']);
    }

    public function testCanNotDeleteUser()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->deleteUser(0);

        $this->assertTrue($data['error']);
        $this->assertEquals('SR_1003', $data['error_code']);
    }

    public function testChangePassword()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->changePassword(123, [
            'old_password' => "123123",
            'new_password' => "123456",
        ]);

        $this->assertFalse($data['error']);
    }

    public function testCanNotChangePassword()
    {
        $customerCurl = new CustomerCurl();
        $data = $customerCurl->changePassword(0, [
            'old_password' => "123123",
            'new_password' => "123456",
        ]);

        $this->assertTrue($data['error']);
        $this->assertEquals('SR_1007', $data['error_code']);
    }
}