<?php
/**
 * PHPUnit bootstrap file
 *
 * @package My_Plugin
 */

//$_tests_dir = getenv( 'WP_TESTS_DIR' );
$_tests_dir = dirname( dirname( __FILE__ ) ) . '/wordpress-tests-lib';

if ( ! $_tests_dir ) {
	$_tests_dir = rtrim( sys_get_temp_dir(), '/\\' ) . '/wordpress-tests-lib';
}

if ( ! file_exists( $_tests_dir . '/includes/functions.php' ) ) {
	echo "Could not find $_tests_dir/includes/functions.php, have you run bin/install-wp-tests.sh ?" . PHP_EOL; // WPCS: XSS ok.
	exit( 1 );
}

// Give access to tests_add_filter() function.
require_once $_tests_dir . '/includes/functions.php';

/**
 * Manually load the plugin being tested.
 */
function _manually_load_plugin() {
//	require dirname( dirname( __FILE__ ) ) . '/my-plugin.php';
}
tests_add_filter( 'muplugins_loaded', '_manually_load_plugin' );

// Start up the WP testing environment.
require $_tests_dir . '/includes/bootstrap.php';

$session_un_set = true;
require_once dirname(dirname($_tests_dir)) . '/wp-content/themes/wpeli-child/vendor/autoload.php';
require_once dirname(dirname($_tests_dir)) . '/wp-content/themes/wpeli-child/app.php';
