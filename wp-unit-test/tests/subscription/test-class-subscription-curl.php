<?php

use EliChild\Modules\Subscription\Supports\SubscriptionCurl;

class SubscriptionCurlTest extends WP_UnitTestCase
{
    public function getMySubscriptions()
    {
        $subCurl = new SubscriptionCurl();
        $data = $subCurl->getMySubscriptions(123);

        $this->assertFalse($data['error']);
    }

    public function testCanNotGetMySubscriptions()
    {
        $subCurl = new SubscriptionCurl();
        $data = $subCurl->getMySubscriptions('');

        $this->assertTrue($data['error']);
        $this->assertEquals('SR_1004', $data['error_code']);
    }

    public function createSubscription()
    {
        $subCurl = new SubscriptionCurl();
        $data = $subCurl->createSubscription(123, [
            'subscription_type' => "monthly",
            'pre_payment' => "3125",
            'monthly_payment' => "375",
            'currency' => "DKK",
            'valid_until' => "2019-09-25",
            'status' => "available",
            'pearl_design_name' => "Lorem ipsum",
            'pearl_design_code' => "8572339895",
        ]);

        $this->assertFalse($data['error']);
    }

    public function testCanNotCreateSubscription()
    {
        $subCurl = new SubscriptionCurl();
        $data = $subCurl->createSubscription('', [
            'subscription_type' => "monthly",
            'pre_payment' => "3125",
            'monthly_payment' => "375",
            'currency' => "DKK",
            'valid_until' => "2019-09-25",
            'status' => "available",
            'pearl_design_name' => "Lorem ipsum",
            'pearl_design_code' => "8572339895",
        ]);

        $this->assertTrue($data['error']);
        $this->assertEquals('SR_1005', $data['error_code']);
    }

    public function testCancelSubscription()
    {
        $subCurl = new SubscriptionCurl();
        $data = $subCurl->cancelSubscription(123);

        $this->assertFalse($data['error']);
    }

    public function testCanNotCancelSubscription()
    {
        $subCurl = new SubscriptionCurl();
        $data = $subCurl->cancelSubscription('');

        $this->assertTrue($data['error']);
        $this->assertEquals('SR_1006', $data['error_code']);
    }
}
